const gulp = require("gulp");

const css = require("./gulp/css");
const clean = require("./gulp/clean");
const fonts = require("./gulp/fonts");
const ts = require("./gulp/ts");
const tslint = require("./gulp/tslint");

gulp.task("css:dev", css.dev);
gulp.task("css:dist", css.dist);
gulp.task("clean", clean);
gulp.task("fonts", fonts);
gulp.task("ts:dev", ts.dev);
gulp.task("ts:dist", ts.dist);
gulp.task("tslint", tslint);

gulp.task("build", gulp.parallel(
	"css:dev",
	"fonts",
	gulp.series("tslint", "clean", "ts:dev")
));

gulp.task("default", gulp.parallel(
	"css:dist",
	"fonts",
	gulp.series("clean", "ts:dist")
));

gulp.task("watch", () => {
	gulp.watch(["scss/**/*.scss"], gulp.parallel("css:dev"));
	gulp.watch(["src/**/*.ts"], gulp.series("tslint", "ts:dev"));
});
