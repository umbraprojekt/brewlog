const gulp = require("gulp");
const clean = require("gulp-clean");

module.exports = () => {
	return gulp.src([
		"src/**/*.js",
		"src/**/*.js.map",
		"spec/**/*.js",
		"spec/**/*.js.map"
	])
		.pipe(clean());
};

