const childProcess = require("child_process");

module.exports.dist = (callback) => {
	childProcess.exec("node_modules/.bin/tsc --inlineSourceMap false --inlineSources false", (err, stdout, stderr) => {
		if (stdout) console.log(stdout);
		if (stderr) console.error(stderr);
		callback(err);
	});
};

module.exports.dev = (callback) => {
	childProcess.exec("node_modules/.bin/tsc", (err, stdout, stderr) => {
		if (stdout) console.log(stdout);
		if (stderr) console.error(stderr);
		callback(err);
	});
};
