import formatter from "format-number";
import * as _nunjucks from "nunjucks";
import * as moment from "moment";
import * as path from "path";
import { batchService } from "./BatchService";
import { Type } from "../enums/Type";

moment.relativeTimeThreshold("d", 365);
moment.locale("pl");

const format = formatter({integerSeparator: " ", decimal: ","});

export const nunjucks = new _nunjucks.Environment(
	new _nunjucks.FileSystemLoader(path.join(__dirname, "../../views")),
	{autoescape: true}
);

nunjucks.addFilter("pl", (input: string) => {
	if (typeof input === "number") {
		return format(input);
	}
	return input;
});

nunjucks.addFilter("ebc", (input: number) => {
	return Math.round(Math.max(1, Math.min(80, input)));
});

nunjucks.addFilter("batchlinks", (input: string, batchBaseUrl: string) => {
	return new _nunjucks.runtime.SafeString(input.replace(/#(\d+)/g, (match: any, p1: string) => {
		const type = batchBaseUrl.indexOf("warki") >= 0 ? Type.BEER : Type.MEAD;
		const batch = batchService.getBatchById(type, parseInt(p1, 10));
		return nunjucks.render("partials/batchLink.njk", { batch, batchBaseUrl }).trim();
	}));
});

nunjucks.addFilter("unit", (input: string|number, suffix: string) => {
	if (!input || input === "?" || input === "-") {
		return input;
	}
	return input + suffix;
});

nunjucks.addFilter("fromnow", (input: string) => {
	return moment(input).fromNow();
});
