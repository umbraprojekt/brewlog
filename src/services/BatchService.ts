import { IBatch } from "../types/IBatch";
import { ITag } from "../types/ITag";
import { Batches } from "../batches/Batches";
import { Type } from "../enums/Type";
import { Status } from "../enums/Status";
import { Tag } from "../enums/Tag";

export class BatchService {

	private readonly _batches: Map<Type, Array<IBatch>> = new Map();
	private readonly _tags: Array<ITag> = [];
	private readonly _allBatches: Array<IBatch> = Batches;

	constructor() {
		this._batches.set(Type.BEER, []);
		this._batches.set(Type.MEAD, []);
		this._allBatches.forEach((batch) => this._batches.get(batch.type).push(batch));

		const tags: Set<Tag> = new Set();
		this._allBatches.forEach((batch) => {
			batch.tags.forEach((tag) => tags.add(tag));
		});
		this._tags = Array.from(tags).sort((a: ITag, b: ITag) => {
			return a.name > b.name ? 1 : (a.name < b.name ? -1 : 0);
		});

		this.fillVersions(this._batches.get(Type.BEER));
		this.fillVersions(this._batches.get(Type.MEAD));
	}

	public getBatchesByStatus(type: Type, status: Status): Array<IBatch> {
		return this._batches.get(type).filter((batch: IBatch) => batch.status === status);
	}

	public getBatchesByTagNames(tagNames: Array<string>): Array<IBatch> {
		return this._allBatches.filter((batch: IBatch) => {
			return tagNames.every((tagName: string) => {
				return batch.tags.some((tag: ITag) => tag.name === tagName);
			});
		});
	}

	public getBatchById(type: Type, id: number): IBatch {
		return this._batches.get(type).find((batch: IBatch) => batch.batch === id);
	}

	public getTags(tagNames: Array<string>): Array<ITag> {
		return this._tags.filter((tag: ITag) => tagNames.indexOf(tag.name) > -1);
	}

	public getAllTags(): Array<ITag> {
		return this._tags;
	}

	private fillVersions(batches: Array<IBatch>): void {
		for (const original of batches) {
			if (original.version === 1 || original.versions) {
				continue;
			}
			const versions: Array<IBatch> = [original];
			while (versions[0].version !== 1) {
				versions.unshift(batches.find((item: IBatch) => item.batch === versions[0].original));
			}
			const versionBatchNumbers: Array<number> = versions.map((version: IBatch) => version.batch);
			versions.forEach((version: IBatch) => version.versions = versionBatchNumbers);
		}
	}
}

export const batchService: BatchService = new BatchService();
