import * as express from "express";
import { NextFunction, Request, Response, Router } from "express";
import { batchService } from "../services/BatchService";
import { nunjucks } from "../services/nunjucks";
import * as extend from "extend";

export const tagController: Router = express.Router();

tagController.get("/", (req: Request, res: Response, next: NextFunction) => {
	if (req.query.tag) {
		const tagNames: Array<string> = Array.isArray(req.query.tag) ? req.query.tag : [req.query.tag];
		extend(res.locals, {
			selectedTags: batchService.getTags(tagNames),
			batches: batchService.getBatchesByTagNames(tagNames),
			tagNames
		});
	}

	extend(res.locals, {
		tags: batchService.getAllTags()
	});

	res.send(nunjucks.render("pages/tags.njk", res.locals));
});
