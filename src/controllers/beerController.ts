import * as express from "express";
import { NextFunction, Request, Response, Router } from "express";
import * as extend from "extend";
import { HTTPError } from "../errors/HTTPError";
import { batchService } from "../services/BatchService";
import { nunjucks } from "../services/nunjucks";
import { IBatch } from "../types/IBatch";
import { Type } from "../enums/Type";

export const beerController: Router = express.Router();

beerController.get("/:id", (req: Request, res: Response, next: NextFunction) => {
	const id: string = req.params.id;
	const batch: IBatch = batchService.getBatchById(Type.BEER, parseInt(id, 10));
	if (!batch) {
		return next(new HTTPError("Page not found.", 404));
	}
	res.locals.batchBaseUrl = "/warki";

	extend(res.locals, batchService.getBatchById(Type.BEER, parseInt(id, 10)));
	res.send(nunjucks.render("pages/beer.njk", res.locals));
});
