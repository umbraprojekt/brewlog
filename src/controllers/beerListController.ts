import * as express from "express";
import { NextFunction, Request, Response, Router } from "express";
import { batchService } from "../services/BatchService";
import { nunjucks } from "../services/nunjucks";
import * as extend from "extend";
import { Type } from "../enums/Type";
import { Status } from "../enums/Status";

export const beerListController: Router = express.Router();

beerListController.get("/", (req: Request, res: Response, next: NextFunction) => {
	extend(res.locals, {
		pinnedBatches: batchService.getBatchesByStatus(Type.BEER, Status.PINNED),
		batches: batchService.getBatchesByStatus(Type.BEER, Status.PUBLISHED)
	});
	res.locals.batchBaseUrl = "/warki";

	res.send(nunjucks.render("pages/beers.njk", res.locals));
});
