import * as express from "express";
import { NextFunction, Request, Response, Router } from "express";
import { batchService } from "../services/BatchService";
import { nunjucks } from "../services/nunjucks";
import * as extend from "extend";
import {HTTPError} from "../errors/HTTPError";
import {IBatch} from "../types/IBatch";
import { Type } from "../enums/Type";
import { Status } from "../enums/Status";

export const meadController: Router = express.Router();

meadController.get("/", (req: Request, res: Response, next: NextFunction) => {
	extend(res.locals, {
		pinnedBatches: batchService.getBatchesByStatus(Type.MEAD, Status.PINNED),
		batches: batchService.getBatchesByStatus(Type.MEAD, Status.PUBLISHED)
	});
	res.locals.batchBaseUrl = "/miody";

	res.send(nunjucks.render("pages/meads.njk", res.locals));
});

meadController.get("/:id", (req: Request, res: Response, next: NextFunction) => {
	const id: string = req.params.id;
	const batch: IBatch = batchService.getBatchById(Type.MEAD, parseInt(id, 10));
	if (!batch) {
		return next(new HTTPError("Page not found.", 404));
	}
	res.locals.batchBaseUrl = "/miody";

	extend(res.locals, batchService.getBatchById(Type.MEAD, parseInt(id, 10)));
	res.send(nunjucks.render("pages/mead.njk", res.locals));
});
