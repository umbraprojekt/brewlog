import * as http from "http";
import { Server } from "http";
import { app } from "./app";
import { port } from "./args";
import { log } from "./services/log";

const logger: debug.IDebugger = log.extend("init");

const server: Server = http.createServer(app);

server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

process.on("unhandledRejection", (up: any) => {
	throw up;
});

function onError(error: any) {
	if (error.syscall !== "listen") {
		throw error;
	}

	const bind = `Port ${port}`;

	// handle specific listen errors with friendly messages
	switch (error.code) {
		case "EACCES":
			logger(`${bind} requires elevated privileges`);
			process.exit(1);
			break;
		case "EADDRINUSE":
			logger(`${bind} is already in use`);
			process.exit(1);
			break;
		default:
			throw error;
	}
}

function onListening() {
	const addr = server.address();
	const bind = typeof addr === "string"
		? "pipe " + addr
		: "port " + addr.port;
	logger(`Listening on ${bind}`);
}
