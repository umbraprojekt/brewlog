import { Glassware } from "./Glassware";

export class Style {

	public static readonly APA = new Style("APA", Glassware.IPA_GLASS);
	public static readonly AMERICAN_BROWN_ALE = new Style("American Brown Ale", Glassware.SHAKER);
	public static readonly AIPA = new Style("American IPA", Glassware.IPA_GLASS);
	public static readonly BALTIC_PORTER = new Style("Porter Bałtycki", Glassware.STEM_GLASS);
	public static readonly BARLEY_WINE = new Style("Barley Wine", Glassware.SNIFTER);
	public static readonly BELGIAN_IPA = new Style("Belgian IPA", Glassware.IPA_GLASS);
	public static readonly BERLINER_WEISSE = new Style("Berliner Weisse", Glassware.GOBLET);
	public static readonly BEST_BITTER = new Style("Best Bitter", Glassware.NONIC);
	public static readonly BLACK_IPA = new Style("Black IPA", Glassware.IPA_GLASS);
	public static readonly BOK = new Style("Koźlak Jesienny", Glassware.STEM_GLASS);
	public static readonly BPA = new Style("Belgian Pale Ale", Glassware.SNIFTER);
	public static readonly BROWN_ALE = new Style("Brown Ale", Glassware.NONIC);
	public static readonly BURTON_ALE = new Style("Old Burton Ale", Glassware.SNIFTER);
	public static readonly COFFEE_STOUT = new Style("Coffee Stout", Glassware.STOUT_GLASS);
	public static readonly CZWORNIAK = new Style("Czwórniak", Glassware.WINE);
	public static readonly DARK_MILD = new Style("Dark Mild", Glassware.NONIC);
	public static readonly DOUBLE_SMOKED_ALE = new Style("Double Smoked Ale", Glassware.STEM_GLASS);
	public static readonly DRY_STOUT = new Style("Dry Stout", Glassware.STOUT_GLASS);
	public static readonly DUBBEL = new Style("Dubbel", Glassware.SNIFTER);
	public static readonly DWOJNIAK = new Style("Dwójniak", Glassware.WINE);
	public static readonly ENGLISH_PORTER = new Style("English Porter", Glassware.NONIC);
	public static readonly ESB = new Style("Strong Bitter", Glassware.NONIC);
	public static readonly FES = new Style("Foreign Extra Stout", Glassware.STOUT_GLASS);
	public static readonly HEFE_WEIZEN = new Style("Hefe-Weizen", Glassware.WEIZEN);
	public static readonly IRA = new Style("Irish Red Ale", Glassware.NONIC);
	public static readonly MELOMEL_TROJNIAK = new Style("Trójniak / melomel", Glassware.WINE);
	public static readonly NEIPA = new Style("New England IPA", Glassware.IPA_GLASS);
	public static readonly OLD_ALE = new Style("Old Ale", Glassware.SNIFTER);
	public static readonly ORDINARY_BITTER = new Style("Ordinary Bitter", Glassware.NONIC);
	public static readonly PALE_ALE = new Style("Pale Ale", Glassware.SHAKER);
	public static readonly QUAD = new Style("Quadrupel", Glassware.SNIFTER);
	public static readonly RAUCHBOCK = new Style("Rauchbock", Glassware.STEM_GLASS);
	public static readonly RED_IPA = new Style("Red IPA", Glassware.IPA_GLASS);
	public static readonly RIS = new Style("Imperial Stout", Glassware.SNIFTER);
	public static readonly ROGGENBIER = new Style("Roggenbier", Glassware.WEIZEN);
	public static readonly ROGGENBOCK = new Style("Roggenbock", Glassware.WEIZEN);
	public static readonly SCOTTISH_EXPORT = new Style("Scottish Export", Glassware.NONIC);
	public static readonly SMOKED_ALE = new Style("Smoked Ale", Glassware.STEM_GLASS);
	public static readonly TROPICAL_STOUT = new Style("Tropical Stout", Glassware.STOUT_GLASS);
	public static readonly TROJNIAK = new Style("Trójniak", Glassware.TUMBLER);
	public static readonly WEE_HEAVY = new Style("Wee Heavy", Glassware.SNIFTER);
	public static readonly WEIZENBOCK = new Style("Weizenbock", Glassware.WEIZEN);
	public static readonly WHEAT_IPA = new Style("Wheat IPA", Glassware.IPA_GLASS);
	public static readonly WHITE_IPA = new Style("White IPA", Glassware.IPA_GLASS);
	public static readonly WITBIER = new Style("Witbier", Glassware.WHEAT);

	private constructor(
		public readonly name: string,
		public readonly glassware: Glassware
	) {}
}
