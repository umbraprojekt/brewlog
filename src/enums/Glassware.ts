export class Glassware {

	public static readonly GOBLET = new Glassware("Goblet", "goblet");
	public static readonly IPA_GLASS = new Glassware("IPA Glass", "ipa");
	public static readonly NONIC = new Glassware("Nonic Pint", "nonic");
	public static readonly SHAKER = new Glassware("Shaker", "shaker");
	public static readonly SNIFTER = new Glassware("Snifter", "snifter");
	public static readonly STEM_GLASS = new Glassware("Pokal", "stem");
	public static readonly STOUT_GLASS = new Glassware("Stout Glass", "stout");
	public static readonly TUMBLER = new Glassware("Tumbler", "tumbler");
	public static readonly WEIZEN = new Glassware("Weizenglas", "weizen");
	public static readonly WHEAT = new Glassware("Wheat Glass", "wheat");
	public static readonly WINE = new Glassware("Kieliszek", "wine");

	private constructor(
		public readonly name: string,
		public readonly cssClass: string
	) {}
}
