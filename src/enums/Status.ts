export enum Status {

	DRAFT,
	PINNED,
	PUBLISHED
}
