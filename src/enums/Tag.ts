export class Tag {

	public static readonly AKACJOWY = new Tag("akacjowy");
	public static readonly APA = new Tag("apa");
	public static readonly BARLEY_WINE = new Tag("barley-wine");
	public static readonly BELGIAN = new Tag("belgijskie");
	public static readonly BEZALKOHOLOWE = new Tag("bezalkoholowe");
	public static readonly BITTER = new Tag("bitter");
	public static readonly BOCK = new Tag("bock");
	public static readonly BROWN_ALE = new Tag("brown-ale");
	public static readonly BURTON_ALE = new Tag("burton-ale");
	public static readonly CHMIEL_NOWOFALOWY = new Tag("chmiel-nowofalowy");
	public static readonly CURACAO = new Tag("curaçao");
	public static readonly CZARNA_PORZECZKA = new Tag("czarna-porzeczka");
	public static readonly CZWORNIAK = new Tag("czwórniak");
	public static readonly DWOJNIAK = new Tag("dwójniak");
	public static readonly EKSTRAKT = new Tag("ekstrakt");
	public static readonly IPA = new Tag("ipa");
	public static readonly KAWA = new Tag("kawa");
	public static readonly KLASZTORNE = new Tag("klasztorne");
	public static readonly KOLENDRA = new Tag("kolendra");
	public static readonly KWAS = new Tag("kwas");
	public static readonly LAGER = new Tag("lager");
	public static readonly LAKTOZA = new Tag("laktoza");
	public static readonly MALE_PIWO = new Tag("małe-piwo");
	public static readonly MELASA = new Tag("melasa");
	public static readonly MELOMEL = new Tag("melomel");
	public static readonly MILD = new Tag("mild");
	public static readonly NIESYCONY = new Tag("niesycony");
	public static readonly OLD_ALE = new Tag("old-ale");
	public static readonly OWIES = new Tag("owies");
	public static readonly PALE_ALE = new Tag("pale-ale");
	public static readonly PORTER = new Tag("porter");
	public static readonly PSZENICA = new Tag("pszenica");
	public static readonly PLATKI_DEBOWE = new Tag("płatki-dębowe");
	public static readonly RED_ALE = new Tag("red-ale");
	public static readonly REITERATED_MASH = new Tag("reiterated-mash");
	public static readonly RIS = new Tag("ris");
	public static readonly ROGGENBIER = new Tag("roggenbier");
	public static readonly ROGGENBOCK = new Tag("roggenbock");
	public static readonly RUMIANEK = new Tag("rumianek");
	public static readonly SCOTTISH_ALE = new Tag("scottish-ale");
	public static readonly SINGLE_HOP = new Tag("single-hop");
	public static readonly SINGLE_MALT = new Tag("single-malt");
	public static readonly STOUT = new Tag("stout");
	public static readonly SYCONY = new Tag("sycony");
	public static readonly TROJNIAK = new Tag("trójniak");
	public static readonly WANILIA = new Tag("wanilia");
	public static readonly WEE_HEAVY = new Tag("wee-heavy");
	public static readonly WEIZEN = new Tag("weizen");
	public static readonly WEIZENBOCK = new Tag("weizenbock");
	public static readonly WIELOKWIATOWY = new Tag("wielokwiatowy");
	public static readonly WITBIER = new Tag("witbier");
	public static readonly WISNIA = new Tag("wiśnia");
	public static readonly WEDZONE = new Tag("wędzone");
	public static readonly ZYTO = new Tag("żyto");

	public count: number = 0;

	private constructor(
		public readonly name: string
	) {}

	public increment(): Tag {
		this.count++;
		return this;
	}
}
