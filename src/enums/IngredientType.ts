export enum IngredientType {
	ADDITION_BOIL = "dodatek",
	ADDITION_MASH = "dodatek",
	BACTERIA = "bakterie",
	EXTRACT = "ekstrakt",
	FINING = "substancja klarująca",
	FRUIT = "owoce",
	GRAIN = "zasyp",
	HOP_BOIL = "chmiel",
	HOP_DRY = "chmiel na zimno",
	SUGAR = "cukier",
	WORT = "brzeczka",
	YEAST_CAKE = "gęstwa",
	YEAST_DRY = "drożdże suche",
	YEAST_LIQUID = "drożdże płynne",
	YEAST_STARTER = "starter",
	YEAST_CULTURE_STARTER = "kultura z butelki",
	FLAVOUR = "dodatek smakowy"
}
