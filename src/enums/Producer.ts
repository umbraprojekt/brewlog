export enum Producer {

	NONE = "",

	BARTNIK_SADECKI = "Bartnik Sądecki",
	BESTMALZ = "Bestmalz",
	BREWMAKER = "Brewmaker",
	BRUNTAL = "Bruntal",
	CASTLE = "Castle Malting",
	CD = "CD",
	COOPERS = "Cooper's",
	FAWCETT = "Fawcett",
	MALTEUROP = "Malteurop",
	NUTRITION = "Nutrition",
	STEINBACH = "Steinbach",
	STRZEGOM = "Strzegom",
	VIKING = "Viking Malt",
	WEYERMANN = "Weyermann",

	BROWIN = "Browin",
	FERMENTIS = "Fermentis",
	FM = "Fermentum Mobile",
	GOZDAWA = "Gozdawa",
	MJ = "Mangrove Jack's",
	SANPROBI = "Sanprobi",
	WYEAST = "Wyeast",

	HORTEX = "Hortex"
}
