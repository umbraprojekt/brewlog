export enum BrewStep {

	DOUGH_IN = "dodanie słodu",
	REST_PROTEIN = "przerwa białkowa",
	REST_FERULIC = "przerwa ferulowa",
	REST_MALTOSE = "przerwa maltozowa",
	REST_DEXTRINE = "przerwa dekstrynująca",
	REST_MAIN = "przerwa scukrzająca",
	MASHOUT = "wygrzew",
	SPARGE = "wysładzanie",
	WORT_FIRST = "brzeczka przednia",
	WORT_FIRST_SPARGE_1 = "brzeczka przednia (1 filtracja)",
	WORT_FIRST_SPARGE_2 = "brzeczka przednia (2 filtracja)",
	WORT_BOILED = "brzeczka nastawna",
	FERM_PRIMARY = "burzliwa",
	FERM_SECONDARY = "cicha",
	FERM_TERTIARY = "cicha 2",
	FERM_LAGERING = "lagerowanie",
	FERM_AGING = "leżakowanie",
	PRE_BOIL_2 = "przed gotowaniem 2",
	POST_BOIL_2 = "po gotowaniu 2",
	BOTTLING = "rozlew"
}
