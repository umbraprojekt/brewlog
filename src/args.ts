import * as minimist from "minimist";
import {ParsedArgs} from "minimist";

const argv: ParsedArgs = minimist(process.argv.slice(2));

export const hostname: string = argv.h || argv.host || "127.0.0.1";
export const port: string = argv.p || argv.port || process.env.PORT || 3000;
export const environment: string = argv.e || argv.env || process.env.NODE_ENV || "development";
