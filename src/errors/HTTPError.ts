export class HTTPError extends Error {

	public readonly status: number;
	public readonly cause: Error;

	public constructor(message: string, status?: number, cause?: Error) {
		super(message);
		this.status = status || 500;
		this.cause = cause;
	}
}
