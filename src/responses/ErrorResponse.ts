export class ErrorResponse {

	public message: string;
	public status: number;
	public stack: string;
	public cause: ErrorResponse;

	public constructor(error: any) {
		this.message = error.message;
		if (error.status) {
			this.status = error.status;
		}
		this.stack = error.stack;
		if (error.cause) {
			this.cause = new ErrorResponse(error.cause);
		}
	}
}
