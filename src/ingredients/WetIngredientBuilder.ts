import formatter from "format-number";
import { IngredientType } from "../enums/IngredientType";
import { Ingredient } from "./Ingredient";
import { Amount } from "./Amount";
import { Producer } from "../enums/Producer";

const format = formatter({ integerSeparator: " ", decimal: "," });

export class WetIngredientBuilder {

	private _type: IngredientType = IngredientType.HOP_BOIL;
	private _name: string = "?";
	private _year: number;
	private _aa: number;
	private _producer: Producer;

	public constructor(
		protected readonly _amount: Amount,
		protected readonly _time?: Amount
	) {
		if (!_time) {
			this._time = Amount.NONE;
		}
	}

	public type(type: IngredientType): WetIngredientBuilder {
		this._type = type;
		return this;
	}

	public name(name: string): WetIngredientBuilder {
		this._name = name;
		return this;
	}

	public producer(producer: Producer): WetIngredientBuilder {
		this._producer = producer;
		return this;
	}

	public get addition(): WetIngredientBuilder { return this.type(IngredientType.ADDITION_BOIL); }
	public get flavour(): WetIngredientBuilder { return this.type(IngredientType.FLAVOUR); }
	public get hopBoil(): WetIngredientBuilder { return this.type(IngredientType.HOP_BOIL); }
	public get hopDry(): WetIngredientBuilder { return this.type(IngredientType.HOP_DRY); }
	public get fining(): WetIngredientBuilder { return this.type(IngredientType.FINING); }
	public get fruit(): WetIngredientBuilder { return this.type(IngredientType.FRUIT); }

	public hopData(year: number, aa: number): WetIngredientBuilder {
		this._year = year;
		this._aa = aa;
		return this;
	}

	public get admiral(): WetIngredientBuilder { return this.name("Admiral"); }
	public get amarillo(): WetIngredientBuilder { return this.name("Amarillo"); }
	public get cascade(): WetIngredientBuilder { return this.name("Cascade"); }
	public get centennial(): WetIngredientBuilder { return this.name("Centennial"); }
	public get challenger(): WetIngredientBuilder { return this.name("Challenger"); }
	public get chinook(): WetIngredientBuilder { return this.name("Chinook"); }
	public get citra(): WetIngredientBuilder { return this.name("Citra"); }
	public get ekg(): WetIngredientBuilder { return this.name("East Kent Goldings"); }
	public get ekuanot(): WetIngredientBuilder { return this.name("Ekuanot"); }
	public get endeavour(): WetIngredientBuilder { return this.name("Endeavour"); }
	public get equinox(): WetIngredientBuilder { return this.name("Equinox"); }
	public get fuggle(): WetIngredientBuilder { return this.name("Fuggles"); }
	public get hallertauHersbrucker(): WetIngredientBuilder { return this.name("Hallertauer Hersbrücker"); }
	public get hallertauTradition(): WetIngredientBuilder { return this.name("Hallertauer Tradition"); }
	public get iunga(): WetIngredientBuilder { return this.name("Iunga"); }
	public get jarrylo(): WetIngredientBuilder { return this.name("Jarrylo"); }
	public get lubelski(): WetIngredientBuilder { return this.name("Lubelski"); }
	public get marynka(): WetIngredientBuilder { return this.name("Marynka"); }
	public get mosaic(): WetIngredientBuilder { return this.name("Mosaic"); }
	public get northdown(): WetIngredientBuilder { return this.name("Northdown"); }
	public get perle(): WetIngredientBuilder { return this.name("Perle"); }
	public get pulawski(): WetIngredientBuilder { return this.name("Puławski"); }
	public get rakau(): WetIngredientBuilder { return this.name("Rakau"); }
	public get saaz(): WetIngredientBuilder { return this.name("Saaz"); }
	public get simcoe(): WetIngredientBuilder { return this.name("Simcoe"); }
	public get sorachi(): WetIngredientBuilder { return this.name("Sorachi Ace"); }
	public get target(): WetIngredientBuilder { return this.name("Target"); }
	public get tettnang(): WetIngredientBuilder { return this.name("Tettnanger"); }
	public get tomahawk(): WetIngredientBuilder { return this.name("Tomahawk"); }
	public get warrior(): WetIngredientBuilder { return this.name("Warrior"); }
	public get willamette(): WetIngredientBuilder { return this.name("Willamette"); }

	public get curacao(): WetIngredientBuilder { return this.flavour.name("skórki pomarańczy Curaçao"); }
	public get coriander(): WetIngredientBuilder { return this.flavour.name("kolendra"); }
	public get camomile(): WetIngredientBuilder { return this.flavour.name("rumianek"); }
	public get licorice(): WetIngredientBuilder { return this.flavour.name("korzeń lukrecji"); }
	public get vanillaBean(): WetIngredientBuilder { return this.flavour.name("laska wanilii"); }
	public get irishMoss(): WetIngredientBuilder { return this.addition.name("mech irlandzki"); }
	public get nutrient(): WetIngredientBuilder { return this.addition.name("pożywka Beer Nutrient Blend")
		.producer(Producer.WYEAST); }
	public get znso4Nutrient(): WetIngredientBuilder { return this.addition.name("pożywka ZnSO4 2%");
	}

	public build(): Ingredient {
		let name: string = this._name;
		if (this._year) {
			name += ` ${this._year}`;
		}
		if (this._aa) {
			name += `, ~${format(this._aa)}% AA`;
		}

		return new Ingredient(this._type, this._producer, name, this._amount, this._time);
	}
}
