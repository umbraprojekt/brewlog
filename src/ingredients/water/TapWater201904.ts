import { Water } from "./Water";
import { IBatchWaterIons } from "../../types/IBatch/IBatchWaterIons";

export class TapWater201904 extends Water {

	public static ions: IBatchWaterIons = { Ca: 101, Mg: 14, Na: 42, SO4: 79, Cl: 60, HCO3: 311 };

	public constructor(amount: number) {
		super("kranówka - Gdańsk Czarny Dwór 2019.04", amount, TapWater201904.ions);
	}
}
