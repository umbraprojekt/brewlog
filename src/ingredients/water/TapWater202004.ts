import { Water } from "./Water";
import { IBatchWaterIons } from "../../types/IBatch/IBatchWaterIons";

export class TapWater202004 extends Water {

	public static ions: IBatchWaterIons = { Ca: 104, Mg: 10, Na: 42, SO4: 77, Cl: 52, HCO3: 392 };

	public constructor(amount: number) {
		super("kranówka - Gdańsk Czarny Dwór 2020.04", amount, TapWater202004.ions);
	}
}
