import { Water } from "./Water";
import { IBatchWaterIons } from "../../types/IBatch/IBatchWaterIons";

export class TapWater2018 extends Water {

	public static ions: IBatchWaterIons = { Ca: 77, Mg: 7, Na: 42, SO4: 49, Cl: 36, HCO3: 290 };

	public constructor(amount: number) {
		super("kranówka - Gdańsk Czarny Dwór", amount, TapWater2018.ions);
	}
}
