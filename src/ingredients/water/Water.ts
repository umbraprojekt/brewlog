import { IBatchWater } from "../../types/IBatch/IBatchWater";
import { IBatchWaterIons } from "../../types/IBatch/IBatchWaterIons";

export abstract class Water implements IBatchWater {

	public static readonly PROFILE_BURTON_ON_TRENT: IBatchWaterIons = {
		Ca: 295,
		Mg: 45,
		Na: 55,
		SO4: 725,
		Cl: 25,
		HCO3: 300
	};
	public static readonly PROFILE_BURTON_ON_TRENT_DANIELS: IBatchWaterIons = {
		Ca: 294,
		Mg: 24,
		Na: 24,
		SO4: 801,
		Cl: 36,
		HCO3: 0
	};
	public static readonly PROFILE_MUNICH_BEERSMITH: IBatchWaterIons = {
		Ca: 75,
		Mg: 20,
		Na: 10,
		SO4: 10,
		Cl: 2,
		HCO3: 200
	};
	public static readonly PROFILE_LONDON_BEERSMITH: IBatchWaterIons = {
		Ca: 52,
		Mg: 16,
		Na: 99,
		SO4: 77,
		Cl: 60,
		HCO3: 156
	};
	public static readonly PROFILE_ANTWERP_BEERSMITH: IBatchWaterIons = {
		Ca: 90,
		Mg: 11,
		Na: 37,
		SO4: 84,
		Cl: 57,
		HCO3: 76
	};

	public constructor(
		public readonly name: string,
		public readonly amount: number,
		public readonly ions: IBatchWaterIons
	) {}
}
