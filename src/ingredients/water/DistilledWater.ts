import { Water } from "./Water";
import { IBatchWaterIons } from "../../types/IBatch/IBatchWaterIons";

export class DistilledWater extends Water {

	public static readonly ions: IBatchWaterIons = { Ca: 0, Mg: 0, Na: 0, SO4: 0, Cl: 0, HCO3: 0 };

	public constructor(amount: number) {
		super("woda destylowana", amount, DistilledWater.ions);
	}
}
