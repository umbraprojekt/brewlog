import { Ingredient } from "./Ingredient";
import { IngredientType } from "../enums/IngredientType";
import { Producer } from "../enums/Producer";
import { Amount } from "./Amount";

export class MashIngredientBuilder {

	private _type: IngredientType = IngredientType.GRAIN;
	private _producer: Producer = Producer.NONE;
	private _name: string;

	public constructor(private readonly _amount: Amount) {}

	public type(type: IngredientType): MashIngredientBuilder {
		this._type = type;
		return this;
	}

	public producer(producer: Producer): MashIngredientBuilder {
		this._producer = producer;
		return this;
	}

	public name(name: string): MashIngredientBuilder {
		this._name = name;
		return this;
	}

	public get addition(): MashIngredientBuilder { return this.type(IngredientType.ADDITION_MASH); }
	public get extract(): MashIngredientBuilder { return this.type(IngredientType.EXTRACT); }
	public get grain(): MashIngredientBuilder { return this.type(IngredientType.GRAIN); }
	public get sugar(): MashIngredientBuilder { return this.type(IngredientType.SUGAR); }
	public get wort(): MashIngredientBuilder { return this.type(IngredientType.WORT); }

	public get bartnik(): MashIngredientBuilder { return this.producer(Producer.BARTNIK_SADECKI); }
	public get bestmalz(): MashIngredientBuilder { return this.producer(Producer.BESTMALZ); }
	public get brewmaker(): MashIngredientBuilder { return this.producer(Producer.BREWMAKER); }
	public get bruntal(): MashIngredientBuilder { return this.producer(Producer.BRUNTAL); }
	public get castle(): MashIngredientBuilder { return this.producer(Producer.CASTLE); }
	public get cd(): MashIngredientBuilder { return this.producer(Producer.CD); }
	public get coopers(): MashIngredientBuilder { return this.producer(Producer.COOPERS); }
	public get fawcett(): MashIngredientBuilder { return this.producer(Producer.FAWCETT); }
	public get malteurop(): MashIngredientBuilder { return this.producer(Producer.MALTEUROP); }
	public get nutrition(): MashIngredientBuilder { return this.producer(Producer.NUTRITION); }
	public get steinbach(): MashIngredientBuilder { return this.producer(Producer.STEINBACH); }
	public get strzegom(): MashIngredientBuilder { return this.producer(Producer.STRZEGOM); }
	public get viking(): MashIngredientBuilder { return this.producer(Producer.VIKING); }
	public get weyermann(): MashIngredientBuilder { return this.producer(Producer.WEYERMANN); }

	public get lightLE(): MashIngredientBuilder { return this.extract.name("ekstrakt słodowy płynny jasny"); }
	public get darkLE(): MashIngredientBuilder { return this.extract.name("ekstrakt słodowy płynny ciemny"); }
	public get wheatLE(): MashIngredientBuilder { return this.extract.name("ekstrakt słodowy płynny pszeniczny"); }
	public get dme(): MashIngredientBuilder { return this.extract.name("ekstrakt słodowy suchy"); }
	public get pils(): MashIngredientBuilder { return this.grain.name("słód pilzneński"); }
	public get munich1(): MashIngredientBuilder { return this.grain.name("słód monachijski typ I"); }
	public get munich2(): MashIngredientBuilder { return this.grain.name("słód monachijski typ II"); }
	public get vienna(): MashIngredientBuilder { return this.grain.name("słód wiedeński"); }
	public get melanoiden(): MashIngredientBuilder { return this.grain.name("słód melanoidynowy"); }
	public get acidulating(): MashIngredientBuilder { return this.grain.name("słód zakwaszający"); }
	public get smokedBeech(): MashIngredientBuilder { return this.grain.name("słód wędzony bukiem"); }
	public get pale(): MashIngredientBuilder { return this.grain.name("słód pale ale"); }
	public get marisOtter(): MashIngredientBuilder { return this.grain.fawcett.name("słód Maris Otter"); }
	public get mild(): MashIngredientBuilder { return this.grain.fawcett.name("słód mild"); }
	public get amber(): MashIngredientBuilder { return this.grain.fawcett.name("słód amber"); }
	public get brown(): MashIngredientBuilder { return this.grain.fawcett.name("słód brown"); }
	public get crystal160(): MashIngredientBuilder { return this.grain.fawcett.name("słód crystal 160EBC"); }
	public get paleCrystal(): MashIngredientBuilder { return this.grain.fawcett.name("słód pale crystal 90EBC"); }
	public get cara30(): MashIngredientBuilder { return this.grain.fawcett.name("słód karmelowy Cara 30EBC"); }
	public get paleChocolate(): MashIngredientBuilder { return this.grain.fawcett.name("słód pale chocolate 620EBC"); }
	public get darkChocolate(): MashIngredientBuilder { return this.grain.fawcett.name("słód chocolate 1200EBC"); }
	public get biscuit(): MashIngredientBuilder { return this.grain.castle.name("słód biscuit"); }
	public get caraClair(): MashIngredientBuilder { return this.grain.castle.name("słód Cara Clair 4EBC"); }
	public get caramelAromatic(): MashIngredientBuilder { return this.grain.viking.name("słód Caramel Aromatic 180EBC"); }
	public get caramelRed(): MashIngredientBuilder { return this.grain.viking.name("słód karmelowy czerwony 50EBC"); }
	public get caramelWheat(): MashIngredientBuilder { return this.grain.viking.name("słód pszeniczny karmelowy"); }
	public get caramelRye(): MashIngredientBuilder { return this.grain.viking.name("słód żytni karmelowy"); }
	public get caramel600(): MashIngredientBuilder { return this.grain.viking.name("słód karmelowy 600EBC"); }
	public get caramel300(): MashIngredientBuilder { return this.grain.viking.name("słód karmelowy 300EBC"); }
	public get caramel150(): MashIngredientBuilder { return this.grain.viking.name("słód karmelowy 150EBC"); }
	public get caramel30(): MashIngredientBuilder { return this.grain.viking.name("słód karmelowy 30EBC"); }
	public get barwiacy(): MashIngredientBuilder { return this.grain.viking.name("słód barwiący 1400EBC"); }
	public get chocolate400(): MashIngredientBuilder { return this.grain.viking.name("słód czekoladowy 400EBC"); }
	public get wheat(): MashIngredientBuilder { return this.grain.name("słód pszeniczny"); }
	public get rye(): MashIngredientBuilder { return this.grain.name("słód żytni"); }
	public get carafa3Special(): MashIngredientBuilder { return this.grain.weyermann.name("słód Carafa 3 Special"); }
	public get caraMunich(): MashIngredientBuilder { return this.grain.weyermann.name("słód Caramunich"); }
	public get caraWheat(): MashIngredientBuilder { return this.grain.weyermann.name("słód Carawheat"); }
	public get specialB(): MashIngredientBuilder { return this.castle.name("słód Special B"); }
	public get roastedBarley(): MashIngredientBuilder { return this.grain.name("jęczmień prażony"); }
	public get wheatUnmalted(): MashIngredientBuilder { return this.grain.name("pszenica niesłodowana"); }
	public get oatFlakes(): MashIngredientBuilder { return this.grain.name("płatki owsiane"); }
	public get barleyFlakes(): MashIngredientBuilder { return this.grain.name("płatki jęczmienne"); }
	public get lactose(): MashIngredientBuilder { return this.sugar.name("laktoza"); }
	public get treacle(): MashIngredientBuilder { return this.sugar.name("black treacle"); }
	public get speltHull(): MashIngredientBuilder { return this.addition.name("łuska orkiszowa"); }
	public get miodAkacjowy(): MashIngredientBuilder { return this.sugar.name("miód akacjowy"); }
	public get miodWielokwiatowy(): MashIngredientBuilder { return this.sugar.name("miód wielokwiatowy"); }
	public get pozywkaWin(): MashIngredientBuilder { return this.addition.nutrition.name("pożywka winiarska"); }

	public build(): Ingredient {
		return new Ingredient(this._type, this._producer, this._name, this._amount);
	}
}
