export enum Unit {

	NONE = "",
	GRAM = "g",
	KILOGRAM = "kg",
	MILLILITRE = "ml",
	LITRE = "l",
	MINUTE = "min.",
	HOUR = "godz.",
	DAY = "dni",
	PACKET = "op.",
	BCFU = "mld CFU",
	UNIT = "szt."
}
