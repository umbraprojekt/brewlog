import { Unit } from "./Unit";
import formatter from "format-number";

const format = formatter({ integerSeparator: " ", decimal: "," });

export class Amount {

	public static readonly NONE: Amount = new Amount(NaN, Unit.NONE);

	public constructor(
		public amount: number,
		public unit: Unit
	) {
		// normalise to minimal units
		if (this.unit === Unit.KILOGRAM) {
			this.amount *= 1000;
			this.unit = Unit.GRAM;
		}
		if (this.unit === Unit.LITRE) {
			this.amount *= 1000;
			this.unit = Unit.MILLILITRE;
		}
		if (this.unit === Unit.DAY) {
			this.amount *= 24;
			this.unit = Unit.HOUR;
		}
		if (this.unit === Unit.HOUR) {
			this.amount *= 60;
			this.unit = Unit.MINUTE;
		}
	}

	public toString(): string {
		if (isNaN(this.amount)) {
			return "?";
		}
		if (this.unit === Unit.NONE) {
			return this.amount.toString();
		}

		const unit = this.unit;
		const amount = this.amount;

		if (unit === Unit.GRAM && amount >= 1000) {
			return `${format(amount / 1000)} ${Unit.KILOGRAM}`;
		}
		if (unit === Unit.MILLILITRE && amount >= 1000) {
			return `${format(amount / 1000)} ${Unit.LITRE}`;
		}
		if (unit === Unit.MINUTE) {
			if (amount >= 60 * 24) {
				return `${format(amount / 24 / 60)} ${Unit.DAY}`;
			}
			if (amount >= 60) {
				return `${format(amount / 60)} ${Unit.HOUR}`;
			}
		}

		return `${format(amount)} ${unit}`;
	}
}
