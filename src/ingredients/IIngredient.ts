import { IngredientType } from "../enums/IngredientType";

export interface IIngredient {

	type: IngredientType;
	name: string;
	amount: string;
	time?: string;
}
