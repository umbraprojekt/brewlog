import { IngredientType } from "../enums/IngredientType";
import { IIngredient } from "./IIngredient";
import { Amount } from "./Amount";
import { Producer } from "../enums/Producer";

export class Ingredient implements IIngredient {

	public readonly type: IngredientType;
	public readonly amount: string;
	public readonly time: string;

	protected readonly _name: string;
	protected readonly _producer: Producer;

	public constructor(type: IngredientType, producer: Producer, name: string, amount: Amount, time?: Amount);
	public constructor(type: IngredientType, name: string, amount: Amount, time?: Amount);
	public constructor(
		type: IngredientType
	) {
		this.type = type;

		let idx: number = arguments.length - 1;
		if (arguments[idx - 1] instanceof Amount) {
			this.time = arguments[idx--].toString();
			this.amount = arguments[idx--].toString();
		} else {
			this.amount = arguments[idx--].toString();
		}
		this._name = arguments[idx--];
		this._producer = idx === 1 ? arguments[idx] : Producer.NONE;
	}

	public get name(): string {
		return [
			this._name,
			(this._producer !== undefined && this._producer !== Producer.NONE ? `(${this._producer})` : "")
		].join(" ").trim();
	}
}
