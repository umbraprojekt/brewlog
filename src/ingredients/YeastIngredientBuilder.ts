import { Ingredient } from "./Ingredient";
import { IngredientType } from "../enums/IngredientType";
import { Producer } from "../enums/Producer";
import { Amount } from "./Amount";

export class YeastIngredientBuilder {

	private _type: IngredientType = IngredientType.GRAIN;
	private _producer: Producer = Producer.NONE;
	private _name: string;
	private _yeastCakeFrom: number;
	private _cultureFrom: number;

	public constructor(private readonly _amount: Amount) {}

	public type(type: IngredientType): YeastIngredientBuilder {
		this._type = type;
		return this;
	}

	public producer(producer: Producer): YeastIngredientBuilder {
		this._producer = producer;
		return this;
	}

	public name(name: string): YeastIngredientBuilder {
		this._name = name;
		return this;
	}

	public get dryYeast(): YeastIngredientBuilder { return this.type(IngredientType.YEAST_DRY); }
	public get liquidYeast(): YeastIngredientBuilder { return this.type(IngredientType.YEAST_LIQUID); }
	public get yeastCake(): YeastIngredientBuilder { return this.type(IngredientType.YEAST_CAKE); }
	public get yeastStarter(): YeastIngredientBuilder { return this.type(IngredientType.YEAST_STARTER); }
	public get yeastCulture(): YeastIngredientBuilder { return this.type(IngredientType.YEAST_CULTURE_STARTER); }
	public get bacteria(): YeastIngredientBuilder { return this.type(IngredientType.BACTERIA); }

	public yeastCakeFrom(batch: number): YeastIngredientBuilder {
		this.yeastCake._yeastCakeFrom = batch;
		return this;
	}
	public cultureFrom(batch: number): YeastIngredientBuilder {
		this.yeastCulture._cultureFrom = batch;
		return this;
	}

	public get browin(): YeastIngredientBuilder { return this.producer(Producer.BROWIN); }
	public get fermentis(): YeastIngredientBuilder { return this.producer(Producer.FERMENTIS); }
	public get gozdawa(): YeastIngredientBuilder { return this.producer(Producer.GOZDAWA); }
	public get fm(): YeastIngredientBuilder { return this.producer(Producer.FM); }
	public get mj(): YeastIngredientBuilder { return this.producer(Producer.MJ); }
	public get sanprobi(): YeastIngredientBuilder { return this.producer(Producer.SANPROBI); }
	public get wyeast(): YeastIngredientBuilder { return this.producer(Producer.WYEAST); }

	public get fm10(): YeastIngredientBuilder { return this.fm.name("FM10 \"O czym szumią wierzby\""); }
	public get fm11(): YeastIngredientBuilder { return this.fm.name("FM11 \"Wichrowe Wzgórza\""); }
	public get fm12(): YeastIngredientBuilder { return this.fm.name("FM12 \"W Szkocką Kratę\""); }
	public get fm20(): YeastIngredientBuilder { return this.fm.name("FM20 \"Białe Walonki\""); }
	public get fm25(): YeastIngredientBuilder { return this.fm.name("FM25 \"Klasztorna Medytacja\""); }
	public get fm31(): YeastIngredientBuilder { return this.fm.name("FM31 \"Bawarska Dolina\""); }
	public get fm41(): YeastIngredientBuilder { return this.fm.name("FM41 \"Gwoździe i Banany\""); }
	public get ls2(): YeastIngredientBuilder { return this.browin.name("Fermicru LS2"); }
	public get m07(): YeastIngredientBuilder { return this.mj.name("M07 British Ale"); }
	public get pa7(): YeastIngredientBuilder { return this.gozdawa.name("Pure Ale 7"); }
	public get s04(): YeastIngredientBuilder { return this.fermentis.name("Safale S-04"); }
	public get us05(): YeastIngredientBuilder { return this.fermentis.name("Safale US-05"); }
	public get wb06(): YeastIngredientBuilder { return this.fermentis.name("Safbrew WB-06"); }

	public get ibs(): YeastIngredientBuilder { return this.bacteria.name("Lactobacillus plantarum").sanprobi; }

	public build(): Ingredient {
		const name: string = this._name +
			(this._yeastCakeFrom ? ` - gęstwa z warki #${this._yeastCakeFrom}` : "") +
			(this._cultureFrom ? ` - kultura z butelki z warki #${this._cultureFrom}` : "");
		return new Ingredient(this._type, this._producer, name, this._amount);
	}
}
