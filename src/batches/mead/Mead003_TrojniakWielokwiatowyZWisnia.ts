import { BrewStep } from "../../enums/BrewStep";
import { Producer } from "../../enums/Producer";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Mead003: IBatchConfig = {
	name: "Ksenocyd",
	style: Style.MELOMEL_TROJNIAK,
	tags: [
		Tag.TROJNIAK,
		Tag.NIESYCONY,
		Tag.MELOMEL,
		Tag.WIELOKWIATOWY,
		Tag.WISNIA
	],
	batch: 3,
	version: 1,
	status: Status.PUBLISHED,
	rating: 5,
	stock: 0,
	params: {
		actual: {
			og: 36,
			volume: 2.4,
			fg: 8,
			abv: 17.7,
			attenuation: 77.8
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(1.2, Unit.KILOGRAM)).miodWielokwiatowy.bartnik.build(),
			new MashIngredientBuilder(new Amount(0.2, Unit.GRAM)).pozywkaWin.build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(10, Unit.GRAM)).dryYeast.name("Mead Yeast Trójniak (Starowar)").build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(450, Unit.GRAM), new Amount(30, Unit.DAY)).fruit.producer(Producer.HORTEX).name("wiśnie mrożone").build()
		]
	},
	water: {
		profile: "kranówka",
		base: new TapWater2018(1.7),
		ions: TapWater2018.ions
	},
	mash: [],
	fermentation: [
		{
			date: "2018-11-05",
			step: BrewStep.FERM_PRIMARY,
			volume: 2.4,
			sg: 36,
			pinned: true
		},
		{
			date: "2018-11-19",
			step: BrewStep.FERM_SECONDARY,
			volume: 2.3,
			sg: 11,
			pinned: true,
			note: "Dodano wiśnie wraz z sokiem, który puściły."
		},
		{
			date: "2019-01-27",
			step: BrewStep.FERM_AGING,
			volume: 2.3,
			sg: 8
		},
		{
			date: "2019-01-28",
			step: BrewStep.FERM_AGING,
			volume: 2.2,
			sg: 8
		},
		{
			date: "2020-05-24",
			step: BrewStep.BOTTLING,
			volume: 2.2,
			sg: 8
		}
	],
	notes: [
		"Wariant miodu #2, ale z dodatkiem wiśni.",
			"Wiśnie drylowane zostały wykąpane we wrzącej wodzie przez kilka sekund, następnie ponownie zamrożone. Woda po kąpaniu wiśni (około 800ml) została użyta do nastawu i uzupełniona kranówką.",
			"Zapomniałem o fermentującym miodzie, w związku z czym cicha trwała ponad dwa miesiące zamiast zakładanego jednego.",
			"Przy zlewaniu do leżakowania nie udało się oddzielić miodu od drożdży. Niestety, wiśnie utrudniły dekantację i większość drożdży wylądowała w leżaku. Rozważę ponowną dekantację gdy drożdże opadną na dno, a miód się nieco sklaruje.",
			"Drożdże opadły na dno już następnego dnia po zlaniu z cichej, więc pozwoiłem sobie od razu oddzielić je. Miód bez warstwy drożdży będzie leżakował w piwnicy przez następnych kilka miesięcy."
	],
	tastingNotes: [
		"Idealnie klarowny, ciemnoróżowy. Prezentuje się świetnie. Aromat słodkawy, miodowy, ale z ciekawą nutką czerwonych owoców w tle, lekko przypomina Flanders Red Ale albo Krieka. W smaku dość słodko, ale jest bardzo fajny balans miodu i wytrawności. Wiśnie wnoszą wyraźny, acz nienachalny i niedominujący owocowy posmak. Leciutkie alkoholowe rozgrzewanie, ale bez pieczenia. Genialny miód."
	]
};
