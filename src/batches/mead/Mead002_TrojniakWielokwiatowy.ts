import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Mead002: IBatchConfig = {
	name: "Gra Endera",
	style: Style.TROJNIAK,
	tags: [
		Tag.TROJNIAK,
		Tag.NIESYCONY,
		Tag.WIELOKWIATOWY
	],
	batch: 2,
	version: 1,
	status: Status.PUBLISHED,
	rating: 4,
	stock: 0,
	params: {
		actual: {
			og: 36,
			volume: 2.4,
			fg: 14.5,
			abv: 13.9,
			attenuation: 59.7
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(1.2, Unit.KILOGRAM)).miodWielokwiatowy.bartnik.build(),
			new MashIngredientBuilder(new Amount(0.2, Unit.GRAM)).pozywkaWin.build()
		],
		boil: [],
		yeast: [
			new YeastIngredientBuilder(new Amount(10, Unit.GRAM)).dryYeast.name("Mead Yeast Trójniak (Starowar)").build()
		],
		secondary: [],
		boil2: []
	},
	water: {
		profile: "kranówka",
		base: new TapWater2018(1.7),
		ions: TapWater2018.ions
	},
	mash: [],
	fermentation: [
		{
			date: "2018-11-05",
			step: BrewStep.FERM_PRIMARY,
			volume: 2.4,
			sg: 36,
			pinned: true
		},
		{
			date: "2018-11-19",
			step: BrewStep.FERM_SECONDARY,
			volume: 2.3,
			sg: 12.5,
			pinned: true
		},
		{
			date: "2019-01-27",
			step: BrewStep.FERM_AGING,
			volume: 2.25,
			sg: 15
		},
		{
			date: "2020-05-24",
			step: BrewStep.BOTTLING,
			volume: 2.2,
			sg: 14.5
		}
	],
	notes: [
		"Jeden z dwóch trójniaków, wersja podstawowa - najprostszy miód wielokwiatowy, bez udziwnień.",
		"Zapomniałem o fermentującym miodzie, w związku z czym cicha trwała ponad dwa miesiące zamiast zakładanego jednego.",
		"Przy zlewaniu do leżaka wyszło większe Blg niż przed cichą. Podejrzewam błąd pomiaru.",
		"Przy butelkowaniu Blg zbliżone do tego przy zlewaniu do leżaka, więc najprawdopodobniej błąd popełniłem przy pomiarze podczas zlewania na cichą."
	],
	tastingNotes: [
		"Klarowny, złocisty, wygląda bardzo apetycznie. Aromat wyraźnie miodowy, smak również, słodki, z niewyczywalnym alkoholem. Smakuje jak dwójniak. Bardzo dobry."
	]
};
