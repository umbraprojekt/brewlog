import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Mead004: IBatchConfig = {
	name: "Nigdziebądź",
	style: Style.CZWORNIAK,
	tags: [
		Tag.CZWORNIAK,
		Tag.SYCONY,
		Tag.WIELOKWIATOWY
	],
	batch: 4,
	version: 1,
	status: Status.PUBLISHED,
	rating: 0,
	stock: 2.6,
	params: {
		actual: {
			og: 24.5,
			volume: 2.7,
			fg: -1,
			abv: 14.5,
			attenuation: 104.1
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).miodWielokwiatowy.cd.build(),
			new MashIngredientBuilder(new Amount(0.2, Unit.GRAM)).pozywkaWin.build()
		],
		boil: [],
		yeast: [
			new YeastIngredientBuilder(new Amount(10, Unit.GRAM)).dryYeast.name("Mead Yeast Trójniak (Starowar)").build()
		],
		secondary: [],
		boil2: []
	},
	water: {
		profile: "kranówka",
		base: new TapWater2018(2),
		ions: TapWater2018.ions
	},
	mash: [],
	fermentation: [
		{
			date: "2019-02-24",
			step: BrewStep.FERM_PRIMARY,
			volume: 2.7,
			sg: 24.5,
			pinned: true
		},
		{
			date: "2019-04-09",
			step: BrewStep.FERM_SECONDARY,
			volume: 2.7,
			sg: -0.5,
			pinned: true
		},
		{
			date: "2019-09-22",
			step: BrewStep.BOTTLING,
			volume: 2.6,
			sg: -1
		}
	],
	notes: [
		"Pierwsze podejście do miodu syconego. Czwórniak, żeby wymagał jak najkrótszego leżakowania.",
		"Kompletnie zapomniałem o tym miodzie podczas fermentacji burzliwej. Podejrzewam, że w momencie zlewania na cichą, fermentacja jest już de facto zakończona.",
		"Próbka przy butelkowaniu: smakuje jak białe wino. Wytrawny do kości, zero słodyczy czy śladów tego, że to miód. Alkohol rozgrzewający, ale nie przeszkadzał. Miód wydaje się niezły, chociaż to nie to, czego się spodziewałem."
	],
	tastingNotes: []
};
