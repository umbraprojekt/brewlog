import { Type } from "../../enums/Type";
import { IBatch } from "../../types/IBatch";
import { Mead001 } from "./Mead001_DwojniakAkacjowy";
import { Mead002 } from "./Mead002_TrojniakWielokwiatowy";
import { Mead003 } from "./Mead003_TrojniakWielokwiatowyZWisnia";
import { Mead004 } from "./Mead004_CzworniakWielokwiatowy";
import { Mead005 } from "./Mead005_TrojniakAkacjowyZWisnia";
import { Mead006 } from "./Mead006_TrojniakAkacjowyZPorzeczka";

export const batches: Array<IBatch> = [
	Mead001,
	Mead002,
	Mead003,
	Mead004,
	Mead005,
	Mead006
]
	.map(batch => ({
		...batch,
		type: Type.MEAD,
		prev: undefined,
		next: undefined,
		url: `/miody/${batch.batch}`
	}))
	.sort((a, b) => b.batch - a.batch);

export const Meads: Array<IBatch> = batches.map(batch => {
	batch.prev = batches.find(item => item.batch === batch.batch - 1);
	batch.next = batches.find(item => item.batch === batch.batch + 1);
	return batch;
});
