import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Mead001: IBatchConfig = {
	name: "Ubik",
	style: Style.DWOJNIAK,
	tags: [
		Tag.DWOJNIAK,
		Tag.NIESYCONY,
		Tag.AKACJOWY
	],
	batch: 1,
	version: 1,
	status: Status.PUBLISHED,
	rating: 0,
	stock: 0,
	params: {
		actual: {
			volume: 1.7,
			og: 50
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(1.2, Unit.KILOGRAM)).miodAkacjowy.bartnik.build(),
			new MashIngredientBuilder(new Amount(0.2, Unit.GRAM)).pozywkaWin.build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(10, Unit.GRAM)).dryYeast.name("Mead Yeast Dwójniak (Starowar)").build()
		]
	},
	water: {
		profile: "kranówka",
		base: new TapWater2018(0.85),
		ions: TapWater2018.ions
	},
	mash: [],
	fermentation: [
		{
			date: "2018-08-21",
			step: BrewStep.FERM_PRIMARY,
			volume: 1.7,
			pinned: true
		},
		{
			date: "2018-09-04",
			step: BrewStep.FERM_SECONDARY,
			volume: 1.5,
			pinned: true
		},
		{
			date: "2018-11-19",
			step: BrewStep.FERM_AGING,
			volume: 1.3
		}
	],
	notes: [
		"Miód kupiony w supermarkecie na próbę. Zobaczymy, co z tego wyjdzie.",
		"Odczytu gęstości początkowej nie dokonałem, bo nie miałem jeszcze refraktometru z wystarczającą skalą. Prawdopodobnie okolice 50°Blg; tej wartości będę się trzymał przy obliczaniu zawartości alkoholu.",
		"Na cichą zlany do gąsiora 2l i przeniesiony do piwnicy. Niech sobie leżakuje."
	],
	tastingNotes: []
};
