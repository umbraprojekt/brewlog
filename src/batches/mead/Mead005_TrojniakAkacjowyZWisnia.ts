import { BrewStep } from "../../enums/BrewStep";
import { Producer } from "../../enums/Producer";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";
import { TapWater202004 } from "../../ingredients/water/TapWater202004";

export const Mead005: IBatchConfig = {
	name: "Belgariada",
	style: Style.MELOMEL_TROJNIAK,
	tags: [
		Tag.TROJNIAK,
		Tag.NIESYCONY,
		Tag.MELOMEL,
		Tag.AKACJOWY,
		Tag.WISNIA
	],
	batch: 5,
	version: 1,
	status: Status.PINNED,
	rating: 0,
	stock: 0,
	params: {
		actual: {
			og: 34,
			volume: 3
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(1.4, Unit.KILOGRAM)).miodAkacjowy.bartnik.build(),
			new MashIngredientBuilder(new Amount(0.25, Unit.GRAM)).pozywkaWin.build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(5, Unit.GRAM)).dryYeast.name("Mead Yeast Trójniak (Starowar)").build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(600, Unit.GRAM), new Amount(30, Unit.DAY)).fruit.producer(Producer.HORTEX).name("wiśnie mrożone").build()
		]
	},
	water: {
		profile: "kranówka",
		base: new TapWater202004(2),
		ions: TapWater202004.ions
	},
	mash: [],
	fermentation: [
		{
			date: "2020-05-29",
			step: BrewStep.FERM_PRIMARY,
			volume: 3,
			sg: 34,
			pinned: true
		}
	],
	notes: [
		"Miała być powtórka #3, ale nie udało mi się kupić dobrego miodu wielokwiatowego, wziąłem więc akacjowy."
	],
	tastingNotes: []
};
