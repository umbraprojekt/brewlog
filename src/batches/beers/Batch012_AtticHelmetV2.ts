import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch012: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Attic Helmet",
	tags: [
		Tag.APA,
		Tag.CHMIEL_NOWOFALOWY,
		Tag.SINGLE_HOP,
		Tag.PSZENICA
	],
	version: 2,
	original: 3,
	batch: 12,
	style: Style.APA,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 28,
			og: 12.5,
			fg: 2.6,
			ibu: 40,
			ebc: 14,
			abv: 5.3,
			efficiency: 70,
			carbonation: 2.1,
			attenuation: 79.1
		},
		actual: {
			volume: 27.3,
			og: 13.5,
			fg: 2.25,
			abv: 6.1,
			efficiency: 73.3,
			attenuation: 83.3
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(6, Unit.KILOGRAM)).pale.strzegom.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).wheat.strzegom.build(),
			new MashIngredientBuilder(new Amount(25, Unit.GRAM)).barwiacy.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(60, Unit.MINUTE)).warrior.hopData(2015, 17).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(20, Unit.MINUTE)).warrior.hopData(2015, 17).build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(7, Unit.MINUTE)).warrior.hopData(2015, 17).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).us05.dryYeast.build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(21, Unit.DAY)).warrior.hopDry.hopData(2015, 17).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				note: "Dodać słód pale ale i pszeniczny",
				volume: 20,
				temperature: 72
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 66,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				note: "Dodać słód barwiący",
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 20,
				temperature: 78,
				estimated: {
					sg: 11.3,
					volume: 32.1
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-07-28",
			step: BrewStep.WORT_FIRST,
			sg: 13.6,
			volume: 28
		},
		{
			date: "2016-07-28",
			step: BrewStep.WORT_BOILED,
			sg: 14.7,
			volume: 25,
			note: "Rozcieńczona 2,3l wody"
		},
		{
			date: "2016-07-28",
			step: BrewStep.FERM_PRIMARY,
			sg: 13.5,
			volume: 27.3,
			pinned: true
		},
		{
			date: "2016-08-11",
			step: BrewStep.FERM_SECONDARY,
			sg: 3.25,
			volume: 27,
			pinned: true
		},
		{
			date: "2016-09-03",
			step: BrewStep.BOTTLING,
			sg: 2.25,
			volume: 26,
			note: "Refermentacja: 120g cukru"
		}
	],
	notes: [
		"Powtórka warki #3, warzona w całości dla kolegów i koleżanek z pracy.",
		"Prawdopodobnie dodałem do piwa 1kg słodu pale ale za dużo, przez co ostatecznie wyszło 25 litrów ekstraktywnego piwa. Musiałem rozcieńczyć do 13,5°Blg, żeby zmieścić się w widełkach dla APA i odtworzyć ekstrakt z warki #3.",
		"Piwo rozdystrybuowane w jedną noc podczas imprezy integracyjnej. Zebrało bardzo pochlebne opinie."
	],
	tastingNotes: [
		"Smaczne, ponownie orzechowe, choć na szczęście mniej niż w warce #3. Wyraźnie owocowe, ananasowe. Przyjemne, choć moim zdaniem mogłoby być ciut lżejsze; 13,5°Blg to jednak za dużo jak na ten styl."
	]
};
