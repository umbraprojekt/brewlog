import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch047: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Leghorn",
	tags: [
		Tag.KWAS,
		Tag.PSZENICA,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 47,
	style: Style.BERLINER_WEISSE,
	stock: 6,
	rating: 4,
	params: {
		planned: {
			volume: 10,
			og: 8,
			fg: 1.8,
			ibu: 8,
			ebc: 4,
			abv: 3.2,
			efficiency: 73,
			carbonation: 2.6,
			attenuation: 77.1
		},
		actual: {
			volume: 10,
			og: 8,
			fg: 2,
			efficiency: 72.9,
			attenuation: 75,
			abv: 3.1
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).pils.bestmalz.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).wheat.bestmalz.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(15, Unit.MINUTE)).tettnang.hopData(2018, 2.8).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(10, Unit.BCFU)).ibs.build(),
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).us05.dryYeast.build()
		]
	},
	water: {
		profile: "Jasne/Słodowe",
		base: new DistilledWater(14.5),
		additions: {
			CaSO4: 0.5,
			NaCl: 0.6,
			MgSO4: 1.1,
			CaCl2: 2.2,
			CaCO3: 0.6
		},
		ions: {
			Ca: 65,
			Mg: 7.5,
			Na: 15,
			SO4: 50,
			Cl: 96,
			HCO3: 23
		}
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 5,
				temperature: 72.1
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 9.5,
				temperature: 78,
				estimated: {
					sg: 7.4,
					volume: 11.9
				}
			}
		]
	],
	boilTime: 15,
	fermentation: [
		{
			date: "2019-06-08",
			step: BrewStep.WORT_FIRST,
			sg: 7.9,
			volume: 11,
			note: "Obniżone pH do 4,5 kwasem mlekowym. Zadane bakterie: 10 kapsułek Sanprobi IBS. Odstawione na 48 godzin do przefermentowania."
		},
		{
			date: "2019-06-10",
			step: BrewStep.WORT_BOILED,
			sg: 8,
			volume: 10
		},
		{
			date: "2019-06-10",
			step: BrewStep.FERM_PRIMARY,
			sg: 8,
			volume: 10,
			pinned: true
		},
		{
			date: "2019-07-06",
			step: BrewStep.BOTTLING,
			sg: 2,
			volume: 10,
			note: "Refermentacja: 60 g cukru"
		}
	],
	notes: [
		"Kettle sour: po wysładzaniu, szybkim podgrzaniu do 80°C i schłodzeniu, obniżyłem pH brzeczki do 4,5 kwasem mlekowym, zadałem bakterie i odstawiłem do fermentacji w zapieczętowanym garze na 48 godzin w temperaturze 30°C, następnie zagotowałem dodając chmiel, schłodziłem i zadałem drożdże. Nie mierzyłem pH po fermentacji mlekowej.",
		"Chciałem zrobić cichą, ale życie napisało mi inny scenariusz i nie byłem w stanie na czas wykorzystać gęstwy, którą bym przy okazji zebrał. Żeby zatem nie marnować drożdży, postanowiłem przefermentować piwo bez przelewania na cichą, a gęstwę zebrać przy rozlewie.",
		"Próbka przy rozlewie bardzo smaczna. Kwaśność wyraźna, ale nie przesadzona. Zapowiada się niesamowicie orzeźwiające piwo."
	],
	tastingNotes: [
		"Barwa jasnosłomkowa, zamglone, niemal całkowity brak piany. Byłoby przyjemnie, gdyby trochę piany się pojawiło, ale poza tym poprawnie.",
		"Aromat przypomina babkę cytrynową. Jest mocna słodowość, jest fajna, czysta nuta kwaskowatości, do tego trochę drożdży (zakładam, że z czasem się to ułoży, ale piłem ledwie kilka dni po rozlewie). Nieskomplikowanie, ale rześko i przyjemnie.",
		"W smaku jest to samo, co i w aromacie, a więc słodowość, przypominająca ciasto drożdżowe i przywodząca na myśl bawarskiego Weizena (tylko bez banana i goździka), a wszystko to podszyte nieagresywną, rześką nutką cytrynowej kwaśności. Kwas jest jednak na drugim planie. Smaczne, choć nie pogardziłbym minimalnie silniejszym kwasem.",
		"Wysycenie niskie, pasujące raczej do bittera, ale ponownie: piłem krótko po rozlewie i na pewno jeszcze się to poprawi.",
		"Piwo koniecznie muszę powtórzyć. Jedyne, co bym zmienił, to wydłużenie czasu fermentacji mlekowej z dwóch do trzech dni, żeby uzyskać trochę wyraźniejszą kwaśność."
	]
};
