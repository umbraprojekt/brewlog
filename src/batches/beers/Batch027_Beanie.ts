import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch027: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Beanie",
	tags: [
		Tag.IPA,
		Tag.PSZENICA,
		Tag.SINGLE_HOP,
		Tag.CHMIEL_NOWOFALOWY
	],
	version: 1,
	batch: 27,
	style: Style.NEIPA,
	rating: 2,
	stock: 0,
	params: {
		planned: {
			volume: 23,
			og: 15.1,
			fg: 3.6,
			ibu: 49,
			ebc: 11,
			abv: 6.4,
			efficiency: 70,
			carbonation: 2,
			attenuation: 76.6
		},
		actual: {
			volume: 24,
			og: 16.3,
			fg: 3.5,
			abv: 7.1,
			efficiency: 80.6,
			attenuation: 78.6
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(6, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).wheat.bestmalz.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(20, Unit.MINUTE)).jarrylo.hopData(2015, 13.3).build(),
			new WetIngredientBuilder(new Amount(70, Unit.GRAM), new Amount(7, Unit.MINUTE)).jarrylo.hopData(2015, 13.3).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(500, Unit.MILLILITRE)).fm10.yeastCakeFrom(26).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(100, Unit.GRAM), new Amount(14, Unit.DAY)).jarrylo.hopDry.hopData(2015, 13.3).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 22,
				temperature: 72.4
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 12
			},
			{
				step: BrewStep.SPARGE,
				volume: 12.9,
				temperature: 78,
				estimated: {
					sg: 13.6,
					volume: 26.9
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2017-04-23",
			step: BrewStep.WORT_FIRST,
			sg: 14.2,
			volume: 28.5
		},
		{
			date: "2017-04-23",
			step: BrewStep.WORT_BOILED,
			sg: 16.3,
			volume: 24
		},
		{
			date: "2017-04-23",
			step: BrewStep.FERM_PRIMARY,
			sg: 16.3,
			volume: 24,
			pinned: true
		},
		{
			date: "2017-05-09",
			step: BrewStep.FERM_SECONDARY,
			sg: 5,
			volume: 24,
			pinned: true
		},
		{
			date: "2017-05-23",
			step: BrewStep.BOTTLING,
			sg: 3.5,
			volume: 23,
			note: "Refermentacja: 90g cukru"
		}
	],
	notes: [
		"Nie dość mętne. Mało słodu pszenicznego. Może trzeba było nasypać płatków?"
	],
	tastingNotes: [
		"Klarowne. W aromacie - zwykła IPA, w smaku trochę gryzące. Chmiel moim zdaniem nijaki, niewyróżniający się niczym szczególnym na tle innych chmieli nowofalowych. Takie sobie."
	]
};
