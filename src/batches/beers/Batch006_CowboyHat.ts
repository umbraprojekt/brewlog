import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch006: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Cowboy Hat",
	tags: [
		Tag.PSZENICA,
		Tag.SINGLE_HOP,
		Tag.BOCK,
		Tag.WEIZENBOCK
	],
	version: 1,
	batch: 6,
	style: Style.WEIZENBOCK,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			volume: 17,
			og: 18.6,
			ibu: 23,
			ebc: 44,
			efficiency: 70,
			carbonation: 2
		},
		actual: {
			volume: 18,
			og: 18.5,
			fg: 3,
			abv: 9,
			efficiency: 70.9,
			attenuation: 84.7
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(3, Unit.KILOGRAM)).wheat.strzegom.build(),
			new MashIngredientBuilder(new Amount(3, Unit.KILOGRAM)).vienna.strzegom.build(),
			new MashIngredientBuilder(new Amount(0.25, Unit.KILOGRAM)).caramelWheat.strzegom.build(),
			new MashIngredientBuilder(new Amount(75, Unit.GRAM)).barwiacy.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(60, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(30, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(15, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(5, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).wb06.yeastCakeFrom(5).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 19,
				temperature: 48,
				note: "Dodać tylko słody pszeniczne",
			},
			{
				step: BrewStep.REST_FERULIC,
				temperature: 44,
				time: 20
			},
			{
				step: BrewStep.REST_PROTEIN,
				temperature: 55,
				time: 20
			},
			{
				step: BrewStep.REST_MALTOSE,
				note: "Dodać słód wiedeński",
				temperature: 64,
				time: 45
			},
			{
				step: BrewStep.REST_DEXTRINE,
				temperature: 72,
				time: 45
			},
			{
				step: BrewStep.MASHOUT,
				note: "Dodać słód barwiący",
				temperature: 78,
				time: 5
			},
			{
				step: BrewStep.SPARGE,
				volume: 15,
				temperature: 78
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-05-12",
			step: BrewStep.FERM_PRIMARY,
			sg: 18.5,
			volume: 18,
			pinned: true
		},
		{
			date: "2016-06-15",
			step: BrewStep.BOTTLING,
			sg: 3,
			volume: 18,
			note: "Refermentacja: 80g cukru"
		}
	],
	notes: [
		"Kiedyś powtórzę, ale z lepszymi drożdżami (płynnymi) i bez tych bezsensownych przerw, zwłaszcza podziału na maltozową i dekstrynującą."
	],
	tastingNotes: [
		"Wyszło bardzo wytrawnie, aż za wytrawnie, choć alkohol nie był zbyt gryzący. Charakter koźlaka zachowany, zaś drożdże nie wniosły nic.",
		"Po nieco ponad roku przypadkiem znalazłem w piwnicy kilka zapodzianych butelek. Piwo okazało się idealnie klarowne i bardzo przyjemnie, owocowo utlenione. Po prostu pyszne, pomimo, że raczej w kategorii koźlaka niż Weizenbocka."
	]
};
