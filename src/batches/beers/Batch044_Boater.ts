import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch044: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Boater",
	tags: [
		Tag.PSZENICA,
		Tag.OWIES,
		Tag.SINGLE_HOP,
		Tag.RUMIANEK,
		Tag.KOLENDRA,
		Tag.CURACAO,
		Tag.WITBIER,
		Tag.BELGIAN
	],
	version: 1,
	batch: 44,
	style: Style.WITBIER,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			volume: 20.5,
			og: 11.1,
			fg: 2.1,
			ibu: 14,
			ebc: 6,
			abv: 4.8,
			efficiency: 70,
			carbonation: 2.6,
			attenuation: 80.7
		},
		actual: {
			og: 11.5,
			fg: 2.5,
			efficiency: 75.6,
			attenuation: 78.2,
			volume: 21.4,
			abv: 4.8
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).pils.castle.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).wheatUnmalted.viking.build(),
			new MashIngredientBuilder(new Amount(0.3, Unit.KILOGRAM)).oatFlakes.build(),
			new MashIngredientBuilder(new Amount(100, Unit.GRAM)).acidulating.bestmalz.build(),
			new MashIngredientBuilder(new Amount(0.25, Unit.KILOGRAM)).speltHull.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(60, Unit.MINUTE)).tettnang.hopData(2017, 3.1).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(7, Unit.MINUTE)).tettnang.hopData(2017, 3.1).build(),
			new WetIngredientBuilder(new Amount(7, Unit.GRAM), new Amount(5, Unit.MINUTE)).camomile.build(),
			new WetIngredientBuilder(new Amount(17, Unit.GRAM), new Amount(5, Unit.MINUTE)).coriander.build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(5, Unit.MINUTE)).curacao.build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).fm20.yeastStarter.build()
		]
	},
	water: {
		profile: "Antwerpia",
		base: new TapWater2018(5.2),
		additions: {
			CaSO4: 5,
			NaCl: 1,
			CaCl2: 2.5,
			CaCO3: 1.5
		},
		dilution: new DistilledWater(25),
		ions: {
			Ca: 94.3,
			Mg: 1.2,
			Na: 20.3,
			SO4: 100.8,
			Cl: 66.8,
			HCO3: 79.7
		}
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 18,
				temperature: 69
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 64,
				time: 120
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 12.2,
				temperature: 78,
				estimated: {
					sg: 9.7,
					volume: 24.8
				}
			}
		]
	],
	boilTime: 75,
	fermentation: [
		{
			date: "2018-06-10",
			step: BrewStep.WORT_FIRST,
			sg: 10.5,
			volume: 24.3,
			note: "Rozcieńczona 2l wody"
		},
		{
			date: "2018-06-10",
			step: BrewStep.WORT_BOILED,
			sg: 11.5,
			volume: 21.4
		},
		{
			date: "2018-06-10",
			step: BrewStep.FERM_PRIMARY,
			sg: 11.5,
			volume: 21.4,
			pinned: true
		},
		{
			date: "2018-07-01",
			step: BrewStep.FERM_SECONDARY,
			sg: 3,
			volume: 21.4,
			pinned: true
		},
		{
			date: "2018-07-15",
			step: BrewStep.BOTTLING,
			sg: 2.5,
			volume: 21.2,
			note: "Refermentacja: 135g cukru"
		}
	],
	notes: [
		"W przeddzień warzenia zalałem łuskę orkiszową wrzątkiem, zagotowałem kilka minut, po czym odcedziłem wodę. Powtórzyłem trzykrotnie (aż woda przestała się barwić). Zostawiłem łuskę w wodzie przez noc.",
		"Pszenicę i owies wrzuciłem najpierw do około 12l wody i zagotowałem przez 15 minut w celu skleikowania. Dodałem od razu wygotowane poprzedniego dnia łuski orkiszowe.",
		"Nie wiem, czego się spodziewałem, ale z pszenicy i owsa  wyszedł gęsty glut (nauczka na przyszłość: intensywnie mieszać pszenicę, bo przykleja się do dna garnka i może się przypalić). Z oryginalnych 14 litrów wody do zacierania zrobiłem 18, a ilość wody do wysładzania zmniejszyłem o 4 litry.",
		"Zacier miał pH 5,1 (korekta z 5,4 odrobinką kwasu mlekowego)."
	],
	tastingNotes: [
		"Lekko zamglone, ale w zasadzie dość mocno się sklarowało. Piana umiarkowana, biała, przy pierwszym nalaniu nietrwała. Przy drugim jest lepiej, ale wtedy zbiła się w dość grube, niekoniecznie ładne bąble.",
		"Bardzo intensywny aromat, złożony. Jest dominująca guma Juicy Fruit i delikatny pieprz - to zapewne wniosły drożdże. Z tyłu czuć dodatki: kolendrę i pomarańczę. Rumianek albo się nie pojawia, albo jest kompletnie przykryty przez gumę.",
		"W smaku w zasadzie podobnie: guma daje wrażenie dużej słodyczy, za nią pojawia się leciutki fenol i pomarańcza.",
		"Lekkie, ale zdecydowanie nie wodniste. Wręcz gęste jak na ten ekstrakt. Wysycenie umiarkowane, przyjemne; lekko musuje w ustach.",
		"Zupełnie się nie spodziewałem takiego efektu; mam wrażenie, że drożdże naprodukowały za dużo estrów. Fakt, że piwo fermentowało w 22-24 stopniach, a więc w temperaturach raczej wysokich, zakładałem jednak, że Witbier to lubi. Chyba niekoniecznie. Piwo ciekawe, smaczne i rześkie, ale moim zdaniem guma do żucia jest zbyt dominująca."
	]
};
