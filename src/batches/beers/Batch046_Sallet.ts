import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch046: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Sallet",
	tags: [
		Tag.WEDZONE,
		Tag.SINGLE_MALT,
		Tag.REITERATED_MASH
	],
	version: 1,
	batch: 46,
	style: Style.DOUBLE_SMOKED_ALE,
	rating: 2,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 21.3,
			fg: 6,
			ibu: 30,
			ebc: 14,
			abv: 8.7,
			efficiency: 70,
			carbonation: 2.4,
			attenuation: 71.4
		},
		actual: {
			volume: 20.1,
			og: 22.5,
			fg: 5.5,
			abv: 9.8,
			efficiency: 74.6,
			attenuation: 75.5
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(9, Unit.KILOGRAM)).smokedBeech.viking.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(60, Unit.MINUTE)).iunga.hopData(2017, 9).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(20, Unit.MINUTE)).iunga.hopData(2017, 9).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(10, Unit.MINUTE)).irishMoss.build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(10, Unit.MINUTE)).nutrient.build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(7, Unit.MINUTE)).marynka.hopData(2017, 6.8).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).us05.yeastCake.build()
		]
	},
	water: {
		profile: "kranówka",
		base: new TapWater2018(33.8),
		ions: TapWater2018.ions
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 15,
				temperature: 75,
				note: "Użyć połowy słodu"
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 69,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 18.8,
				temperature: 78
			}
		],
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 15,
				temperature: 75,
				note: "Użyć drugiej połowy słodu"
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 69,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 12.8,
				temperature: 78,
				estimated: {
					sg: 19,
					volume: 23.8
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2018-11-17",
			step: BrewStep.WORT_FIRST_SPARGE_1,
			sg: 10.7,
			volume: 27.8
		},
		{
			date: "2018-11-17",
			step: BrewStep.WORT_FIRST_SPARGE_2,
			sg: 20.4,
			volume: 23
		},
		{
			date: "2018-11-17",
			step: BrewStep.WORT_BOILED,
			sg: 22.5,
			volume: 20.1
		},
		{
			date: "2018-11-17",
			step: BrewStep.FERM_PRIMARY,
			sg: 22.5,
			volume: 20.1,
			pinned: true
		},
		{
			date: "2018-11-30",
			step: BrewStep.FERM_SECONDARY,
			sg: 6.5,
			volume: 19.5,
			pinned: true
		},
		{
			date: "2018-12-16",
			step: BrewStep.BOTTLING,
			sg: 5.5,
			volume: 19,
			note: "Refermentacja: 108g cukru"
		}
	],
	notes: [
		"Z uwagi na niezadowalającą jakość słodu (miał być wędzony torfem, ale pachnie wędzonym boczkiem, więc mam przypuszczenia, że ktoś się gdzieś pomylił i sprzedano mi słód wędzony bukiem; reklamacji nie uznano), postanowiłem uwarzyć takie nieplanowane piwo wzorowane na niemieckim Doppelbocku, ale jasne i fermentowane drożdżami górnej fermentacji ze względu na brak lodówki w moim wyposażeniu.",
		"Planowany profil wody: Monachium, ale użyję po prostu kranówki. Nie modyfikuję wody z kranu po trosze z lenistwa; jej profil jest wystarczająco bliski monachijskiemu, żeby nie przejmować się zanadto modyfikacjami wody.",
		"Test metody zacierania \"reiterated mash\".",
		"Gęstwa użyczona przez Browar Miejski Sopot."
	],
	tastingNotes: [
		"Barwa złocista, piwo kryształowo klarowne. Piana biała, na dwa palce, ale niezbyt trwała, szybko redukuje się do cieniutkiej warstewki. Wygląda jak rasowy koncernowy lager.",
		"Aromat wędzony, ale nie przytłaczający. Przebija się słodkawa słodowość. Charakter wędzonki jest raczej dymny, co troszkę mnie dziwi, bo sam słód wydawał się wędzony bukiem. Teraz mam wątpliwości. Lizolu i antyseptyków nadal nie ma, więc nadal nie uważam, że to torf.",
		"W smaku jest słodkawo, słodowo z wyczuwalną nutą alkoholową. Nic specjalnego. Sporo ciała, wysycenie średnie w stronę wysokiego (zgodnie z planem).",
		"Ogólnie piwo jest do szybkiego rozdania jako ciekawostka lub surowiec na szybkie zrycie bani. Nie oferuje żadnych wrażeń sensorycznych, jest proste i jednowymiarowe. Po prostu nudne."
	]
};
