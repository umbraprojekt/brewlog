import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch011: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Kippah",
	tags: [
		Tag.MALE_PIWO,
		Tag.EKSTRAKT,
		Tag.SINGLE_HOP,
		Tag.IPA,
		Tag.CHMIEL_NOWOFALOWY
	],
	version: 1,
	batch: 11,
	style: Style.AIPA,
	rating: 5,
	stock: 0,
	params: {
		planned: {
			ibu: 96,
			carbonation: 2.2
		},
		actual: {
			volume: 13.5,
			og: 14.5,
			fg: 4.25,
			abv: 5.6
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(13, Unit.LITRE)).wort.name("brzeczka z wysłodzin z warki #10, 8°Blg").build(),
			new MashIngredientBuilder(new Amount(1.7, Unit.KILOGRAM)).lightLE.bruntal.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(60, Unit.MINUTE)).equinox.hopData(2015, 13.9).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(20, Unit.MINUTE)).equinox.hopData(2015, 13.9).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).equinox.hopData(2015, 13.9).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).us05.dryYeast.build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(14, Unit.DAY)).equinox.hopDry.hopData(2015, 13.9).build()
		]
	},
	mash: [],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-07-01",
			step: BrewStep.WORT_BOILED,
			sg: 12,
			volume: 15
		},
		{
			date: "2016-07-01",
			step: BrewStep.FERM_PRIMARY,
			sg: 14.5,
			volume: 13.5,
			pinned: true
		},
		{
			date: "2016-07-15",
			step: BrewStep.FERM_SECONDARY,
			sg: 4.5,
			volume: 13.5,
			pinned: true
		},
		{
			date: "2016-07-29",
			step: BrewStep.BOTTLING,
			sg: 4.25,
			volume: 12.5,
			note: "Refermentacja: 60g cukru"
		}
	],
	notes: [
		"Warka nieplanowana, ale z warki #10 została całkiem ekstraktywna brzeczka, więc wystarczyło ją dosłodzić ekstraktem i AIPA gotowa."
	],
	tastingNotes: [
		"Zaskoczenie: piwo wyszło wyśmienite! Delikatna biszkoptowo-karmelowa słodowość z piękną, intensywną owocowością od chmielu Equinox. Wysoka, ale bardzo przyjemna goryczka. Może kiedyś spróbuję to odtworzyć z normalnym zacieraniem."
	]
};
