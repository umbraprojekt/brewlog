import { BrewStep } from "../../enums/BrewStep";
import { IngredientType } from "../../enums/IngredientType";
import { Producer } from "../../enums/Producer";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { Ingredient } from "../../ingredients/Ingredient";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch007: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Leprechaun Hat",
	tags: [
		Tag.STOUT,
		Tag.KAWA
	],
	version: 1,
	batch: 7,
	style: Style.COFFEE_STOUT,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 24,
			og: 12.4,
			ibu: 37,
			ebc: 94,
			efficiency: 70,
			carbonation: 1.7
		},
		actual: {
			volume: 24,
			og: 13,
			fg: 4.5,
			abv: 4.7,
			efficiency: 70.8,
			attenuation: 66.3
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).pale.strzegom.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramel600.strzegom.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).roastedBarley.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(60, Unit.MINUTE)).admiral.hopData(2015, 14.2).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(30, Unit.MINUTE)).admiral.hopData(2015, 14.2).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(15, Unit.MINUTE)).fuggle.hopData(2015, 4.2).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(5, Unit.MINUTE)).fuggle.hopData(2015, 4.2).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).dryYeast.s04.build()
		],
		bottling: [
			new Ingredient(IngredientType.FLAVOUR, Producer.NONE, "kawa Nicaragua Flores del Café", new Amount(25, Unit.GRAM))
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 18,
				temperature: 70,
				note: "Dodać słód pale ale i karmelowy",
			},
			{
				step: BrewStep.REST_MALTOSE,
				temperature: 63,
				time: 40
			},
			{
				step: BrewStep.REST_DEXTRINE,
				temperature: 72,
				time: 20
			},
			{
				step: BrewStep.MASHOUT,
				note: "Dodać prażony jęczmień",
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 18.5,
				temperature: 78
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-05-20",
			step: BrewStep.FERM_PRIMARY,
			sg: 13,
			volume: 24,
			pinned: true
		},
		{
			date: "2016-06-06",
			step: BrewStep.FERM_SECONDARY,
			sg: 5,
			pinned: true
		},
		{
			date: "2016-06-20",
			step: BrewStep.BOTTLING,
			sg: 4.5,
			volume: 24,
			note: "Refermentacja: 80g cukru"
		}
	],
	notes: [
		"Przy butelkowaniu piwo podzieliłem na dwie połowy po 12l, do jednej dodałem kawę w postaci espresso, druga zaś pozostała bez dodatków.",
		"Kawowy stout zebrał od ludzi sporo pochwał i próśb o uwarzenie kolejnej partii."
	],
	tastingNotes: [
		"Obie wersje piwa wyszły bardzo smaczne, dość delikatne jeśli chodzi o paloność, gładkie."
	]
};
