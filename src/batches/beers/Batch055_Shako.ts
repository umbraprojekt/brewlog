import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { Water } from "../../ingredients/water/Water";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch055: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Shako",
	tags: [
		Tag.BELGIAN,
		Tag.PALE_ALE,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 55,
	style: Style.BPA,
	stock: 40,
	params: {
		planned: {
			volume: 20,
			og: 12.7,
			fg: 3,
			ibu: 25,
			ebc: 25,
			abv: 5.2,
			efficiency: 80,
			carbonation: 2.0,
			attenuation: 76.4
		},
		actual: {
			volume: 21,
			og: 12.5,
			fg: 3,
			abv: 5.1,
			efficiency: 82.8,
			attenuation: 76
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).castle.pils.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).castle.munich1.build(),
			new MashIngredientBuilder(new Amount(200, Unit.GRAM)).specialB.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(60, Unit.MINUTE)).hallertauHersbrucker.hopData(2019, 4.0).build(),
			new WetIngredientBuilder(new Amount(45, Unit.GRAM), new Amount(20, Unit.MINUTE)).hallertauHersbrucker.hopData(2019, 4.0).build(),
			new WetIngredientBuilder(new Amount(45, Unit.GRAM), new Amount(7, Unit.MINUTE)).hallertauHersbrucker.hopData(2019, 4.0).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).fm25.yeastStarter.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 13.6,
				temperature: 72.3
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 15.4,
				temperature: 78,
				estimated: {
					sg: 11.3,
					volume: 23.8
				}
			}
		]
	],
	boilTime: 60,
	water: {
		profile: "Antwerpia",
		base: new DistilledWater(29),
		additions: {
			CaSO4: 3.6,
			NaCl: 2.4,
			MgSO4: 1.2,
			CaCl2: 0.4,
			CaCO3: 3.8
		},
		ions: Water.PROFILE_ANTWERP_BEERSMITH
	},
	fermentation: [
		{
			date: "2020-04-26",
			step: BrewStep.WORT_FIRST,
			sg: 11.7,
			volume: 23
		},
		{
			date: "2020-04-26",
			step: BrewStep.WORT_BOILED,
			sg: 13.4,
			volume: 19.5
		},
		{
			date: "2020-04-26",
			step: BrewStep.FERM_PRIMARY,
			sg: 12.5,
			volume: 21,
			pinned: true,
			note: "Rozcieńczenie: 1,5 l wody destylowanej"
		},
		{
			date: "2020-05-10",
			step: BrewStep.FERM_SECONDARY,
			sg: 3.25,
			volume: 20.7,
			pinned: true
		},
		{
			date: "2020-05-24",
			step: BrewStep.BOTTLING,
			sg: 3,
			volume: 20.6,
			note: "Refermentacja: 105 g cukru"
		}
	],
	notes: [
		"Starter z przeterminowanych o kilka dni drożdży, pojemność: prawie 2 litry. 12 godzin po zadaniu drożdży oznaki intensywnej fermentacji.",
		"Próbka przy zlewaniu na cichą bardzo smaczna, z wyraźnym korzennym charakterem belgijskich drożdży. Zapowiada się piwo o dość bogatym smaku i aromacie."
	],
	tastingNotes: []
};
