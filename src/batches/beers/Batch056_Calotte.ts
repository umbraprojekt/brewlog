import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { Water } from "../../ingredients/water/Water";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch056: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Calotte",
	tags: [
		Tag.BELGIAN,
		Tag.KLASZTORNE
	],
	version: 1,
	batch: 56,
	style: Style.DUBBEL,
	stock: 0,
	params: {
		planned: {
			volume: 22,
			og: 15.4,
			fg: 2.7,
			ibu: 23,
			ebc: 28,
			abv: 7,
			efficiency: 75,
			carbonation: 2.3,
			attenuation: 82.6
		},
		actual: {
			volume: 22.7,
			og: 16,
			efficiency: 81.4,
			fg: 2.5,
			abv: 7.4,
			attenuation: 84.4
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4, Unit.KILOGRAM)).castle.pils.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).castle.munich2.build(),
			new MashIngredientBuilder(new Amount(200, Unit.GRAM)).specialB.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(60, Unit.MINUTE)).hallertauHersbrucker.hopData(2019, 4.0).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(60, Unit.MINUTE)).tettnang.hopData(2018, 3.3).build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(20, Unit.MINUTE)).hallertauHersbrucker.hopData(2019, 4.0).build(),
			new WetIngredientBuilder(new Amount(500, Unit.GRAM), new Amount(10, Unit.MINUTE)).addition.name("cukier muscovado").build(),
			new WetIngredientBuilder(new Amount(9, Unit.GRAM), new Amount(10, Unit.MINUTE)).nutrient.build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(10, Unit.MINUTE)).irishMoss.build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(7, Unit.MINUTE)).tettnang.hopData(2018, 3.3).build(),
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(200, Unit.MILLILITRE)).fm25.yeastCakeFrom(55).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 17,
				temperature: 72.2
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 15,
				temperature: 78,
				estimated: {
					sg: 12,
					volume: 25.9
				}
			}
		]
	],
	boilTime: 60,
	water: {
		profile: "Antwerpia",
		base: new DistilledWater(32),
		additions: {
			CaSO4: 4,
			NaCl: 2.7,
			MgSO4: 1.3,
			CaCl2: 0.4,
			CaCO3: 4.2
		},
		ions: Water.PROFILE_ANTWERP_BEERSMITH
	},
	fermentation: [
		{
			date: "2020-05-10",
			step: BrewStep.WORT_FIRST,
			sg: 13.4,
			volume: 25
		},
		{
			date: "2020-05-10",
			step: BrewStep.WORT_BOILED,
			sg: 16.5,
			volume: 22
		},
		{
			date: "2020-05-10",
			step: BrewStep.FERM_PRIMARY,
			sg: 16,
			volume: 22.7,
			pinned: true,
			note: "Rozcieńczenie: 0,7 l wody destylowanej"
		},
		{
			date: "2020-05-23",
			step: BrewStep.FERM_SECONDARY,
			sg: 3,
			volume: 22.1,
			pinned: true
		},
		{
			date: "2020-06-09",
			step: BrewStep.BOTTLING,
			sg: 2.5,
			volume: 21.8,
			note: "Refermentacja: 115 g cukru Demerara"
		}
	],
	notes: [
		"Na tę warkę poszedł cały mój zapas pożywki Wyeast; od następnej warki postaram się w ramach pożywki dodawać czysty siarczan cynku, bo na pożywce można się zrujnować."
	],
	tastingNotes: []
};
