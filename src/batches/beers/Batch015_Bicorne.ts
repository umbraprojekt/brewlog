import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch015: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Bicorne",
	tags: [
		Tag.ROGGENBIER,
		Tag.ZYTO,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 15,
	style: Style.ROGGENBIER,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 17,
			og: 12.5,
			fg: 2,
			ibu: 13,
			ebc: 35,
			abv: 5.6,
			efficiency: 70,
			carbonation: 2.3,
			attenuation: 84.2
		},
		actual: {
			volume: 17.1,
			og: 12.5,
			fg: 4.5,
			abv: 4.3,
			efficiency: 70.7,
			attenuation: 64
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).rye.viking.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).pils.viking.build(),
			new MashIngredientBuilder(new Amount(100, Unit.GRAM)).carafa3Special.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(60, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(20, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(7, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).fm41.liquidYeast.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				note: "Dodać słody jasne",
				volume: 13,
				temperature: 69
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 65,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				note: "Dodać słód palony",
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 13,
				temperature: 78,
				estimated: {
					sg: 10.9,
					volume: 20.6
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-10-08",
			step: BrewStep.WORT_FIRST,
			sg: 11.2,
			volume: 20.5
		},
		{
			date: "2016-10-08",
			step: BrewStep.WORT_BOILED,
			sg: 12.5,
			volume: 17.1
		},
		{
			date: "2016-10-08",
			step: BrewStep.FERM_PRIMARY,
			sg: 12.5,
			volume: 17.1,
			pinned: true
		},
		{
			date: "2016-11-08",
			step: BrewStep.BOTTLING,
			sg: 4.5,
			volume: 16.8,
			note: "Refermentacja: 85g cukru"
		}
	],
	notes: [
		"Zawartość fiolki drożdży wlałem bezpośrednio do fermentora, nie robiłem startera.",
		"Drożdże dość niemrawo się rozkręcały, nie bulkało przez całą fermentację, ale namnożyły się i zjadły co mogły. Nie przegazowało się."
	],
	tastingNotes: [
		"Całkiem smacznie, choć bardziej fenolowe niż estrowe. Oleiście gęste, zgodnie z oczekiwaniami. Ciut za ciemne jak na mój gust. Może wystarczyłoby 50g Carafy na korektę barwy?"
	]
};
