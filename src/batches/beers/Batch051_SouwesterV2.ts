import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { Water } from "../../ingredients/water/Water";

export const Batch051: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Sou'wester",
	tags: [
		Tag.MILD
	],
	version: 2,
	original: 26,
	batch: 51,
	style: Style.DARK_MILD,
	stock: 0,
	params: {
		planned: {
			volume: 24,
			og: 9.2,
			fg: 2.6,
			ibu: 18,
			ebc: 41,
			abv: 3.5,
			efficiency: 80,
			carbonation: 1.8,
			attenuation: 71.5
		},
		actual: {
			volume: 23.3,
			efficiency: 84.7,
			og: 10,
			fg: 3.5,
			attenuation: 65,
			abv: 3.5
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(3, Unit.KILOGRAM)).mild.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).crystal160.build(),
			new MashIngredientBuilder(new Amount(100, Unit.GRAM)).darkChocolate.build(),
			new MashIngredientBuilder(new Amount(50, Unit.GRAM)).roastedBarley.viking.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(60, Unit.MINUTE)).challenger.hopData(2017, 5.2).build(),
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).yeastStarter.fm10.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 12,
				temperature: 74.7,
				note: "Cały zasyp trafia do kadzi zaciernej, razem z palonym jęczmieniem."
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 69,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 20.6,
				temperature: 78,
				estimated: {
					sg: 8.3,
					volume: 27.9
				}
			}
		]
	],
	boilTime: 60,
	water: {
		profile: "Londyn (wg BeerSmith)",
		base: new DistilledWater(32.6),
		additions: {
			CaSO4: 3.1,
			NaCl: 3.5,
			MgSO4: 2.2,
			NaHCO3: 5.6,
			CaCO3: 1.9
		},
		ions: Water.PROFILE_LONDON_BEERSMITH
	},
	fermentation: [
		{
			date: "2019-11-10",
			step: BrewStep.WORT_FIRST,
			sg: 8.2,
			volume: 26.3
		},
		{
			date: "2019-11-10",
			step: BrewStep.WORT_BOILED,
			sg: 10,
			volume: 23.3
		},
		{
			date: "2019-11-10",
			step: BrewStep.FERM_PRIMARY,
			sg: 10,
			volume: 23.3,
			pinned: true
		},
		{
			date: "2019-11-26",
			step: BrewStep.FERM_SECONDARY,
			sg: 3.5,
			volume: 23.3,
			pinned: true
		},
		{
			date: "2019-12-20",
			step: BrewStep.BOTTLING,
			sg: 3.5,
			volume: 23.1,
			note: "refermentacja: 82 g cukru Demerara"
		}
	],
	notes: [
		"Nowa wersja Milda #26, tym razem praktycznie w całości na zasypie z angielskich słodów (z wyjątkiem palonego jęczmienia).",
		"Problem z nagazowaniem. Piwo przy pierwszych degustacjach praktycznie płaskie."
	],
	tastingNotes: []
};
