import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch041: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "War Bonnet",
	tags: [
		Tag.BROWN_ALE,
		Tag.CHMIEL_NOWOFALOWY,
		Tag.BEZALKOHOLOWE
	],
	version: 2,
	original: 2,
	batch: 41,
	style: Style.AMERICAN_BROWN_ALE,
	rating: 3,
	stock: 4,
	params: {
		planned: {
			volume: 20,
			og: 14.6,
			fg: 3.4,
			ibu: 50,
			ebc: 46,
			abv: 0,
			efficiency: 75,
			carbonation: 2.5,
			attenuation: 76.6
		},
		actual: {
			og: 14.5,
			efficiency: 84.4,
			volume: 22.6,
			fg: 3,
			abv: 0,
			attenuation: 79.3
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4, Unit.KILOGRAM)).pale.bestmalz.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).biscuit.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).chocolate400.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(60, Unit.MINUTE)).tomahawk.hopData(2016, 8.3).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(15, Unit.MINUTE)).irishMoss.build()
		],
		boil2: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(20, Unit.MINUTE)).willamette.hopData(2017, 4.4).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(20, Unit.MINUTE)).tomahawk.hopData(2016, 8.3).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).willamette.hopData(2017, 4.4).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(7, Unit.MINUTE)).tomahawk.hopData(2016, 8.3).build(),
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(0, Unit.MINUTE)).willamette.hopData(2017, 4.4).build(),
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).us05.yeastCakeFrom(40).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 17,
				temperature: 72.6
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 13.4,
				temperature: 78,
				estimated: {
					sg: 13,
					volume: 23.8
				}
			}
		]
	],
	boilTime: [
		60,
		150
	],
	fermentation: [
		{
			date: "2018-05-05",
			step: BrewStep.WORT_FIRST,
			sg: 13.4,
			volume: 25
		},
		{
			date: "2018-05-05",
			step: BrewStep.WORT_BOILED,
			sg: 15.6,
			volume: 21,
			note: "Rozcieńczona 1,6l wody"
		},
		{
			date: "2018-05-05",
			step: BrewStep.FERM_PRIMARY,
			sg: 14.5,
			volume: 22.6,
			pinned: true
		},
		{
			date: "2018-05-19",
			step: BrewStep.FERM_SECONDARY,
			volume: 21.5,
			pinned: true
		},
		{
			date: "2018-06-09",
			step: BrewStep.BOTTLING,
			volume: 20,
			note: "Refermentacja: 150g cukru"
		}
	],
	notes: [
		"Idea podobna do warki #2, ale zrezygnowałem z powtarzania piwa z ekstraktów i postanowiłem zbudować coś ciekawego na bazie słodów. Duży dodatek słodu biscuit ma za zadanie wnieść nuty herbatnikowe, podczas gdy chmiel ma odtworzyć korzenny charakter pierwowzoru. Całość powinna dać wrażenie zbliżone do ciastek korzennych.",
		"Pełne gotowanie przed fermentacją ma na celu pełne zizomeryzowanie alfa kwasów z dawki chmielu na goryczkę i wytrącenie osadów białkowych. Drugie gotowanie wydłużone do 2,5 godziny celem odparowania 90% alkoholu. W założeniu drugie gotowanie ma być jak najdelikatniejsze, żeby nie powodować zbyt daleko idących reakcji Maillarda.",
		"Profil wody: 100% kranówka przefiltrowana w domowym filtrze Brita; mineralizacja około 160 mg/l (jeśli wierzyć miernikowi).",
		"Zacieranie zacząłem w temperaturze 70°C; do docelowych 67°C spadła po 15 minutach. Zobaczymy, czy wpłynie to na odfermentowanie.",
		"Przy zlewaniu na cichą nie zapisałem parametrów piwa, brakuje więc gęstości.",
		"Po cichej okazało się, że piwo ma bardzo kawowy aromat i posmak. Nie w to celowałem, ale zobaczymy, co będzie dalej.",
		"Ilość cukru do refermentacji wyliczyłem na 122g. Zwiększam ok. 20%, do 150g, żeby zrekompensować usunięty podczas gotowania rozpuszczzony w piwie dwutlenek węgla."
	],
	tastingNotes: [
		"Bardzo mętne, wręcz błotniste, jasnobrązowe. Niezbyt trwała, ale jednak jakaś tam piana, drobna, na jeden palec.",
		"W zapachu na pierwszym planie przyprawowość, taka trochę w stronę goździka. Poza tym delikatna ziołowość, trochę karmelu i ciastka.",
		"Smak nie przystaje do aromatu. Jest głównie chmielowy, ziołowy, może nieco cytrusowy, nie do końca szlachetny. W posmaku pojawia się lekka zbożowość i trochę paloności. Goryczka wysoka, nieco za wysoka.",
		"Jak to bezalkoholowe, jest cienkie, wodniste. Nagazowanie raczej niskie.",
		"Ogólnie niezłe, chociaż pachnie lepiej niż smakuje. Jak dotąd, to najlepsze moje bezalkoholowe pod względem nagazowania i aromatu, w smaku troszkę rozczarowuje. Rześkie, nawet nieźle wchodzi.",
		"Do poprawy: nieco więcej dodatku cukru do refermentacji bezalkoholowego i z 10 IBU mniej na przyszłość.",
		"Po dwóch miesiącach od zabutelkowania chmiel przestał być tak agresywny w smaku, goryczka się ułożyła. Zdecydowanie poprawiło to wrażenie, jakie zostawia piwo."
	]
};
