import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch022: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Diving Helmet",
	tags: [
		Tag.LAGER,
		Tag.PORTER,
		Tag.PLATKI_DEBOWE
	],
	version: 1,
	batch: 22,
	style: Style.BALTIC_PORTER,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 22.4,
			fg: 6.8,
			ibu: 39,
			ebc: 53,
			abv: 9,
			efficiency: 70,
			carbonation: 2,
			attenuation: 69.8
		},
		actual: {
			volume: 18.5,
			og: 21.5,
			fg: 9,
			abv: 7.2,
			efficiency: 62,
			attenuation: 58.1
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(8, Unit.KILOGRAM)).munich1.viking.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).caramel30.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).chocolate400.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(90, Unit.MINUTE)).marynka.hopData(2015, 9.5).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(20, Unit.MINUTE)).marynka.hopData(2015, 9.5).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(20, Unit.MINUTE)).lubelski.hopData(2015, 3.6).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(7, Unit.MINUTE)).marynka.hopData(2015, 9.5).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(7, Unit.MINUTE)).lubelski.hopData(2015, 3.6).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(500, Unit.MILLILITRE)).fm31.yeastCakeFrom(21).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(90, Unit.DAY)).flavour.name("płatki dębowe jasne").build()
		]
	},
	mashNote: "Zacieranie przeprowadzić dwukrotnie, za każdym razem z użyciem połowy zasypu.",
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 15,
				temperature: 75
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 69,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 4,
				temperature: 78,
				estimated: {
					sg: 18.6,
					volume: 25.7
				}
			}
		]
	],
	boilTime: 120,
	fermentation: [
		{
			date: "2017-01-09",
			step: BrewStep.WORT_FIRST,
			sg: 16.5,
			volume: 26.5
		},
		{
			date: "2017-01-09",
			step: BrewStep.WORT_BOILED,
			sg: 21.5,
			volume: 18.5
		},
		{
			date: "2017-01-09",
			step: BrewStep.FERM_PRIMARY,
			sg: 21.5,
			volume: 18.5,
			pinned: true
		},
		{
			date: "2017-02-06",
			step: BrewStep.FERM_SECONDARY,
			sg: 10.5,
			volume: 18.5,
			pinned: true
		},
		{
			date: "2017-05-20",
			step: BrewStep.BOTTLING,
			sg: 9,
			volume: 18.5,
			note: "Refermentacja: 80g cukru"
		}
	],
	notes: [
		"Z uwagi na dużą ilość słodu, zacieranie trzeba było przeprowadzić dwukrotnie.",
		"Piwo dość szybko zaczęło się przegazowywać pomimo długiego leżakowania. Granatów nie było, ale odgazowywałem bardzo długo. Przypuszczam, że drożdże mogły dojeść w butelce jeszcze ze 2°Blg."
	],
	tastingNotes: [
		"Przegazowane, ale bardzo smaczne, z wyraźną, acz nienachalną dębowością i wszystkimi cechami porteru bałtyckiego",
		"Przy późniejszych degustacjach, już po odgazowaniu, miałem wrażenie, że dębowość jednak za bardzo zdominowała piwo, zbytnio przykrywając cechy porteru."
	]
};
