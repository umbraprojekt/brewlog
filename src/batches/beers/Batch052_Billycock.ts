import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { Water } from "../../ingredients/water/Water";

export const Batch052: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Billycock",
	tags: [
		Tag.PORTER
	],
	version: 1,
	batch: 52,
	style: Style.ENGLISH_PORTER,
	stock: 0,
	params: {
		planned: {
			volume: 10,
			og: 11.8,
			fg: 2.8,
			ibu: 29,
			ebc: 54,
			abv: 4.8,
			efficiency: 80,
			carbonation: 2,
			attenuation: 75.9
		},
		actual: {
			volume: 10.5,
			og: 10,
			efficiency: 70.8,
			attenuation: 60,
			abv: 3.2,
			fg: 4
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).marisOtter.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).barleyFlakes.build(),
			new MashIngredientBuilder(new Amount(250, Unit.GRAM)).brown.build(),
			new MashIngredientBuilder(new Amount(200, Unit.GRAM)).crystal160.build(),
			new MashIngredientBuilder(new Amount(50, Unit.GRAM)).darkChocolate.build(),
			new MashIngredientBuilder(new Amount(25, Unit.GRAM)).carafa3Special.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(60, Unit.MINUTE)).challenger.hopData(2017, 5.2).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(20, Unit.MINUTE)).ekg.hopData(2018, 4.1).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(7, Unit.MINUTE)).ekg.hopData(2018, 4.1).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).fm10.yeastCakeFrom(51).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 7,
				temperature: 71.9,
				note: "Cały zasyp trafia do kadzi zaciernej"
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 9.5,
				temperature: 78,
				estimated: {
					sg: 9.7,
					volume: 13.5
				}
			}
		]
	],
	boilTime: 60,
	water: {
		profile: "Londyn (wg BeerSmith)",
		base: new DistilledWater(16.5),
		additions: {
			CaSO4: 1.6,
			NaCl: 1.8,
			MgSO4: 1.1,
			NaHCO3: 2.8,
			CaCO3: 1
		},
		ions: Water.PROFILE_LONDON_BEERSMITH
	},
	fermentation: [
		{
			date: "2019-11-26",
			step: BrewStep.WORT_FIRST,
			sg: 8,
			volume: 13.4
		},
		{
			date: "2019-11-26",
			step: BrewStep.WORT_BOILED,
			sg: 10,
			volume: 10.5
		},
		{
			date: "2019-11-26",
			step: BrewStep.FERM_PRIMARY,
			sg: 10,
			volume: 10.5,
			pinned: true
		},
		{
			date: "2019-12-08",
			step: BrewStep.FERM_SECONDARY,
			sg: 4,
			volume: 10.2,
			pinned: true
		},
		{
			date: "2020-01-19",
			step: BrewStep.BOTTLING,
			sg: 4,
			volume: 10.2,
			note: "refermentacja: 43 g cukru"
		}
	],
	notes: [
		"Tragiczna wydajność, kompletnie nieprzystająca do tego, co zazwyczaj osiągam. Jednak te prawie 25% zasypu w postaci płatków robi swoje. Trzeba było dodać łuski orkiszowej.",
		"Nie chciało mi się ani filtrować brzeczki z chmielinami przez worek filtracyjny, ani myć i dezynfekować dodatkowego wiadra. Zalałem wiadro po warce #51 brzeczką i tak zostawiłem.",
		"Próbka przy zlewaniu na cichą smaczna, czekoladowa i o dziwo, dość wytrawna. Od razu czuć, że to porter. Dokładnie taki smak chciałem osiągnąć.",
		"Bardzo niskie odfermentowanie. Czy to efekt używania pirometru zamiast termometru?",
		"Przy butelkowaniu okazało się, że na powierzchni piwa pływała plamka pleśni w kształcie półksiężyca; prawdopodobnie do piwa wpadł włos. Piwo nie miało niewłaściwego smaku ani zapachu, więc po pozbyciu się pleśni, zabutelkowałem."
	],
	tastingNotes: []
};
