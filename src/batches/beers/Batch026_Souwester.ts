import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch026: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Sou'wester",
	tags: [
		Tag.MILD,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 26,
	style: Style.DARK_MILD,
	rating: 5,
	stock: 0,
	params: {
		planned: {
			volume: 26,
			og: 8.8,
			fg: 2.2,
			ibu: 18,
			ebc: 42,
			abv: 3.5,
			efficiency: 75,
			carbonation: 1.7,
			attenuation: 75.2
		},
		actual: {
			volume: 26,
			og: 10,
			fg: 3,
			abv: 4.5,
			efficiency: 86,
			attenuation: 70
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2.75, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(0.75, Unit.KILOGRAM)).mild.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramel300.build(),
			new MashIngredientBuilder(new Amount(100, Unit.GRAM)).chocolate400.build(),
			new MashIngredientBuilder(new Amount(100, Unit.GRAM)).chocolate400.build(),
			new MashIngredientBuilder(new Amount(100, Unit.GRAM)).roastedBarley.viking.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(60, Unit.MINUTE)).ekg.hopData(2015, 3.3).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).fm10.yeastStarter.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 13,
				temperature: 72.5
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 12
			},
			{
				step: BrewStep.SPARGE,
				volume: 22.22,
				temperature: 78,
				estimated: {
					sg: 7.9,
					volume: 30
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2017-04-08",
			step: BrewStep.WORT_FIRST,
			sg: 8.5,
			volume: 30.9
		},
		{
			date: "2017-04-08",
			step: BrewStep.WORT_BOILED,
			sg: 10,
			volume: 26
		},
		{
			date: "2017-04-08",
			step: BrewStep.FERM_PRIMARY,
			sg: 10,
			volume: 26,
			pinned: true
		},
		{
			date: "2017-04-23",
			step: BrewStep.FERM_SECONDARY,
			sg: 3.25,
			volume: 26,
			pinned: true
		},
		{
			date: "2017-05-14",
			step: BrewStep.BOTTLING,
			sg: 3,
			volume: 26,
			note: "Refermentacja: 80g cukru"
		}
	],
	notes: [
		"Przegazowało się pomimo miesięcznej fermentacji. Granatów nie było, ale trzeba było spuścić sporo gazu."
	],
	tastingNotes: [
		"Wyborne, bardzo złożone i niesamowicie pijalne, choć przegazowane. Koniecznie trzeba powtórzyć."
	]
};
