import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { Water } from "../../ingredients/water/Water";

export const Batch053: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Bycocket",
	tags: [
		Tag.BITTER
	],
	version: 2,
	original: 42,
	batch: 53,
	style: Style.ESB,
	stock: 0,
	params: {
		planned: {
			volume: 10,
			og: 14.1,
			fg: 3.3,
			ibu: 40,
			ebc: 25,
			abv: 5.9,
			efficiency: 80,
			carbonation: 2.0,
			attenuation: 76.6
		},
		actual: {
			volume: 11,
			og: 11.9,
			efficiency: 73.7,
			attenuation: 53.8,
			abv: 3.5,
			fg: 5.5
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(3, Unit.KILOGRAM)).marisOtter.build(),
			new MashIngredientBuilder(new Amount(300, Unit.GRAM)).crystal160.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(60, Unit.MINUTE)).ekg.hopData(2018, 4.1).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(60, Unit.MINUTE)).challenger.hopData(2017, 5.2).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(20, Unit.MINUTE)).ekg.hopData(2018, 4.1).build(),
			new WetIngredientBuilder(new Amount(3, Unit.GRAM), new Amount(10, Unit.MINUTE)).nutrient.build(),
			new WetIngredientBuilder(new Amount(2.5, Unit.GRAM), new Amount(10, Unit.MINUTE)).irishMoss.build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(7, Unit.MINUTE)).ekg.hopData(2018, 4.1).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(150, Unit.MILLILITRE)).fm10.yeastCakeFrom(52).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 8,
				temperature: 72
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 8.7,
				temperature: 78,
				estimated: {
					sg: 11.7,
					volume: 13.4
				}
			}
		]
	],
	boilTime: 60,
	water: {
		profile: "Burton On Trent (wg Raya Danielsa)",
		base: new DistilledWater(16.7),
		additions: {
			CaSO4: 21,
			NaCl: 1,
			MgSO4: 4.1
		},
		ions: Water.PROFILE_BURTON_ON_TRENT_DANIELS
	},
	fermentation: [
		{
			date: "2019-12-08",
			step: BrewStep.WORT_FIRST,
			sg: 9.7,
			volume: 13.9
		},
		{
			date: "2019-12-08",
			step: BrewStep.WORT_BOILED,
			sg: 11.9,
			volume: 11
		},
		{
			date: "2019-12-08",
			step: BrewStep.FERM_PRIMARY,
			sg: 11.9,
			volume: 11,
			pinned: true
		},
		{
			date: "2019-12-20",
			step: BrewStep.FERM_SECONDARY,
			sg: 5.5,
			volume: 10.5,
			pinned: true
		},
		{
			date: "2020-01-19",
			step: BrewStep.BOTTLING,
			sg: 5.5,
			volume: 10.4,
			note: "refermentacja: 44 g cukru"
		}
	],
	notes: [
		"Ponownie uzyskałem niską wydajność, podobnie jak w przypadku warki #52. Zaczynam się zastanawiać, czy mój sprzęt jest dostosowany do warzenia warek 10 l.",
		"Tragicznie niskie odfermentowanie. Trochę się boję, że wyjdą granaty, ale zobaczymy. Podejrzewam, że to efekt używania pirometru i mierzenia temperatury górnych warstw zacieru, przez co temperatura zacierania była zdecydowanie za wysoka."
	],
	tastingNotes: []
};
