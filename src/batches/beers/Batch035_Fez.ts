import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch035: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Fez",
	tags: [
		Tag.BITTER,
		Tag.BEZALKOHOLOWE
	],
	version: 1,
	batch: 35,
	style: Style.ORDINARY_BITTER,
	rating: 0,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 8.1,
			fg: 2.4,
			ibu: 27,
			ebc: 21,
			abv: 0,
			efficiency: 75,
			carbonation: 2.3,
			attenuation: 70.2
		},
		actual: {
			og: 8.1,
			fg: 3.5,
			abv: 0,
			efficiency: 77,
			attenuation: 56.8,
			volume: 20.5
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2.5, Unit.KILOGRAM)).pale.malteurop.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramel150.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(20, Unit.MINUTE)).marynka.hopData(2016, 6.2).build()
		],
		boil2: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(20, Unit.MINUTE)).marynka.hopData(2016, 6.2).build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(7, Unit.MINUTE)).marynka.hopData(2016, 6.2).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).pa7.dryYeast.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 10,
				temperature: 75.4
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 70,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 17.8,
				temperature: 78,
				estimated: {
					sg: 7.2,
					volume: 23.8
				}
			}
		]
	],
	boilTime: [
		20,
		30
	],
	fermentation: [
		{
			date: "2018-03-24",
			step: BrewStep.WORT_FIRST,
			sg: 7.8,
			volume: 21.7
		},
		{
			date: "2018-03-24",
			step: BrewStep.WORT_BOILED,
			sg: 8.1,
			volume: 20.5
		},
		{
			date: "2018-03-24",
			step: BrewStep.FERM_PRIMARY,
			sg: 8.1,
			volume: 20.5,
			pinned: true
		},
		{
			date: "2018-04-08",
			step: BrewStep.FERM_SECONDARY,
			sg: 4,
			volume: 19.5,
			pinned: true
		},
		{
			date: "2018-04-21",
			step: BrewStep.PRE_BOIL_2,
			sg: 3.5,
			volume: 19.4
		},
		{
			date: "2018-04-21",
			step: BrewStep.POST_BOIL_2,
			volume: 17.4,
			note: "Rozcieńczyłem 1,5l wody"
		},
		{
			date: "2018-04-21",
			step: BrewStep.BOTTLING,
			volume: 18.9,
			note: "Refermentacja: 100g cukru"
		}
	],
	notes: [
		"Eksperymentalne piwo bezalkoholowe. Po zakończonej fermentacji będzie gotowane po raz drugi przez 30 minut w celu odparowania alkoholu. Podczas drugiego gotowania dodany również ma być chmiel na smak i aromat.",
		"Profil wody: 100% kranówka przefiltrowana w domowym filtrze Brita.",
		"Dwa gotowania: 20 minut przed zadaniem drożdży i 30 minut po zakończonej fermentacji.",
		"Podczas fermentacji zdecydowałem się na zmianę chmielenia. IBU podniosłem z 18 do 27 dodając dodatkowe 10g Marynki i 25g Lubelskiego (miałem akurat zapas, więc mogłem się bez problemu pozbyć). Zmieniłem też styl z Light Pale Ale na bardziej pasujący Ordinary Bitter, pomimo braku brytyjskich chmieli w recepturze.",
		"Po fermentacji, podczas zlewania piwa do gara warzelnego, stwierdziłem, że aromat bardzo przypomina warkę #9, którą właśnie ze względu na aromat uznałem za kompletnie nieudaną. Czyżby to nie była kwestia chmielu? Aromat przypomina landrynki migdałowe.",
		"Podczas drugiego gotowania nastąpił przełom. Postanowiłem dodać 5g mchu irlandzkiego na ostatnie 5 minut gotowania. Sądząc po zapachu morza przy rozlewie, to był wyjątkowo głupi pomysł.",
		"Do rozlewu dodałem drożdży z gęstwy z warki #37. Chciałem dodać żywych drożdży pozostałych po cichej, ale przy przemywaniu omyłkowo zalałem je zbyt gorącą wodą i prawdopodobnie ugotowałem.",
		"Próbka była paskudna, jakbym zapijał wodą morską. Jeśli drożdże w butelkach nie usuną tego zapachu i smaku, to piwo będzie do wylania. Na razie wnioski z testowej warki są dwa: po pierwsze, gotowanie przed fermentacją powinno być dłuższe, żeby zbić białka, po drugie, mech irlandzki ma sens tylko przed fermentacją."
	],
	tastingNotes: [
		"Niestety, zgodnie z moimi obawami, piwo nawet po dłuższym czasie miało posmak alg, może nie do końca odpychający, ale niedający dokończyć szklanki. Poza tym powtórka aromatu z warki #9, czyli coś przypominającego landrynki migdałowe, ale też ze skojarzeniem z farbą emulsyjną. Nie wiem, czy to aldehyd octowy, czy jakaś inna wada, której nie umiem nazwać. Warka w całości do wylania."
	]
}
