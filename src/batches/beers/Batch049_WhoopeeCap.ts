import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { Water } from "../../ingredients/water/Water";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch049: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Whoopee Cap",
	tags: [
		Tag.IPA,
		Tag.CHMIEL_NOWOFALOWY,
		Tag.PSZENICA
	],
	version: 1,
	batch: 49,
	style: Style.AIPA,
	stock: 18,
	rating: 5,
	params: {
		planned: {
			volume: 18,
			og: 15,
			fg: 2.8,
			ibu: 71,
			ebc: 12,
			abv: 6.6,
			efficiency: 75,
			carbonation: 2.3,
			attenuation: 81.2
		},
		actual: {
			volume: 19.3,
			og: 15,
			fg: 3,
			efficiency: 80.6,
			abv: 6.5,
			attenuation: 80
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4, Unit.KILOGRAM)).pale.bestmalz.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).wheat.bestmalz.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(20, Unit.MINUTE)).centennial.hopData(2018, 8.8).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(20, Unit.MINUTE)).ekuanot.hopData(2018, 12.8).build(),
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(7, Unit.MINUTE)).centennial.hopData(2018, 8.8).build(),
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(7, Unit.MINUTE)).ekuanot.hopData(2018, 12.8).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).us05.yeastCakeFrom(48).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(3, Unit.DAY)).centennial.hopData(2018, 8.8).build(),
			new WetIngredientBuilder(new Amount(130, Unit.GRAM), new Amount(3, Unit.DAY)).ekuanot.hopData(2018, 12.8).build()
		]
	},
	water: {
		profile: "Burton On Trent (wg Raya Danielsa)",
		base: new DistilledWater(27.7),
		additions: {
			CaSO4: 35,
			NaCl: 1.6,
			MgSO4: 6.8
		},
		ions: Water.PROFILE_BURTON_ON_TRENT_DANIELS
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 16,
				temperature: 70.1
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 65,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 11.7,
				temperature: 78,
				estimated: {
					sg: 13.2,
					volume: 21.7
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2019-07-20",
			step: BrewStep.WORT_FIRST,
			sg: 14.6,
			volume: 21
		},
		{
			date: "2019-07-20",
			step: BrewStep.WORT_BOILED,
			sg: 16.2,
			volume: 17.8
		},
		{
			date: "2019-07-20",
			step: BrewStep.FERM_PRIMARY,
			sg: 15,
			volume: 19.3,
			pinned: true,
			note: "Rozcieńczono 1,5 l wody"
		},
		{
			date: "2019-08-03",
			step: BrewStep.FERM_SECONDARY,
			sg: 3.25,
			volume: 19.2,
			pinned: true
		},
		{
			date: "2019-08-13",
			step: BrewStep.BOTTLING,
			sg: 3,
			volume: 18.3,
			note: "Refermentacja: 100 g cukru"
		}
	],
	notes: [
		"Przy zlewaniu na cichą skosztowałem próbkę. Bardzo przyjemny, wyraźny cytrusowy aromat, piwko dość wytrawne i z mocną, ale bardzo przyjemną goryczką. Jak na razie wygląda na to, że powinna wyjść bardzo zacna AIPA w zachodnim stylu, dokładnie tak, jak było zamierzone.",
		"Próbka z rozlewu nadal bardzo smaczna, chociaż spodziewałem się mocniejszego wpływu tak obfitego chmielenia na zimno. Ilość chmielu sprawiła, że sporo pyłu przedostało się do piwa. Pierwsze i ostatnie butelki mogą się przywitać fontanną przy otwieraniu. Trzeba będzie uważać."
	],
	tastingNotes: [
		"Jasnozłociste, jednolicie zamglone, z trwałą i puszystą białą pianką. Z wyglądu bardzo apetyczne.",
		"W aromacie oczywiście dominuje chmiel, głównie idący w owoce tropikalne: mango, ananasy. Leciutka nutka białego wina, typowa dla Ekuanota. Tło słodowe w zasadzie niewyczuwalne, lekkuchno słodkawe.",
		"Smak to bezkompromisowa AIPA, uderzenie chmielu, cytrusów, owoców i mocarna, wyczuwalna od pierwszego łyka grejpfrutowa goryczka. Wytrawne, dość mocno wysycone.",
		"To jest właśnie AIPA, jaką chciałbym zawsze pić. Sprawia wrażenie lekkiej (pomimo poziomu alkoholu), bez zatykającego karmelu, za to nachmielona do granic możliwości, ale bez wrażenia przechmielenia. Piwerko przyjemne pod każdym kątem.",
		"Do powtórzenia, na przyszłość tylko wycelowałbym w nagazowanie na poziomie 2,1 vol. Ewentualnie można pobawić się z innymi chmielami i porównać."
	]
};
