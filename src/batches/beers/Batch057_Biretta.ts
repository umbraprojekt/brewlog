import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch057: IBatchConfig = {
	status: Status.PINNED,
	name: "Biretta",
	tags: [
		Tag.BELGIAN,
		Tag.CHMIEL_NOWOFALOWY,
		Tag.IPA
	],
	version: 1,
	batch: 57,
	style: Style.BELGIAN_IPA,
	stock: 0,
	params: {
		planned: {
			volume: 22,
			og: 16.4,
			fg: 3.1,
			ibu: 82,
			ebc: 23,
			abv: 7.3,
			efficiency: 75,
			carbonation: 2.2,
			attenuation: 80.9
		},
		actual: {
			volume: 24.6,
			og: 16.5,
			efficiency: 86
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(6, Unit.KILOGRAM)).castle.pale.build(),
			new MashIngredientBuilder(new Amount(200, Unit.GRAM)).specialB.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(60, Unit.MINUTE)).amarillo.hopData(2018, 8.2).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(60, Unit.MINUTE)).chinook.hopData(2018, 9.7).build(),
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(20, Unit.MINUTE)).amarillo.hopData(2018, 8.2).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(20, Unit.MINUTE)).chinook.hopData(2018, 9.7).build(),
			new WetIngredientBuilder(new Amount(4, Unit.GRAM), new Amount(15, Unit.MINUTE)).irishMoss.build(),
			new WetIngredientBuilder(new Amount(400, Unit.GRAM), new Amount(7, Unit.MINUTE)).addition.name("cukier demerara").build(),
			new WetIngredientBuilder(new Amount(20, Unit.MILLILITRE), new Amount(7, Unit.MINUTE)).znso4Nutrient.build(),
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(7, Unit.MINUTE)).amarillo.hopData(2018, 8.2).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(7, Unit.MINUTE)).chinook.hopData(2018, 9.7).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(400, Unit.MILLILITRE)).fm25.yeastCakeFrom(56).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(100, Unit.GRAM), new Amount(3, Unit.DAY)).amarillo.hopData(2018, 8.2).build(),
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(3, Unit.DAY)).chinook.hopData(2018, 9.7).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 18,
				temperature: 72.9
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 15,
				temperature: 78,
				estimated: {
					sg: 13.2,
					volume: 25.9
				}
			}
		]
	],
	boilTime: 60,
	water: {
		profile: "Antwerpia + burtonizacja",
		base: new DistilledWater(33),
		additions: {
			CaSO4: 16.4,
			MgSO4: 17.9,
			NaCl: 2.4,
			NaHCO3: 3.2
		},
		ions: {
			Ca: 90,
			Mg: 11,
			Na: 37,
			SO4: 500,
			Cl: 57,
			HCO3: 76
		}
	},
	fermentation: [
		{
			date: "2020-05-23",
			step: BrewStep.WORT_FIRST,
			sg: 15.3,
			volume: 26
		},
		{
			date: "2020-05-23",
			step: BrewStep.WORT_BOILED,
			sg: 18.3,
			volume: 21.9
		},
		{
			date: "2020-05-23",
			step: BrewStep.FERM_PRIMARY,
			sg: 16.5,
			volume: 24.4,
			pinned: true,
			note: "Rozcieńczenie: 2,5 l wody destylowanej"
		}
	],
	notes: [
		"Pierwsza warka z użyciem pożywki w formie 2% roztworu siarczanu cynku."
	],
	tastingNotes: []
};
