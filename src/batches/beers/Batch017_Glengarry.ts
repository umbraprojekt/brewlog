import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch017: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Glengarry",
	tags: [
		Tag.WEE_HEAVY,
		Tag.PLATKI_DEBOWE
	],
	version: 1,
	batch: 17,
	style: Style.WEE_HEAVY,
	rating: 5,
	stock: 1,
	params: {
		planned: {
			volume: 12,
			og: 23.6,
			fg: 7.2,
			ibu: 32,
			ebc: 48,
			abv: 9.5,
			efficiency: 70,
			carbonation: 2,
			attenuation: 69.4
		},
		actual: {
			volume: 12.2,
			og: 21.5,
			fg: 8,
			abv: 7.8,
			efficiency: 64.3,
			attenuation: 69.8
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).caramel150.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(60, Unit.MINUTE)).endeavour.hopData(2015, 7.5).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(20, Unit.MINUTE)).endeavour.hopData(2015, 7.5).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).lubelski.hopData(2015, 3.6).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(500, Unit.MILLILITRE)).fm12.yeastCakeFrom(16).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(60, Unit.DAY)).flavour.name("płatki dębowe mocno opiekane").build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 20,
				temperature: 76
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 70,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 5,
				temperature: 78,
				estimated: {
					sg: 18.1,
					volume: 17.3
				}
			}
		]
	],
	boilTime: 150,
	fermentation: [
		{
			date: "2016-10-25",
			step: BrewStep.WORT_FIRST,
			sg: 19.2,
			volume: 15.7
		},
		{
			date: "2016-10-25",
			step: BrewStep.WORT_BOILED,
			sg: 21.5,
			volume: 12.2
		},
		{
			date: "2016-10-25",
			step: BrewStep.FERM_PRIMARY,
			sg: 21.5,
			volume: 12.2,
			pinned: true
		},
		{
			date: "2016-11-08",
			step: BrewStep.FERM_SECONDARY,
			sg: 8.5,
			volume: 11.5,
			pinned: true
		},
		{
			date: "2017-01-10",
			step: BrewStep.BOTTLING,
			sg: 8,
			volume: 11.4,
			note: "Refermentacja: 50g cukru"
		}
	],
	notes: [
		"Piwo fermentowało bardzo burzliwie, dwukrotnie zapchało rurkę fermentacyjną i zapaskudziło całe wieko wiadra. Wymieniałem wieko na czyste, ryzykując zakażenie, ale na szczęście nic się nie stało.",
		"Płatki dodałem po uprzednim przetrzymaniu 10 minut w piekarniku nagrzanym do 160°C w celu wysterylizowania.",
		"Przy butelkowaniu piwo mocno pachniało świeżym jabłkiem (dojrzałym), ale po jakimś czasie zapach zniknął."
	],
	tastingNotes: [
		"Fenomenalne piwo! Słodowe, karmelowe, z lekką tylko kontrą od chmielu, a do tego wyraźnie dębowe, korpulentne, ale nie męczące. Przyjemnie złożone. Koniecznie muszę powtórzyć!"
	]
};
