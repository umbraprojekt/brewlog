import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch009: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Aviator Hat",
	tags: [
		Tag.BITTER,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 9,
	style: Style.BEST_BITTER,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 11.2,
			fg: 3,
			ibu: 36,
			ebc: 26,
			abv: 4.4,
			efficiency: 70,
			carbonation: 1.5,
			attenuation: 73.5
		},
		actual: {
			volume: 21.8,
			og: 11,
			fg: 2,
			abv: 4.8,
			efficiency: 75,
			attenuation: 81.8
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(3.5, Unit.KILOGRAM)).pale.strzegom.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).caramel150.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(60, Unit.MINUTE)).pulawski.hopData(2015, 7.8).build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(20, Unit.MINUTE)).pulawski.hopData(2015, 7.8).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(7, Unit.MINUTE)).pulawski.hopData(2015, 7.8).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).m07.dryYeast.build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.DAY)).pulawski.hopData(2015, 7.8).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 15.5,
				temperature: 66
			},
			{
				step: BrewStep.REST_MALTOSE,
				temperature: 62,
				time: 10
			},
			{
				step: BrewStep.REST_DEXTRINE,
				temperature: 68,
				time: 50
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 10,
				temperature: 78,
				estimated: {
					sg: 9.9,
					volume: 23.8
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-06-16",
			step: BrewStep.WORT_FIRST,
			sg: 9.8,
			volume: 21,
			note: "Prawdopodobnie błędnie zapisana objętość (powinno być około 25l)"
		},
		{
			date: "2016-06-16",
			step: BrewStep.WORT_BOILED,
			sg: 11,
			volume: 21.8
		},
		{
			date: "2016-06-16",
			step: BrewStep.FERM_PRIMARY,
			sg: 11,
			volume: 21.8,
			pinned: true
		},
		{
			date: "2016-06-30",
			step: BrewStep.FERM_SECONDARY,
			pinned: true
		},
		{
			date: "2016-07-07",
			step: BrewStep.BOTTLING,
			sg: 2,
			volume: 21.5,
			note: "Refermentacja: 60g cukru"
		}
	],
	notes: [
		"Eksperyment z chmielem Puławskim.",
		"Przy rozlewie piwo pachniało dziwnie, czymś pomiędzy owocową landrynką a aldehydem octowym"
	],
	tastingNotes: [
		"Chmiel wnosi nietypowe aromaty, które można wręcz określić jako jakąś dziwną wadę, ni to aldehyd octowy, ni to estry. Bardzo dziwny i nie do końca przyjemny aromat.",
		"Piwo do powtórki, bo wyszło smaczne, ale chmielenie koniecznie trzeba zmienić. Nigdy więcej Puławskiego!"
	]
};
