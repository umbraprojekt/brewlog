import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch018: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Tricorne",
	tags: [
		Tag.ROGGENBOCK,
		Tag.ZYTO,
		Tag.SINGLE_HOP,
		Tag.BOCK
	],
	version: 1,
	batch: 18,
	style: Style.ROGGENBOCK,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 16,
			og: 17.7,
			fg: 3.9,
			ibu: 28,
			ebc: 41,
			abv: 7.7,
			efficiency: 70,
			carbonation: 2.3,
			attenuation: 77.9
		},
		actual: {
			volume: 17,
			og: 17.5,
			fg: 7,
			abv: 6,
			efficiency: 69,
			attenuation: 60
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(3, Unit.KILOGRAM)).rye.viking.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).vienna.viking.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).caramel150.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(60, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(20, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(7, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(50, Unit.MILLILITRE)).fm41.yeastCakeFrom(15).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 19,
				temperature: 74.5
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 9,
				temperature: 78,
				estimated: {
					sg: 15.6,
					volume: 20.6
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-11-08",
			step: BrewStep.WORT_FIRST,
			sg: 14.5,
			volume: 20.5
		},
		{
			date: "2016-11-08",
			step: BrewStep.WORT_BOILED,
			sg: 17.5,
			volume: 17
		},
		{
			date: "2016-11-08",
			step: BrewStep.FERM_PRIMARY,
			sg: 17.5,
			volume: 17,
			pinned: true
		},
		{
			date: "2016-12-11",
			step: BrewStep.BOTTLING,
			sg: 7,
			volume: 16.5,
			note: "Refermentacja: 90g cukru"
		}
	],
	notes: [
		"Najtrudniejsze wysładzanie w moim życiu, 7 godzin kapania kropelka po kropelce. Ostatecznie dolałem dodatkowe 5l wody przy wysładzaniu, żeby uzyskać założoną objętość brzeczki.",
		"Podejrzewam, że końcowy ekstrakt mógł w rzeczywistości być niższy, ale w tak gęstym piwie spławik nie dawał prawidłwego pomiaru. Ale to tylko teoria."
	],
	tastingNotes: [
		"Gęste jak kisiel, bardzo smaczne, słodowe, bananowo-goździkowe. Uwarzyłbym jeszcze, ale nie chcę powtarzać tak trudnego wysładzania. Może zaopatrzę się kiedyś w łuski gryczane."
	]
};
