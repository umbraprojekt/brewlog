import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch010: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Mitre",
	tags: [
		Tag.BARLEY_WINE
	],
	version: 1,
	batch: 10,
	style: Style.BARLEY_WINE,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			volume: 11,
			og: 27,
			fg: 8.3,
			ibu: 60,
			ebc: 40,
			abv: 11.5,
			efficiency: 70,
			carbonation: 1.5,
			attenuation: 69.8
		},
		actual: {
			volume: 9.9,
			og: 25.5,
			fg: 5.5,
			abv: 11.7,
			efficiency: 58,
			attenuation: 78.4
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5.5, Unit.KILOGRAM)).pale.strzegom.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).caramel150.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(60, Unit.MINUTE)).admiral.hopData(2015, 13.6).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(20, Unit.MINUTE)).fuggle.hopData(2015, 4.2).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(7, Unit.MINUTE)).admiral.hopData(2015, 13.6).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).fuggle.hopData(2015, 4.2).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).m07.yeastCakeFrom(9).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 24,
				temperature: 74
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 69,
				time: 90
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 2,
				temperature: 78,
				estimated: {
					sg: 15.6,
					volume: 18.8
				}
			}
		]
	],
	boilTime: 180,
	fermentation: [
		{
			date: "2016-06-30",
			step: BrewStep.WORT_FIRST,
			sg: 15.6,
			volume: 19.3
		},
		{
			date: "2016-06-30",
			step: BrewStep.WORT_BOILED,
			sg: 25.5,
			volume: 9.9
		},
		{
			date: "2016-06-30",
			step: BrewStep.FERM_PRIMARY,
			sg: 25.5,
			volume: 9.9,
			pinned: true
		},
		{
			date: "2016-07-14",
			step: BrewStep.FERM_SECONDARY,
			sg: 8.25,
			volume: 9.5,
			pinned: true
		},
		{
			date: "2016-08-02",
			step: BrewStep.BOTTLING,
			sg: 5.5,
			volume: 9.3,
			note: "Refermentacja: 25g cukru"
		}
	],
	notes: [
		"Na okazję tej warki zbudowałem styropianową chłodziarkę na wkłady z lodem.",
		"Wydajność okazała się znacznie niższa niż zakładana.",
		"Piwo bardzo mocno się przegazowało; musiałem dość długo je odgazowywać."
	],
	tastingNotes: [
		"Niezłe, choć nie tak złożone jak miałem nadzieję. Nawet po ponad roku leżakowania nie ma oznak utlenienia, za to piwo jest mocno alkoholowe (chociaż alkohol nie jest nieprzyjemny i ładnie się wygładził). Lekkie nuty ciemnego cukru, rodzynek, w połączeniu z alkoholem przywodzą na myśl odległe skojarzenie z rumem."
	]
};
