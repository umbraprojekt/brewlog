import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch039: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Tubeteika",
	tags: [
		Tag.STOUT,
		Tag.MALE_PIWO,
		Tag.BEZALKOHOLOWE,
		Tag.MELASA,
		Tag.CHMIEL_NOWOFALOWY
	],
	version: 1,
	batch: 39,
	style: Style.DRY_STOUT,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			ibu: 31,
			abv: 0,
			carbonation: 2
		},
		actual: {
			og: 9.2,
			fg: 2,
			abv: 0,
			attenuation: 78.3,
			volume: 9.4
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(11, Unit.LITRE)).wort.name("Brzeczka z wysłodzin po warce #38, 7,3°Blg").build(),
			new MashIngredientBuilder(new Amount(150, Unit.GRAM)).treacle.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(26, Unit.GRAM), new Amount(30, Unit.MINUTE)).cascade.hopData(2015, 3).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(10, Unit.MINUTE)).nutrient.build()
		],
		boil2: [
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(20, Unit.MINUTE)).lubelski.hopData(2017, 3.5).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(7, Unit.MINUTE)).lubelski.hopData(2017, 3.5).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(0, Unit.MINUTE)).lubelski.hopData(2017, 3.5).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).us05.yeastCakeFrom(36).build()
		]
	},
	mash: [],
	boilTime: [
		30,
		150
	],
	fermentation: [
		{
			date: "2018-04-15",
			step: BrewStep.WORT_BOILED,
			sg: 9.2,
			volume: 9.4
		},
		{
			date: "2018-04-15",
			step: BrewStep.FERM_PRIMARY,
			sg: 9.2,
			volume: 9.4,
			pinned: true
		},
		{
			date: "2018-04-23",
			step: BrewStep.FERM_SECONDARY,
			sg: 2.5,
			volume: 9.4,
			pinned: true
		},
		{
			date: "2018-05-12",
			step: BrewStep.BOTTLING,
			sg: 2,
			volume: 9.4,
			note: "Refermentacja: 90g czarnej melasy (black treacle)"
		}
	],
	notes: [
		"Małe piwo z wysłodzin po warce #38. Parametry nieznane, IBU szacowane z kalkulatora chmielenia.",
		"Nie przewidziałem, że mi zabraknie chmielu. Na goryczkę poszło wszystko, co miałem w zapasie, na drugie gotowanie dokupię Lubelskiego."
	],
	tastingNotes: [
		"Wodniste, kompletnie brak ciała. W aromacie dominuje lukrecja (czarne haribo), w smaku w sumie też. Niewysoka, ale całkiem przyjemna chmielowa goryczka. Lekko ziołowa. Bardzo rześkie, pomimo, że to stout. Schłodzone można pić wiadrami, ale tylko dla orzeźwienia. Na pewno nie dla smaku. Takie sobie."
	]
};
