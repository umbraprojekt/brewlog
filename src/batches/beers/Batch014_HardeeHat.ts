import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch014: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Hardee Hat",
	tags: [
		Tag.IPA,
		Tag.CHMIEL_NOWOFALOWY,
		Tag.SINGLE_HOP,
		Tag.PSZENICA
	],
	version: 1,
	batch: 14,
	style: Style.BLACK_IPA,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 16.8,
			fg: 4,
			ibu: 102,
			ebc: 81,
			abv: 7.1,
			efficiency: 70,
			carbonation: 2,
			attenuation: 76.4
		},
		actual: {
			volume: 21.6,
			og: 17.3,
			fg: 4,
			abv: 7.4,
			efficiency: 78,
			attenuation: 76.9
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).pale.strzegom.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).wheat.strzegom.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramel150.strzegom.build(),
			new MashIngredientBuilder(new Amount(0.25, Unit.KILOGRAM)).carafa3Special.build(),
			new MashIngredientBuilder(new Amount(0.15, Unit.KILOGRAM)).barwiacy.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(60, Unit.MINUTE)).simcoe.hopData(2015, 13.1).build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(20, Unit.MINUTE)).simcoe.hopData(2015, 13.1).build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(7, Unit.MINUTE)).simcoe.hopData(2015, 13.1).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).us05.yeastCakeFrom(13).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(100, Unit.GRAM), new Amount(18, Unit.DAY)).simcoe.hopData(2015, 13.1).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				note: "Dodać słody jasne i karmelowe",
				volume: 20,
				temperature: 73
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				note: "Dodać słody palone",
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 12,
				temperature: 78,
				estimated: {
					sg: 15,
					volume: 23.8
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-08-24",
			step: BrewStep.WORT_FIRST,
			sg: 16,
			volume: 26
		},
		{
			date: "2016-08-24",
			step: BrewStep.WORT_BOILED,
			sg: 17.3,
			volume: 21.6
		},
		{
			date: "2016-08-24",
			step: BrewStep.FERM_PRIMARY,
			sg: 17.3,
			volume: 21.6,
			pinned: true
		},
		{
			date: "2016-09-4",
			step: BrewStep.FERM_SECONDARY,
			sg: 5.5,
			volume: 21,
			pinned: true
		},
		{
			date: "2016-09-22",
			step: BrewStep.BOTTLING,
			sg: 4,
			volume: 20.3,
			note: "Refermentacja: 90g cukru"
		}
	],
	notes: [
		"Warzyłem dzień przed zadaniem drodży, ale ze względu na remont łazienki, nie miałem jak schłodzić brzeczki. Stała całą noc na balkonie (na szczęście było dość zimno), a drożdże zadałem rano."
	],
	tastingNotes: [
		"Udana warka, Black IPA pełną gębą. Dość korpulentne, prażone i mocarnie chmielowe. Może goryczka niższa niż wyliczona (chmiel miał prawie rok, a ja nie wziąłem tego pod uwagę), ale i tak bardzo wysoka."
	]
};
