import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch028: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Aviator Hat",
	tags: [
		Tag.BITTER,
		Tag.SINGLE_HOP
	],
	version: 2,
	original: 9,
	batch: 28,
	style: Style.BEST_BITTER,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 10.7,
			fg: 2.9,
			ibu: 33,
			ebc: 24,
			abv: 4.2,
			efficiency: 75,
			carbonation: 1.5,
			attenuation: 73.3
		},
		actual: {
			volume: 21.8,
			og: 10.5,
			fg: 3,
			abv: 4,
			efficiency: 80.3,
			attenuation: 75.6
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(3.5, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).crystal160.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(60, Unit.MINUTE)).ekg.hopData(2015, 3.3).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(20, Unit.MINUTE)).ekg.hopData(2015, 3.3).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(7, Unit.MINUTE)).ekg.hopData(2015, 3.3).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).s04.dryYeast.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 13,
				temperature: 72.2
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 12
			},
			{
				step: BrewStep.SPARGE,
				volume: 15.77,
				temperature: 78,
				estimated: {
					sg: 9.5,
					volume: 23.8
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2017-09-16",
			step: BrewStep.WORT_FIRST,
			sg: 9.8,
			volume: 23
		},
		{
			date: "2017-09-16",
			step: BrewStep.WORT_BOILED,
			sg: 10.5,
			volume: 21.8
		},
		{
			date: "2017-09-16",
			step: BrewStep.FERM_PRIMARY,
			sg: 10.5,
			volume: 21.8,
			pinned: true
		},
		{
			date: "2017-09-22",
			step: BrewStep.FERM_SECONDARY,
			sg: 4,
			volume: 21.4,
			pinned: true
		},
		{
			date: "2017-10-15",
			step: BrewStep.BOTTLING,
			sg: 3,
			volume: 20.2,
			note: "Refermentacja: 50g cukru"
		}
	],
	notes: [
		"Powtórka warki #9 ze zmienionym chmieleniem."
	],
	tastingNotes: [
		"Przy rozlewie piwo miało delikatny, szlachetny aromat chmielowy z niewielką domieszką owocowych estrów. W smaku leciutko, wytrawnie, z adekwatną do stylu goryczką.",
		"Dwa tygodnie po rozlewie piwo nadal ma bardzo przyjemny charakter chmielowy, baza słodowa jest mocno orzechowa (jak zwykle przy dużym udziale słodu pale ale Viking Malt), nagazowanie jest za niskie. Prawdopodobnie dałem za mało cukru do refermentacji.",
		"Po dłuższym czasie okazało się, że nagazowanie wzrosło do pożądanego poziomu - po prostu zajęło to dłużej niż zazwyczaj. Bardzo smaczne piwko, chociaż powtórka chyba będzie z angielskim słodem bazowym."
	]
};
