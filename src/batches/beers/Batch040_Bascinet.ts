import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch040: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Bascinet",
	tags: [
		Tag.WEDZONE,
		Tag.BEZALKOHOLOWE,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 40,
	style: Style.SMOKED_ALE,
	rating: 3,
	stock: 4,
	params: {
		planned: {
			volume: 24,
			og: 12.3,
			fg: 3,
			ibu: 25,
			ebc: 31,
			abv: 0,
			efficiency: 80,
			carbonation: 2.5,
			attenuation: 75.3
		},
		actual: {
			og: 12.5,
			volume: 25.5,
			fg: 3,
			abv: 0,
			efficiency: 86.3,
			attenuation: 76
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(3, Unit.KILOGRAM)).vienna.bestmalz.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).smokedBeech.steinbach.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).smokedBeech.bestmalz.build(),
			new MashIngredientBuilder(new Amount(100, Unit.GRAM)).carafa3Special.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(20, Unit.MINUTE)).perle.hopData(2017, 6.3).build()
		],
		boil2: [
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(20, Unit.MINUTE)).perle.hopData(2017, 6.3).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(7, Unit.MINUTE)).perle.hopData(2017, 6.3).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).us05.dryYeast.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 14,
				temperature: 73.2
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 20.7,
				temperature: 78,
				estimated: {
					sg: 10.8,
					volume: 28.6
				}
			}
		]
	],
	boilTime: [
		50,
		150
	],
	fermentation: [
		{
			date: "2018-04-22",
			step: BrewStep.WORT_FIRST,
			sg: 11,
			volume: 29
		},
		{
			date: "2018-04-22",
			step: BrewStep.WORT_BOILED,
			sg: 12.5,
			volume: 25.5
		},
		{
			date: "2018-04-22",
			step: BrewStep.FERM_PRIMARY,
			sg: 12.5,
			volume: 25.5,
			pinned: true
		},
		{
			date: "2018-05-04",
			step: BrewStep.FERM_SECONDARY,
			sg: 3,
			volume: 24.7,
			pinned: true
		},
		{
			date: "2018-05-18",
			step: BrewStep.BOTTLING,
			sg: 3,
			volume: 24.5,
			note: "Refermentacja: 150g cukru"
		}
	],
	notes: [
		"Bezalkoholowe piwo wędzone, luźno wzorowane na warce #21, ale z niższym ekstraktem i górnej fermentacji. Procentowy udział słodu wędzonego minimalnie zwiększony (39 zamiast 33%) ze względu na brak możliwości kupienia 0,5kg w sklepie, gdzie się zaopatruję. Niestety, dostępność słodu Steinbach była ograniczona w momencie składania zamówienia, więc brakujący kilogram stanowił będzie słód Bestmalz. Nie wiem, jak to wpłynie na poziom wędzonki w gotowym piwie.",
		"Pierwsze gotowanie wydłużone w stosunku do poprzednich piw bezalkoholowych. Chcę mieć pewność, że w jego trakcie nastąpi przełom i wytrąci się nadmiar białek.",
		"Woda: 100% kranówka z filtra Brita."
	],
	tastingNotes: [
		"Miedziane, klarowne. Piana bardzo niska, drobna, barwy kremowej. Nietrwała i sycząca.",
		"W aromacie nie dzieje się wiele: lekka szynkowa wędzonka i delikatna zbożowość. W sumie zgodnie z założeniami.",
		"W smaku ponownie wędzonka, ale nienachalna, raczej towarzysząca zbożowej słodowości niż ją dominująca. Chmielowa kontra marginalna, chociaż da się wyczuć nieco ziołowości.",
		"Wodniste, cienkie, ale nie zwraca to specjalnie uwagi.",
		"Niby nic szczególnego, ale jak na bezalkoholowe, to całkiem ciekawe piwo. Przyjemnie się pije, nie męczy. Fajne.",
		"Zasyp i chmielenie OK, nie widzę nic, co chciałbym poprawić. Do ewentualnego powtórzenia tak, jak jest, ale w wersji z alkoholem."
	]
};
