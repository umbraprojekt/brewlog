import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch020: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Monomakh's Cap",
	tags: [
		Tag.RIS,
		Tag.EKSTRAKT,
		Tag.ZYTO,
		Tag.STOUT,
		Tag.PLATKI_DEBOWE
	],
	version: 1,
	batch: 20,
	style: Style.RIS,
	rating: 2,
	stock: 8,
	params: {
		planned: {
			volume: 12,
			og: 34.7,
			fg: 12.5,
			ibu: 123,
			ebc: 132,
			abv: 14.2,
			efficiency: 70,
			carbonation: 2,
			attenuation: 64.1
		},
		actual: {
			volume: 10.9,
			og: 34,
			fg: 11.8,
			abv: 14,
			efficiency: 59.5,
			attenuation: 65.3
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(1.5, Unit.KILOGRAM)).wheatLE.coopers.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).caramel30.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).rye.viking.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).chocolate400.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).roastedBarley.viking.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(60, Unit.GRAM), new Amount(60, Unit.MINUTE)).admiral.hopData(2015, 13.6).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(20, Unit.MINUTE)).admiral.hopData(2015, 13.6).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(7, Unit.MINUTE)).admiral.hopData(2015, 13.6).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(500, Unit.MILLILITRE)).m07.yeastCakeFrom(19).build(),
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).ls2.dryYeast.build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(75, Unit.GRAM), new Amount(90, Unit.DAY)).flavour.name("płatki dębowe z beczki po koniaku").build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 20,
				temperature: 78
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 72,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 5.2,
				temperature: 78,
				estimated: {
					sg: 24.5,
					volume: 19.2
				}
			}
		]
	],
	boilTime: 180,
	fermentation: [
		{
			date: "2016-12-06",
			step: BrewStep.WORT_FIRST,
			sg: 22.5,
			volume: 19.5
		},
		{
			date: "2016-12-06",
			step: BrewStep.WORT_BOILED,
			sg: 36,
			volume: 10.2,
			note: "Rozcieńczona 0,7l wody"
		},
		{
			date: "2016-12-06",
			step: BrewStep.FERM_PRIMARY,
			sg: 34,
			volume: 10.9,
			pinned: true
		},
		{
			date: "2016-12-29",
			step: BrewStep.FERM_SECONDARY,
			sg: 16,
			volume: 10.9,
			pinned: true
		},
		{
			date: "2017-01-19",
			step: BrewStep.FERM_TERTIARY,
			sg: 13.5,
			volume: 10.9,
			pinned: true
		},
		{
			date: "2017-04-09",
			step: BrewStep.BOTTLING,
			sg: 11.8,
			volume: 10.5,
			note: "Refermentacja: 20g cukru"
		}
	],
	notes: [
		"Ekstrakt początkowy wyszedł 36°Blg, ale rozcieńczyłem do 34, bo bałem się, że drożdże nie dadzą rady.",
		"Dodałem pożywki dla drożdży piwnych, bo M07 prawdopodobnie były już mocno zmęczone życiem.",
		"Po fermentacji burzliwej drożdżami M07 dodałem uwodnione Fermicru LS2.",
		"Po pierwszej cichej drożdżami Fermicru LS2 dodałem płatki dębowe (zagotowane w rondelku w odrobinie piwa odebranego z fermentora, schłodzone i wlane z powrotem do wiadra).",
		"Przy butelkowaniu nie dodawałem świeżych drożdży do refermentacji. A chyba powinienem.",
		"Piwo się nie nagazowało, nawet kiedy eksperymentalnie dodałem jeszcze gram cukru do jednej butelki. Prawdopodobnie drożdże się zmęczyły albo wręcz zabił je alkohol."
	],
	tastingNotes: [
		"Niemal zupełnie płaskie, bardzo oleiste, słodkie, wyklejające. Wyjątkowo ciężkie, intensywne, deserowe piwo, praktycznie likier. Nie wyobrażam sobie wypicia całej butelki za jednym posiedzeniem. Być może kiedyś pobawię się w coś podobnego, ale zastanowię się nad drożdżami Super High Gravity."
	]
};
