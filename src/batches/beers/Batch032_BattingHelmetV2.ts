import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch032: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Batting Helmet",
	tags: [
		Tag.IPA,
		Tag.SINGLE_HOP,
		Tag.CHMIEL_NOWOFALOWY
	],
	version: 2,
	original: 4,
	batch: 32,
	style: Style.RED_IPA,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			volume: 21,
			og: 16.3,
			fg: 3.8,
			ibu: 67,
			ebc: 28,
			abv: 6.9,
			efficiency: 70,
			carbonation: 2.2,
			attenuation: 76.5
		},
		actual: {
			og: 16,
			fg: 3.5,
			volume: 22.8,
			abv: 6.9,
			efficiency: 74.7,
			attenuation: 78.1
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).caramelRed.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).munich2.viking.build(),
			new MashIngredientBuilder(new Amount(30, Unit.GRAM)).carafa3Special.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(60, Unit.MINUTE)).mosaic.hopData(2016, 10.2).build(),
			new WetIngredientBuilder(new Amount(60, Unit.GRAM), new Amount(20, Unit.MINUTE)).mosaic.hopData(2016, 10.2).build(),
			new WetIngredientBuilder(new Amount(60, Unit.GRAM), new Amount(7, Unit.MINUTE)).mosaic.hopData(2016, 10.2).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).us05.dryYeast.build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(14, Unit.DAY)).mosaic.hopDry.hopData(2016, 10.2).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 21,
				temperature: 72.7,
				note: "Dodać słody jasne i karmelowe"
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10,
				note: "Dodać słód palony"
			},
			{
				step: BrewStep.SPARGE,
				volume: 11.85,
				temperature: 78,
				estimated: {
					sg: 14.5,
					volume: 24.8
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2017-10-25",
			step: BrewStep.WORT_FIRST,
			sg: 16.5,
			volume: 25,
			note: "Rozcieńczona 3,5l wody"
		},
		{
			date: "2017-10-25",
			step: BrewStep.WORT_BOILED,
			sg: 16,
			volume: 22.8
		},
		{
			date: "2017-10-25",
			step: BrewStep.FERM_PRIMARY,
			sg: 16,
			volume: 22.8,
			pinned: true
		},
		{
			date: "2017-11-12",
			step: BrewStep.FERM_SECONDARY,
			sg: 4,
			volume: 22.8,
			pinned: true
		},
		{
			date: "2017-11-27",
			step: BrewStep.BOTTLING,
			sg: 3.5,
			volume: 22,
			note: "Refermentacja: 110g cukru"
		}
	],
	notes: [
		"Powtórka warki #4.",
		"Do zacierania i wysładzania użyłem 30l wody źródlanej o mineralizacji 350mg/l, pozostałe około 7,5l stanowiła woda z kranu przefiltrowana w domowym filtrze Brita.",
		"Zbyt wysoka wydajność wysładzania. W związku z tym delikatnie zmodyfikowałem chmielenie celem otrzymania docelowej wartości IBU: 15g na 60 minut kosztem 65g na cichą. Pozostałe porcje bez zmian.",
		"Przy pierwszej porcji chmielu sporo wykipiało. Dosypałem dodatkowe 5g, kosztem porcji na cichą (zostaje 60g)."
	],
	tastingNotes: [
		"Trochę rozczarowujące. Charakter chmielu Mosaic co najwyżej umiarkowany, pomimo odpowiedniej goryczki i słodowości. Piwo ogólnie smaczne, ale przydałoby się je uwarzyć kiedyś z chmielem mającym mniej niż rok."
	]
}
