import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { Water } from "../../ingredients/water/Water";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch058: IBatchConfig = {
	status: Status.PINNED,
	name: "Hood",
	tags: [
		Tag.BELGIAN,
		Tag.KLASZTORNE,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 58,
	style: Style.QUAD,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 20.9,
			fg: 1.7,
			ibu: 30,
			ebc: 42,
			abv: 10.8,
			efficiency: 70,
			carbonation: 2.3,
			attenuation: 91.8
		},
		actual: {
			volume: 21.8,
			og: 22,
			efficiency: 83.6
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).castle.pils.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).castle.munich1.build(),
			new MashIngredientBuilder(new Amount(400, Unit.GRAM)).specialB.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(60, Unit.MINUTE)).hallertauHersbrucker.hopData(2019, 4.0).build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(20, Unit.MINUTE)).hallertauHersbrucker.hopData(2019, 4.0).build(),
			new WetIngredientBuilder(new Amount(4, Unit.GRAM), new Amount(15, Unit.MINUTE)).irishMoss.build(),
			new WetIngredientBuilder(new Amount(1, Unit.KILOGRAM), new Amount(7, Unit.MINUTE)).addition.name("cukier muscovado").build(),
			new WetIngredientBuilder(new Amount(20, Unit.MILLILITRE), new Amount(7, Unit.MINUTE)).znso4Nutrient.build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(7, Unit.MINUTE)).hallertauHersbrucker.hopData(2019, 4.0).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(200, Unit.MILLILITRE)).fm25.yeastCakeFrom(56).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 21,
				temperature: 68.8
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 64,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 15,
				temperature: 78,
				estimated: {
					sg: 13.8,
					volume: 25.7
				}
			}
		]
	],
	boilTime: 120,
	water: {
		profile: "Antwerpia",
		base: new DistilledWater(33),
		additions: {
			CaSO4: 4.1,
			MgSO4: 1.3,
			NaCl: 2.8,
			CaCl2: 0.5,
			CaCO3: 4.3
		},
		ions: Water.PROFILE_ANTWERP_BEERSMITH
	},
	fermentation: [
		{
			date: "2020-06-07",
			step: BrewStep.WORT_FIRST,
			sg: 15.3,
			volume: 27.3
		},
		{
			date: "2020-06-07",
			step: BrewStep.WORT_BOILED,
			sg: 22,
			volume: 21.8
		},
		{
			date: "2020-06-07",
			step: BrewStep.FERM_PRIMARY,
			sg: 22,
			volume: 21.8,
			pinned: true
		}
	],
	notes: [
		"Użyta gęstwa zebrana z Dubbla #56, przemyta i trzymana w lodówce.",
		"Gigantyczne kłopoty podczas filtracji brzeczki z chmielin po gotowaniu. Nowy worek filtracyjny się zapchał, a podczas próby przefiltrowania reszty brzeczki starym workiem, większość chmielin dostała się do fermentora. Musiałem przygotować nowy fermentor i ponownie filtrować starym workiem. Całość trwała jakieś 2 godziny. Mam nadzieję, że nie wda się żadna infekcja."
	],
	tastingNotes: []
};
