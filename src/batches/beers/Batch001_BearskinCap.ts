import { IngredientType } from "../../enums/IngredientType";
import { Producer } from "../../enums/Producer";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { Ingredient } from "../../ingredients/Ingredient";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";
import { BrewStep } from "../../enums/BrewStep";

export const Batch001: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Bearskin Cap",
	tags: [
		Tag.BITTER,
		Tag.EKSTRAKT,
		Tag.CHMIEL_NOWOFALOWY
	],
	version: 1,
	batch: 1,
	style: Style.BEST_BITTER,
	rating: 2,
	stock: 0,
	params: {
		planned: {},
		actual: {
			og: 10.5
		}
	},
	ingredients: {
		mash: [
			new Ingredient(IngredientType.EXTRACT, Producer.BREWMAKER, "Essential Bitter", new Amount(1.5, Unit.KILOGRAM)),
			new MashIngredientBuilder(new Amount(1.7, Unit.KILOGRAM)).lightLE.bruntal.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM)).sorachi.build()
		],
		yeast: [
			new Ingredient(IngredientType.YEAST_DRY, Producer.BREWMAKER, "?", new Amount(1, Unit.PACKET))
		]
	},
	mash: [],
	fermentation: [
		{
			date: "2015-12-17",
			step: BrewStep.BOTTLING
		}
	],
	notes: [
		"Pierwsze podejście do warzenia, oczywiście z brewkitu. Dochmielony Sorachi Ace na aromat i być może również na zimno (nie pamiętam). Drożdże nieznane, bo pochodzące z brewkitu (biała saszetka bez jakichkolwiek oznaczeń).",
		"Nie zachowały się szczegóły na temat warzenia, chmielenia i fermentacji, więc dane odtworzone na podstawie zawodnej pamięci i historii zamówień ze sklepu z surowcami.",
		"Refermentacja bez odmierzania dokładnej ilości cukru: 1 płaska łyżeczka na butelkę."
	],
	tastingNotes: [
		"Charakteru Sorachi Ace nie było, chociaż ciężko stwierdzić, czy przez jego małą ilość, czy zaawansowany wiek. Piwo ogólnie nie zachwycało, ale w porównaniu z koncernowym było naprawdę dobre. Powtarzał nie będę, rzecz jasna, ale zachęciło mnie to do dalszego warzenia."
	]
};
