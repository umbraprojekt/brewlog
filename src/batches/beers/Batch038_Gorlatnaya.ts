import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch038: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Gorlatnaya",
	tags: [
		Tag.STOUT,
		Tag.RIS,
		Tag.MELASA,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 38,
	style: Style.RIS,
	rating: 4,
	stock: 18,
	params: {
		planned: {
			volume: 12,
			og: 22,
			fg: 5.3,
			ibu: 81,
			ebc: 105,
			abv: 9.6,
			efficiency: 60,
			carbonation: 2.2,
			attenuation: 76
		},
		actual: {
			volume: 11,
			abv: 10.2,
			attenuation: 71.3,
			og: 24.4,
			efficiency: 61.8
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2.25, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).pale.bestmalz.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).caramel30.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).chocolate400.build(),
			new MashIngredientBuilder(new Amount(0.3, Unit.KILOGRAM)).treacle.build(),
			new MashIngredientBuilder(new Amount(0.25, Unit.KILOGRAM)).roastedBarley.viking.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(60, Unit.MINUTE)).target.hopData(2017, 7.5).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(20, Unit.MINUTE)).target.hopData(2017, 7.5).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(10, Unit.MINUTE)).nutrient.build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).target.hopData(2017, 7.5).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(500, Unit.MILLILITRE)).us05.yeastCakeFrom(36).build()
		]
	},
	water: {
		profile: "kranówka",
		base: new TapWater2018(24.3),
		ions: TapWater2018.ions
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 19,
				temperature: 72.4,
				note: "Słody jasne, karmelowe i czekoladowe, połowa jęczmienia prażonego"
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10,
				note: "Dodać drugą połowę jęczmienia prażonego"
			},
			{
				step: BrewStep.SPARGE,
				volume: 5.3,
				temperature: 78,
				estimated: {
					sg: 16.9,
					volume: 17.3
				}
			}
		]
	],
	boilTime: 120,
	fermentation: [
		{
			date: "2018-04-15",
			step: BrewStep.WORT_FIRST,
			sg: 15.7,
			volume: 17.5
		},
		{
			date: "2018-04-15",
			step: BrewStep.WORT_BOILED,
			sg: 24.4,
			volume: 11
		},
		{
			date: "2018-04-15",
			step: BrewStep.FERM_PRIMARY,
			sg: 24.4,
			volume: 11,
			pinned: true
		},
		{
			date: "2018-04-26",
			step: BrewStep.FERM_SECONDARY,
			sg: 7,
			volume: 11,
			pinned: true
		},
		{
			date: "2018-07-17",
			step: BrewStep.BOTTLING,
			sg: 7,
			volume: 10.8,
			note: "Refermentacja: 84g black treacle"
		}
	],
	notes: [
		"Pomimo, że w piwnicy zalega jeszcze większość imperialnego stoutu z warki #20, postanowiłem uwarzyć piwo w tym samym stylu, ale znacznie lżejsze.",
		"Profil wody: 100% kranówka.",
		"Po wysładzaniu uzyskałem dodatkowe 11 litrów brzeczki o gęstości 7,3°Blg, której użyję w warce #39.",
		"Gęstwa po warce #36 była nieźle rozruszana: już po 20 minutach w bulkometrze żwawo bulkało, nawet pomimo porządnego schłodzenia brzeczki poniżej 20°C. Miejmy nadzieję, że fermentacja nie będzie tak szalona, żeby naprodukować fuzli. Nie mam jednak lodówki, żeby kontrolować temperaturę i zdany jestem na łaskę losu."
	],
	tastingNotes: [
		"Czarne jak smoła, bez prześwitów, gęste i w zasadzie bez piany. W aromacie słodowe, palone, kawowe. Zanim się ogrzało, sprawało wrażenie jakby pachniało słodem pilzneńskim (ziarno lekko idące w stronę tektury), ale później to wrażenie znikło. Smak dość agresywny, bardzo gorzki, mocno palony, kawowy. Nie czuć alkoholu. Nisko wysycone, gładkie. Nuty lukrecji na granicy autosugestii, w zasadzie pomijalne. Bardzo dobre jako bazowy RIS, bez udziwnień. Przyciężkie, więc szybka konsumpcja odpada."
	]
}
