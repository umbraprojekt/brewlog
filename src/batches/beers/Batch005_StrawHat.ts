import { IBatchConfig } from "../../types/IBatch";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { Amount } from "../../ingredients/Amount";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { BrewStep } from "../../enums/BrewStep";

export const Batch005: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Straw Hat",
	tags: [
		Tag.PSZENICA,
		Tag.WEIZEN,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 5,
	style: Style.HEFE_WEIZEN,
	rating: 2,
	stock: 0,
	params: {
		planned: {
			volume: 21.5,
			og: 12,
			ibu: 12,
			ebc: 10,
			efficiency: 70,
			carbonation: 3
		},
		actual: {
			volume: 23,
			og: 12.5,
			fg: 2,
			abv: 5.7,
			efficiency: 75.1,
			attenuation: 84.6
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2.5, Unit.KILOGRAM)).wheat.strzegom.build(),
			new MashIngredientBuilder(new Amount(2.5, Unit.KILOGRAM)).pils.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(60, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(5, Unit.MINUTE)).hallertauTradition.hopData(2015, 4.5).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).dryYeast.wb06.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 17.5,
				temperature: 46,
				note: "Dodać tylko słód pszeniczny",
			},
			{
				step: BrewStep.REST_FERULIC,
				temperature: 44,
				time: 20
			},
			{
				step: BrewStep.REST_PROTEIN,
				temperature: 55,
				time: 20
			},
			{
				step: BrewStep.REST_MALTOSE,
				note: "Dodać słód pilzneński",
				temperature: 63,
				time: 40
			},
			{
				step: BrewStep.REST_DEXTRINE,
				temperature: 72,
				time: 40
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 5
			},
			{
				step: BrewStep.SPARGE,
				volume: 15,
				temperature: 78
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-04-19",
			step: BrewStep.FERM_PRIMARY,
			sg: 12.5,
			volume: 23,
			pinned: true
		},
		{
			date: "2016-05-10",
			step: BrewStep.BOTTLING,
			sg: 2,
			volume: 23,
			note: "Refermentacja: 190g cukru"
		}
	],
	notes: [
		"Kiedyś trzeba zrobić pszenicę raz jeszcze, ale drożdże muszą być inne, a bezsensowne przerwy muszą zniknąć z zacierania."
	],
	tastingNotes: [
		"Bardzo wytrawne, mocarnie nagazowane (sam nie wiem, jak nie popękały butelki) i bez żadnych nut bananów czy goździka. Jak na pszenicę, bardzo słabe piwo.Taki \"Euro Weizen\". Ale przynajmniej rozszedł się szybko."
	]
};
