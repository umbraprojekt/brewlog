import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch021: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Tirolerhut",
	tags: [
		Tag.BOCK,
		Tag.WEDZONE,
		Tag.SINGLE_HOP,
		Tag.LAGER
	],
	version: 1,
	batch: 21,
	style: Style.RAUCHBOCK,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 19,
			og: 17.1,
			fg: 4.3,
			ibu: 27,
			ebc: 35,
			abv: 7.1,
			efficiency: 70,
			carbonation: 2.3,
			attenuation: 74.9
		},
		actual: {
			volume: 21.3,
			og: 17,
			fg: 6,
			abv: 6.1,
			efficiency: 77.8,
			attenuation: 64.7
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4, Unit.KILOGRAM)).munich1.viking.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).smokedBeech.steinbach.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).melanoiden.bestmalz.build(),
			new MashIngredientBuilder(new Amount(50, Unit.GRAM)).carafa3Special.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(90, Unit.MINUTE)).perle.hopData(2015, 7).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(20, Unit.MINUTE)).perle.hopData(2015, 7).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(7, Unit.MINUTE)).perle.hopData(2015, 7).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).fm31.yeastStarter.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				note: "Dodać słody jasne",
				volume: 20,
				temperature: 72.5
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				note: "Dodać słód palony",
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 11.23,
				temperature: 78,
				estimated: {
					sg: 14.6,
					volume: 23.7
				}
			}
		]
	],
	boilTime: 90,
	fermentation: [
		{
			date: "2016-12-15",
			step: BrewStep.WORT_FIRST,
			sg: 16,
			volume: 24.5
		},
		{
			date: "2016-12-15",
			step: BrewStep.WORT_BOILED,
			sg: 17,
			volume: 21.3
		},
		{
			date: "2016-12-15",
			step: BrewStep.FERM_PRIMARY,
			sg: 17,
			volume: 21.3,
			pinned: true
		},
		{
			date: "2017-01-09",
			step: BrewStep.FERM_SECONDARY,
			sg: 9,
			volume: 21.3,
			pinned: true
		},
		{
			date: "2017-02-07",
			step: BrewStep.BOTTLING,
			sg: 6,
			volume: 21,
			note: "Refermentacja: 112g cukru"
		}
	],
	notes: [
		"Przegazowało się. Nie na tyle mocno, żeby wybuchało, ale musiałem poodgazowywać."
	],
	tastingNotes: [
		"Wędzonka bardzo wyrazista i trwała. Trochę za słodkie.",
		"Z czasem słodycz zmalała (drożdże dojadły cukry, co spowodowało przegazowanie) i wyszedł naprawdę dobry koźlak wędzony."
	]
};
