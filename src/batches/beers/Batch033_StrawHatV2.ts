import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";
import { BrewStep } from "../../enums/BrewStep";

export const Batch033: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Straw Hat",
	tags: [
		Tag.WEIZEN,
		Tag.SINGLE_HOP,
		Tag.PSZENICA,
		Tag.BEZALKOHOLOWE
	],
	version: 2,
	original: 5,
	batch: 33,
	style: Style.HEFE_WEIZEN,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 18.5,
			og: 11,
			fg: 1.9,
			ibu: 10,
			ebc: 6,
			abv: 4.8,
			efficiency: 70,
			carbonation: 2.5,
			attenuation: 83
		},
		actual: {
			volume: 19.5,
			og: 11.5,
			fg: 2.7,
			abv: 4.8,
			efficiency: 77.4,
			attenuation: 77.7
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).wheat.weyermann.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).pils.weyermann.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(60, Unit.MINUTE)).hallertauTradition.hopData(2016, 3.7).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(20, Unit.MINUTE)).hallertauTradition.hopData(2016, 3.7).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(7, Unit.MINUTE)).hallertauTradition.hopData(2016, 3.7).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).fm41.yeastStarter.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 13,
				temperature: 70
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 65,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 14.21,
				temperature: 78,
				estimated: {
					sg: 9.7,
					volume: 22.2
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2018-02-24",
			step: BrewStep.WORT_FIRST,
			sg: 9.9,
			volume: 22.2
		},
		{
			date: "2018-02-24",
			step: BrewStep.WORT_BOILED,
			sg: 11.5,
			volume: 19.5
		},
		{
			date: "2018-02-24",
			step: BrewStep.FERM_PRIMARY,
			sg: 11.5,
			volume: 19.5,
			pinned: true
		},
		{
			date: "2018-03-04",
			step: BrewStep.FERM_SECONDARY,
			sg: 3,
			volume: 19.1,
			pinned: true
		},
		{
			date: "2018-03-25",
			step: BrewStep.BOTTLING,
			sg: 2.7,
			volume: 18.8,
			note: "Refermentacja: 110g cukru"
		}
	],
	notes: [
		"Powtórka warki #5, ale z zacieraniem jednotemperaturowym.",
		"Profil wody: 100% butelkowana Amita, ~350mg/l.",
		"Przy rozlewie postanowiłem zrobić eksperyment: połowę piwa podgrzałem do 80°C i przetrzymałem tak przez 30 minut celem odparowania alkoholu. Ponieważ przy okazji odparowało pół litra wody, uzupełniłem brak przegotowaną i ostudzoną wodą z filtra."
	],
	tastingNotes: [
		"Wersja 0% wydaje się trochę rozwodniona, nagazowanie bardzo niskie. Aromat delikatnie bananowy i delikatnie goździkowy. W smaku delikatna słodowość, a lekki goździk pojawia się dopiero w finiszu. Bardzo wytrawne. Ogólnie całkiem niezłe, jednoznacznie identyfikowalne jako Hefe-Weizen.",
		"Wersja podstawowa, również nisko, ale wyraźnie mocniej nagazowana, ma banana w aromacie na podobnym poziomie, a goździk jest bardzo wyraźny, w zasadzie dominujący. W smaku podobnie: lekka słodowość i mnóstwo goździka. Sprawia wrażenie delikatnie słodszego, ma jakby więcej ciała.",
		"Barwa obu wersji jest identyczna, zmętnienie utrzymuje się pomimo kilku tygodni od rozlewu.",
		"Ogólnie, obie wersje są fajne, bardzo rześkie. Widać, że fenole naprodukowane przez drożdże są lotne i w większości uleciały podczas podgrzewania po fermentacji. Mimo wszystko, eksperyment udany. Szkoda tylko niskiego nagazowania; następnym razem sypnę więcej cukru."
	]
};
