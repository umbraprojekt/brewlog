import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch030: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Caubeen",
	tags: [
		Tag.RED_ALE,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 30,
	style: Style.IRA,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 22,
			og: 11.1,
			fg: 2.6,
			ibu: 28,
			ebc: 25,
			abv: 4.5,
			efficiency: 75,
			carbonation: 2.2,
			attenuation: 76.9
		},
		actual: {
			volume: 25.6,
			og: 11,
			fg: 3,
			efficiency: 86.5,
			abv: 4.3,
			attenuation: 72.7
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramel30.build(),
			new MashIngredientBuilder(new Amount(85, Unit.GRAM)).roastedBarley.viking.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(60, Unit.MINUTE)).ekg.hopData(2016, 3.7).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(20, Unit.MINUTE)).ekg.hopData(2016, 3.7).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(10, Unit.MINUTE)).irishMoss.build(),
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(7, Unit.MINUTE)).ekg.hopData(2016, 3.7).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).us05.dryYeast.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 15,
				temperature: 72.2
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 17.39,
				temperature: 78,
				estimated: {
					sg: 9.6,
					volume: 26.8
				}
			}
		]
	],
	boilTime: 90,
	fermentation: [
		{
			date: "2017-10-07",
			step: BrewStep.WORT_FIRST,
			sg: 10,
			volume: 28.5
		},
		{
			date: "2017-10-07",
			step: BrewStep.WORT_BOILED,
			sg: 11,
			volume: 25.6
		},
		{
			date: "2017-10-07",
			step: BrewStep.FERM_PRIMARY,
			sg: 11,
			volume: 25.6,
			pinned: true
		},
		{
			date: "2017-10-15",
			step: BrewStep.FERM_SECONDARY,
			sg: 3,
			volume: 24.1,
			pinned: true
		},
		{
			date: "2017-11-11",
			step: BrewStep.BOTTLING,
			sg: 3,
			volume: 23.9,
			note: "Refermentacja: 120g cukru"
		}
	],
	notes: [
		"W związku z wyższą niż zakładana wydajnością, zmieniłem trochę chmielenie i poszło 40g na 60 minut, 30g na 20 minut i 30g na 7 minut. Z kalkulatora wyszło, że otrzymana goryczka powinna być taka sama, jak w recepturze, czyli 28 IBU.",
		"Po gotowaniu otrzymałem 23,1l brzeczki o gęstości 12,2°Blg, więc rozieńczyłem do 11°Blg dodając 2,5l wody."
	],
	tastingNotes: [
		"Bardzo podobne do Bittera #28, wręcz nie do odróżnienia na pierwszy rzut oka, z delikatną jedynie opiekanością w finiszu. Bazowy słód wniósł podobną orzechowość, a chmiel angielskie, ziemisto-tytoniowe nuty. Piwo bardzo przyjemne, ale balans jest przesunięty za bardzo w stronę chmielu. Do powtórki, ale z angielskim słodem bazowym i z oszczędniejszym chmieleniem na smak i aromat.",
		"Po trzech miesiącach od rozlewu piwo się przegazowało. Nie żeby wyszły granaty, ale nagazowanie było drapiące w gardło."
	]
};
