import { BrewStep } from "../../enums/BrewStep";
import { IngredientType } from "../../enums/IngredientType";
import { Producer } from "../../enums/Producer";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { Ingredient } from "../../ingredients/Ingredient";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch019: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Leprechaun Hat",
	tags: [
		Tag.STOUT,
		Tag.KAWA
	],
	version: 2,
	original: 7,
	batch: 19,
	style: Style.COFFEE_STOUT,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			volume: 24,
			og: 13.1,
			fg: 3.2,
			ibu: 38,
			ebc: 88,
			abv: 5.4,
			efficiency: 70,
			carbonation: 1.8,
			attenuation: 75.7
		},
		actual: {
			volume: 24.8,
			og: 14.5,
			fg: 3.5,
			abv: 6,
			efficiency: 80.3,
			attenuation: 75.9
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramel600.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).roastedBarley.viking.build(),
			new MashIngredientBuilder(new Amount(0.4, Unit.KILOGRAM)).oatFlakes.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(60, Unit.MINUTE)).admiral.hopData(2015, 13.6).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(20, Unit.MINUTE)).admiral.hopData(2015, 13.6).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).lubelski.hopData(2015, 4.5).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(50, Unit.MILLILITRE)).m07.cultureFrom(10).build()
		],
		bottling: [
			new Ingredient(IngredientType.FLAVOUR, Producer.NONE, "kawa Nicaragua Flores del Café", new Amount(80, Unit.GRAM))
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				note: "Dodać słody jasne, karmelowe i płatki",
				volume: 20,
				temperature: 72
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				note: "Dodać prażony jęczmień",
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 15,
				temperature: 78,
				estimated: {
					sg: 11.8,
					volume: 27.9
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-11-24",
			step: BrewStep.WORT_FIRST,
			sg: 13.7,
			volume: 28.3
		},
		{
			date: "2016-11-24",
			step: BrewStep.WORT_BOILED,
			sg: 14.5,
			volume: 24.8
		},
		{
			date: "2016-11-24",
			step: BrewStep.FERM_PRIMARY,
			sg: 14.5,
			volume: 24.8,
			pinned: true
		},
		{
			date: "2016-12-06",
			step: BrewStep.FERM_SECONDARY,
			sg: 4,
			volume: 24.8,
			pinned: true
		},
		{
			date: "2016-12-29",
			step: BrewStep.BOTTLING,
			sg: 3.5,
			volume: 24.5,
			note: "Refermentacja: 85g cukru"
		}
	],
	notes: [
		"Powtórka warki #7.",
		"Starter drożdżowy przygotowany z osadu drożdżowego z butelki z warki #10. Pomimo dużej mocy piwa, drożdże po tygodniu na mieszadle magnetycznym bardzo ładnie się namnożyły.",
		"Przy butelkowaniu kawa dodana w formie espresso."
	],
	tastingNotes: [
		"Kawa wyszła dość agresywna, co z palonością od jęczmienia dało trochę popiołowy charakter. Poza tym smaczne, choć troszkę ciężkawe. Mogłoby jednak mieć te 13°Blg..."
	]
};
