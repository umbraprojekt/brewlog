import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { Water } from "../../ingredients/water/Water";

export const Batch050: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Dutch Bonnet",
	tags: [
		Tag.BOCK,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 50,
	style: Style.BOK,
	stock: 39,
	rating: 5,
	params: {
		planned: {
			volume: 19,
			og: 18.8,
			fg: 4.5,
			ibu: 24,
			ebc: 32,
			abv: 8.1,
			efficiency: 75,
			carbonation: 2.3,
			attenuation: 76.3
		},
		actual: {
			volume: 19.9,
			og: 17.8,
			efficiency: 73.8,
			abv: 7.9,
			fg: 3.5,
			attenuation: 80.3
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).pils.bestmalz.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).munich1.bestmalz.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).munich2.bestmalz.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caraMunich.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(45, Unit.MINUTE)).hallertauTradition.hopData(2018, 4.8).build(),
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(500, Unit.MILLILITRE)).us05.yeastCakeFrom(49).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 21,
				temperature: 72.3
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 9.25,
				temperature: 78,
				estimated: {
					sg: 16.7,
					volume: 22.7
				}
			}
		]
	],
	boilTime: 60,
	water: {
		profile: "Monachium (wg BeerSmith)",
		base: new DistilledWater(30.25),
		additions: {
			MgSO4: 1.1,
			NaHCO3: 2.9,
			CaCO3: 6.3
		},
		ions: Water.PROFILE_MUNICH_BEERSMITH
	},
	fermentation: [
		{
			date: "2019-08-03",
			step: BrewStep.WORT_FIRST,
			sg: 16.3,
			volume: 23.9
		},
		{
			date: "2019-08-03",
			step: BrewStep.WORT_BOILED,
			sg: 17.8,
			volume: 19.9
		},
		{
			date: "2019-08-03",
			step: BrewStep.FERM_PRIMARY,
			sg: 17.8,
			volume: 19.9,
			pinned: true
		},
		{
			date: "2019-08-23",
			step: BrewStep.FERM_SECONDARY,
			sg: 4.75,
			volume: 19.9,
			pinned: true
		},
		{
			date: "2019-09-09",
			step: BrewStep.BOTTLING,
			sg: 3.5,
			volume: 19.8,
			note: "Refermentacja: 100 g cukru Demerara"
		}
	],
	notes: [
		"Bok, czyli holenderski koźlak jesienny, ma być próbą zrobienia koźlaka przy braku możliwości obniżenia temperatury fermentacji do poziomu lagera.",
		"Początkowo celowałem w około 16°Blg, ale w dniu warzenia zauważyłem, że zostawiłem w recepturze wydajność na poziomie 70%, a zazwyczaj osiągam znacznie więcej, nawet przy podobnych wagowo zasypach. Wzrosła zatem oczekiwana gęstość, ale już mi się nie chciało przeliczać pozostałych parametrów i pogodziłem się z faktem, że wyjdzie mi koźlak jesienny dubeltowy.",
		"Obniżona wydajność, bo nie chciało mi się przygotowywać worka do filtracji chmielin i w garze został mniej więcej litr brzeczki. Byłoby coś w okolicach 77-78%, ale lenistwo ponownie wzięło górę.",
		"W trzecim dniu fermentacji wieczorem piana podeszła do rurki i zaczęła wydostawać się. Spróbowałem obniżyć temperaturę w pomieszczeniu przez otwarcie okna na noc. Zebrałem około 400ml drożdży z pokrywy fermentora zanim stwierdziłem, że w rurce już pojawia się dwutlenek węgla zamiast drożdży. Pokrywę wymieniłem na czystą i zdezynfekowaną, wraz ze świeżą rurką fermentacyjną. Pianę zbiłem zdezynfekowaną łygą.",
		"Próbka przy rozlewie miała ładny słodowy aromat, chlebowy, od razu sugerujący, że to koźlak. W smaku jednak piwo bardzo wytrawne i dość mocno alkoholowe. Zdecydowanie za mało słodyczy."
	],
	tastingNotes: [
		"Miedziana barwa, względnie klarowne już kilka dni po rozlewie, drożdże ładnie zbite na dnie butelki. Piany w zasadzie brak.",
		"Aromat przyjemny, mocno słodowy, chlebowy, typowy dla rasowego koźlaka. Bez ekscesów, ale bardzo poprawny.",
		"Smak, podobnie jak aromat, jest bardzo typowy dla koźlaka, bez specjalnych fajerwerków, ale poprawny, przyjemny. Wielki plus za niewyczuwalny alkohol - tego właśnie się obawiałem przy tak mocnym piwie.",
		"Myślę, że warto powtórzyć to piwo. Zobaczymy, jak się zachowa po przeleżakowaniu, ale wygląda całkiem obiecująco. Warto spróbować wersji dolnofermentacyjnej.",
		"Po dwóch miesiącach leżakowania, piwo się nieco zaokrągliło, a przy nalewaniu pojawia się całkiem ładna piana. Najwyraźniej przy pierwszej degustacji piwo jeszcze było ciut za młode."
	]
};
