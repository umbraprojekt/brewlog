import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch008: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Hard Hat",
	tags: [
		Tag.STOUT,
		Tag.ZYTO,
		Tag.PLATKI_DEBOWE
	],
	version: 1,
	batch: 8,
	style: Style.FES,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 18.5,
			og: 18.3,
			fg: 4.6,
			ibu: 60,
			ebc: 100,
			abv: 7.7,
			efficiency: 70,
			carbonation: 1.8,
			attenuation: 75
		},
		actual: {
			volume: 19.7,
			og: 18,
			fg: 6.5,
			abv: 6.5,
			efficiency: 72.9,
			attenuation: 63.9
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4, Unit.KILOGRAM)).pale.strzegom.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).rye.strzegom.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramel30.strzegom.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramelRye.strzegom.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).roastedBarley.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(60, Unit.MINUTE)).admiral.hopData(2015, 14.2).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(20, Unit.MINUTE)).fuggle.hopData(2015, 4.2).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).fuggle.hopData(2015, 4.2).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).s04.yeastCakeFrom(7).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(14, Unit.DAY)).flavour.name("płatki dębowe z beczki po koniaku").build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 21,
				temperature: 71,
				note: "Dodać słody jasne i karmelowe"
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 66,
				time: 75
			},
			{
				step: BrewStep.MASHOUT,
				note: "Dodać słód czekoladowy i prażony jęczmień",
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 9,
				temperature: 78,
				estimated: {
					sg: 16.3,
					volume: 22.2
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-06-07",
			step: BrewStep.WORT_FIRST,
			sg: 16.6,
			volume: 23
		},
		{
			date: "2016-06-07",
			step: BrewStep.WORT_BOILED,
			sg: 18,
			volume: 19.7
		},
		{
			date: "2016-06-07",
			step: BrewStep.FERM_PRIMARY,
			sg: 18,
			volume: 19.7,
			pinned: true
		},
		{
			date: "2016-06-21",
			step: BrewStep.FERM_SECONDARY,
			sg: 7,
			pinned: true
		},
		{
			date: "2016-07-06",
			step: BrewStep.BOTTLING,
			sg: 6.5,
			volume: 19,
			note: "Refermentacja: 60g cukru"
		}
	],
	notes: [
		"Poczynając od tej warki, receptury układałem w programie BeerSmith 2.",
		"Przy przelewaniu na cichą podzieliłem piwo na dwa fermentory, po połowie. Do jednego dodałem płatki dębowe."
	],
	tastingNotes: [
		"Wersja z płatkami dębowymi nie przeszła dębiną. Różnica między wersjami była niewyczuwalna. Być może za krótka cicha?",
		"Mimo wszystko piwo wyszło wyśmienite, półwytrawne, czekoladowe i bardzo lekko palone, gęste. Warto powtórzyć."
	]
};
