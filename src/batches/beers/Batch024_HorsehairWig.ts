import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch024: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Horsehair Wig",
	tags: [
		Tag.PORTER,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 24,
	style: Style.ENGLISH_PORTER,
	rating: 2,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 15.9,
			fg: 5,
			ibu: 72,
			ebc: 82,
			abv: 6,
			efficiency: 75,
			carbonation: 1.7,
			attenuation: 68.3
		},
		actual: {
			volume: 19.3,
			og: 15.9,
			fg: 10.5,
			abv: 3.1,
			efficiency: 72.5,
			attenuation: 34.1
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).marisOtter.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).amber.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).brown.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(110, Unit.GRAM), new Amount(60, Unit.MINUTE)).fuggle.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(20, Unit.MINUTE)).fuggle.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(15, Unit.MINUTE)).licorice.build(),
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(7, Unit.MINUTE)).fuggle.hopData(2015, 4.5).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(0, Unit.MINUTE)).fuggle.hopData(2015, 4.5).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(500, Unit.MILLILITRE)).s04.yeastCakeFrom(23).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 19,
				temperature: 74.6
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 69,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 12.72,
				temperature: 78,
				estimated: {
					sg: 13.7,
					volume: 24.7
				}
			}
		]
	],
	boilTime: 90,
	fermentation: [
		{
			date: "2017-01-28",
			step: BrewStep.WORT_FIRST,
			sg: 13.6,
			volume: 24.9
		},
		{
			date: "2017-01-28",
			step: BrewStep.WORT_BOILED,
			sg: 15.9,
			volume: 19.3
		},
		{
			date: "2017-01-28",
			step: BrewStep.FERM_PRIMARY,
			sg: 15.9,
			volume: 19.3,
			pinned: true
		},
		{
			date: "2017-02-26",
			step: BrewStep.FERM_SECONDARY,
			sg: 11,
			volume: 19,
			pinned: true
		},
		{
			date: "2017-03-14",
			step: BrewStep.BOTTLING,
			sg: 10.5,
			volume: 18.9,
			note: "Refermentacja: 70g cukru"
		}
	],
	notes: [
		"Receptura na bazie przepisu z 1776 roku, przedstawionego w książce \"Radical Brewing\" Randy'ego Moshera.",
		"Bardzo słabe odfermentowanie. Albo zasyp dał za małą siłę diastatyczną (2kg Maris Otter + 4kg słodu nieaktywnego enzymatycznie), albo przesadziłem z temperaturą (miałem uszkodzony termometr, więc nie wykluczam możliwości ugotowania enzymów zanim skończyły pracę).",
		"Piwo przegazowało się po kilku miesiącach. Były granaty, nawet z grubościennych butelek."
	],
	tastingNotes: [
		"Ekstremalnie mętne, prawdopodobnie od nieskonwertowanej skrobi. Smaczne, ale z kiloma wadami: zbyt wysoka goryczka, zbytnia paloność. Lukrecji nie było czuć wcale."
	]
};
