import { Type } from "../../enums/Type";
import { IBatch } from "../../types/IBatch";
import { Batch001 } from "./Batch001_BearskinCap";
import { Batch002 } from "./Batch002_WarBonnet";
import { Batch003 } from "./Batch003_AtticHelmet";
import { Batch004 } from "./Batch004_BattingHelmet";
import { Batch005 } from "./Batch005_StrawHat";
import { Batch006 } from "./Batch006_CowboyHat";
import { Batch007 } from "./Batch007_LeprechaunHat";
import { Batch008 } from "./Batch008_HardHat";
import { Batch009 } from "./Batch009_AviatorHat";
import { Batch010 } from "./Batch010_Mitre";
import { Batch011 } from "./Batch011_Kippah";
import { Batch012 } from "./Batch012_AtticHelmetV2";
import { Batch013 } from "./Batch013_Panama";
import { Batch014 } from "./Batch014_HardeeHat";
import { Batch015 } from "./Batch015_Bicorne";
import { Batch016 } from "./Batch016_TamOShanter";
import { Batch017 } from "./Batch017_Glengarry";
import { Batch018 } from "./Batch018_Tricorne";
import { Batch019 } from "./Batch019_LeprechaunHatV2";
import { Batch020 } from "./Batch020_MonomakhsCap";
import { Batch021 } from "./Batch021_Tirolerhut";
import { Batch022 } from "./Batch022_DivingHelmet";
import { Batch023 } from "./Batch023_CanterburyCap";
import { Batch024 } from "./Batch024_HorsehairWig";
import { Batch025 } from "./Batch025_BeaverHat";
import { Batch026 } from "./Batch026_Souwester";
import { Batch027 } from "./Batch027_Beanie";
import { Batch028 } from "./Batch028_AviatorHatV2";
import { Batch029 } from "./Batch029_HardHatV2";
import { Batch030 } from "./Batch030_Caubeen";
import { Batch031 } from "./Batch031_Podstok";
import { Batch032 } from "./Batch032_BattingHelmetV2";
import { Batch033 } from "./Batch033_StrawHatV2";
import { Batch034 } from "./Batch034_CowboyHatV2";
import { Batch035 } from "./Batch035_Fez";
import { Batch036 } from "./Batch036_AtticHelmetV3";
import { Batch037 } from "./Batch037_Dastar";
import { Batch038 } from "./Batch038_Gorlatnaya";
import { Batch039 } from "./Batch039_Tubeteika";
import { Batch040 } from "./Batch040_Bascinet";
import { Batch041 } from "./Batch041_WarBonnetV2";
import { Batch042 } from "./Batch042_Bycocket";
import { Batch043 } from "./Batch043_GainsboroughHat";
import { Batch044 } from "./Batch044_Boater";
import { Batch045 } from "./Batch045_Chupalla";
import { Batch046 } from "./Batch046_Sallet";
import { Batch047 } from "./Batch047_Leghorn";
import { Batch048 } from "./Batch048_AtticHelmetV4";
import { Batch049 } from "./Batch049_WhoopeeCap";
import { Batch050 } from "./Batch050_DutchBonnet";
import { Batch051 } from "./Batch051_SouwesterV2";
import { Batch052 } from "./Batch052_Billycock";
import { Batch053 } from "./Batch053_BycocketV2";
import { Batch054 } from "./Batch054_Deerstalker";
import { Batch055 } from "./Batch055_Shako";
import { Batch056 } from "./Batch056_Calotte";
import { Batch057 } from "./Batch057_Biretta";
import { Batch058 } from "./Batch058_Hood";

const batches: Array<IBatch> = [
	Batch001,
	Batch002,
	Batch003,
	Batch004,
	Batch005,
	Batch006,
	Batch007,
	Batch008,
	Batch009,
	Batch010,
	Batch011,
	Batch012,
	Batch013,
	Batch014,
	Batch015,
	Batch016,
	Batch017,
	Batch018,
	Batch019,
	Batch020,
	Batch021,
	Batch022,
	Batch023,
	Batch024,
	Batch025,
	Batch026,
	Batch027,
	Batch028,
	Batch029,
	Batch030,
	Batch031,
	Batch032,
	Batch033,
	Batch034,
	Batch035,
	Batch036,
	Batch037,
	Batch038,
	Batch039,
	Batch040,
	Batch041,
	Batch042,
	Batch043,
	Batch044,
	Batch045,
	Batch046,
	Batch047,
	Batch048,
	Batch049,
	Batch050,
	Batch051,
	Batch052,
	Batch053,
	Batch054,
	Batch055,
	Batch056,
	Batch057,
	Batch058
]
	.map(batch => ({
		...batch,
		type: Type.BEER,
		prev: undefined,
		next: undefined,
		url: `/warki/${batch.batch}`
	}))
	.sort((a, b) => b.batch - a.batch);

export const Beers: Array<IBatch> = batches.map(batch => {
	batch.prev = batches.find(item => item.batch === batch.batch - 1);
	batch.next = batches.find(item => item.batch === batch.batch + 1);
	return batch;
});
