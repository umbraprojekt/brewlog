import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch045: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Chupalla",
	tags: [
		Tag.PSZENICA,
		Tag.OWIES,
		Tag.RUMIANEK,
		Tag.KOLENDRA,
		Tag.CURACAO,
		Tag.CHMIEL_NOWOFALOWY,
		Tag.IPA,
		Tag.BELGIAN
	],
	version: 1,
	batch: 45,
	style: Style.WHITE_IPA,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 14.2,
			fg: 2.8,
			ibu: 57,
			ebc: 7,
			abv: 6.2,
			efficiency: 70,
			carbonation: 2.5,
			attenuation: 80.5
		},
		actual: {
			og: 14.1,
			fg: 2,
			abv: 6.5,
			volume: 19.5,
			efficiency: 67.9,
			attenuation: 85.8
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(2.5, Unit.KILOGRAM)).pils.castle.build(),
			new MashIngredientBuilder(new Amount(2.5, Unit.KILOGRAM)).wheatUnmalted.viking.build(),
			new MashIngredientBuilder(new Amount(0.4, Unit.KILOGRAM)).oatFlakes.build(),
			new MashIngredientBuilder(new Amount(150, Unit.GRAM)).acidulating.bestmalz.build(),
			new MashIngredientBuilder(new Amount(250, Unit.GRAM)).speltHull.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(20, Unit.MINUTE)).amarillo.hopData(2017, 8.1).build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(20, Unit.MINUTE)).cascade.hopData(2017, 5.5).build(),
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(7, Unit.MINUTE)).amarillo.hopData(2017, 8.1).build(),
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(7, Unit.MINUTE)).cascade.hopData(2017, 5.5).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(5, Unit.MINUTE)).camomile.build(),
			new WetIngredientBuilder(new Amount(17, Unit.GRAM), new Amount(5, Unit.MINUTE)).coriander.build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(5, Unit.MINUTE)).curacao.build(),
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(150, Unit.MILLILITRE)).fm20.yeastCakeFrom(44).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(100, Unit.GRAM), new Amount(4, Unit.DAY)).amarillo.hopDry.hopData(2017, 8.1).build(),
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(4, Unit.DAY)).cascade.hopDry.hopData(2017, 5.5).build()
		]
	},
	water: {
		profile: "Antwerpia + chmiel",
		base: new TapWater2018(5.8),
		additions: {
			CaSO4: 5,
			NaCl: 1,
			MgSO4: 2,
			CaCl2: 2,
			NaHCO3: 0.5,
			CaCO3: 1
		},
		dilution: new DistilledWater(25),
		ions: {
			Ca: 83,
			Mg: 7.7,
			Na: 25.1,
			SO4: 125.1,
			Cl: 58.4,
			HCO3: 85.8
		}
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 21,
				temperature: 68,
				time: 120
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 64,
				time: 120
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 9.8,
				temperature: 78,
				estimated: {
					sg: 12.4,
					volume: 24.2
				}
			}
		]
	],
	boilTime: 75,
	fermentation: [
		{
			date: "2018-07-01",
			step: BrewStep.WORT_FIRST,
			sg: 12.3,
			volume: 25
		},
		{
			date: "2018-07-01",
			step: BrewStep.WORT_BOILED,
			sg: 14.1,
			volume: 19.5
		},
		{
			date: "2018-07-01",
			step: BrewStep.FERM_PRIMARY,
			sg: 14.1,
			volume: 19.5,
			pinned: true
		},
		{
			date: "2018-07-15",
			step: BrewStep.FERM_SECONDARY,
			sg: 3.5,
			volume: 19.3,
			pinned: true
		},
		{
			date: "2018-08-01",
			step: BrewStep.BOTTLING,
			sg: 2,
			volume: 18.5,
			note: "Refermentacja: 113g cukru"
		}
	],
	notes: [
		"Planowany profil wody: Antwerpia, ale ze zwiększoną twardością siarczanową (burtonizacja) w celu uwypuklenia chmielowości."
	],
	tastingNotes: [
		"Barwa ciemnozłota; za ciemna, celowałem w znacznie jaśniejsze piwo. Biała, niska piana, szybko znika.",
		"Aromat przede wszystkim chmielowy: cytrusy i mango. Za nimi belgijska pieprzność od drożdży. Kolendra i rumianek niewyczuwalne, prawdopodobnie przykryte chmielem. Bardzo intensywny, przyjemny aromat.",
		"W smaku piwo jest bardzo wytrawne. Chmielowość zdecydowanie na pierwszym miejscu, ale goryczka jest co najwyżej średnia, mnóstwo za to smaku. Cytrus, lekka przyprawowość. Daje się wyczuć delikatny alkohol.",
		"Pomimo dużego udziału pszenicy i owsa, piwo nie sprawia wrażenia gęstego. Jest gładkie, ale lekkie. Nagazowanie umiarkowane do wysokiego, wyraźnie musuje w ustach.",
		"Całkiem przyjemne piwo. Niepozbawione niedociągnięć, ale zasadniczo bardzo smaczne, rześkie, pijalne.",
		"Na pewno do powtórzenia. Do poprawy: spróbować zrobić krótką przerwę białkową albo zatrzeć na dwa gary, zgodnie z wytycznymi Randy'ego Moshera; może poprawi to pianę. Uważać na przypalanie się pszenicy podczas kleikowania (przypuszczam, że to wpłynęło na ciemnawą barwę piwa)."
	]
};
