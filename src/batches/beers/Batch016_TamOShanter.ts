import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch016: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Tam o' Shanter",
	tags: [
		Tag.SCOTTISH_ALE,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 16,
	style: Style.SCOTTISH_EXPORT,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 24,
			og: 11.4,
			fg: 3.4,
			ibu: 25,
			ebc: 31,
			abv: 4.3,
			efficiency: 70,
			carbonation: 1.8,
			attenuation: 70.4
		},
		actual: {
			volume: 25,
			og: 12.7,
			fg: 4.5,
			abv: 4.4,
			efficiency: 81.8,
			attenuation: 64.6
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramel300.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(45, Unit.MINUTE)).endeavour.hopData(2015, 7.5).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(20, Unit.MINUTE)).endeavour.hopData(2015, 7.5).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).endeavour.hopData(2015, 7.5).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).fm12.yeastStarter.build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 17,
				temperature: 76
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 70,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 17,
				temperature: 78,
				estimated: {
					sg: 10.4,
					volume: 27.5
				}
			}
		]
	],
	boilTime: 45,
	fermentation: [
		{
			date: "2016-10-15",
			step: BrewStep.WORT_FIRST,
			sg: 11.5,
			volume: 28.3
		},
		{
			date: "2016-10-15",
			step: BrewStep.WORT_BOILED,
			sg: 12.7,
			volume: 25
		},
		{
			date: "2016-10-15",
			step: BrewStep.FERM_PRIMARY,
			sg: 12.7,
			volume: 25,
			pinned: true
		},
		{
			date: "2016-10-25",
			step: BrewStep.FERM_SECONDARY,
			sg: 5,
			volume: 24.7,
			pinned: true
		},
		{
			date: "2016-11-13",
			step: BrewStep.BOTTLING,
			sg: 4.5,
			volume: 24.2,
			note: "Refermentacja: 50g cukru"
		}
	],
	notes: [
		"Po pewnym czasie mocno się przegazowało."
	],
	tastingNotes: [
		"Bardziej zmętnione niż większość moich piw, ale bardzo smaczne i bogate: orzechowy słód i ziołowo-tytoniowy chmiel, lekkie, ale nie wodniste. Warto powtórzyć."
	]
};
