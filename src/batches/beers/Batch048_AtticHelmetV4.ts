import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { Water } from "../../ingredients/water/Water";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch048: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Attic Helmet",
	tags: [
		Tag.APA,
		Tag.CHMIEL_NOWOFALOWY,
		Tag.OWIES,
		Tag.SINGLE_HOP
	],
	version: 4,
	original: 36,
	batch: 48,
	style: Style.APA,
	stock: 35,
	rating: 1,
	params: {
		planned: {
			volume: 23,
			og: 12.1,
			fg: 2.8,
			ibu: 45,
			ebc: 8,
			abv: 5,
			efficiency: 78,
			carbonation: 2.3,
			attenuation: 76.8
		},
		actual: {
			volume: 26.1,
			og: 12,
			efficiency: 87.7,
			fg: 2.5,
			abv: 5.1,
			attenuation: 79.2
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4.5, Unit.KILOGRAM)).pale.bestmalz.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).oatFlakes.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(20, Unit.MINUTE)).simcoe.hopData(2018, 12.4).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).simcoe.hopData(2018, 12.4).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(200, Unit.MILLILITRE)).us05.yeastCakeFrom(47).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(80, Unit.GRAM), new Amount(4, Unit.DAY)).hopDry.simcoe.hopData(2017, 12.4).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 15,
				temperature: 72.7
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 17.9,
				temperature: 78,
				estimated: {
					sg: 10.9,
					volume: 26.9
				}
			}
		]
	],
	boilTime: 60,
	water: {
		profile: "Burton On Trent (wg Raya Danielsa)",
		base: new DistilledWater(33),
		additions: {
			CaSO4: 41.7,
			NaCl: 1.9,
			MgSO4: 8.1
		},
		ions: Water.PROFILE_BURTON_ON_TRENT_DANIELS
	},
	fermentation: [
		{
			date: "2019-07-06",
			step: BrewStep.WORT_FIRST,
			sg: 11.9,
			volume: 27.5
		},
		{
			date: "2019-07-06",
			step: BrewStep.WORT_BOILED,
			sg: 13.2,
			volume: 23.6
		},
		{
			date: "2019-07-06",
			step: BrewStep.FERM_PRIMARY,
			sg: 12,
			volume: 26.1,
			note: "Rozcieńczenie: 2,5 l wody",
			pinned: true
		},
		{
			date: "2019-07-20",
			step: BrewStep.FERM_SECONDARY,
			sg: 2.5,
			volume: 26,
			pinned: true
		},
		{
			date: "2019-08-04",
			step: BrewStep.BOTTLING,
			sg: 2.5,
			volume: 25.5,
			note: "Refermentacja: 139 g cukru"
		}
	],
	notes: [
		"Czwarte podejście do APA single hop. W stosunku do warki #36 delikatnie zmieniony zasyp (usunięta Carafa, niewielka część słodu zamieniona na płatki owsiane dla poprawienia pienistości), zaś chmiel na zimno nie będzie zadany na całość cichej, a tylko na ostatnie 4 dni.",
		"Zmiana chmielu na Simcoe wynika z faktu, że chmiel Warrior ze zbioru 2018 nie był nigdzie dostępny w momencie robienia zakupów.",
		"Miało być docelowo 40, a nie 45 IBU, ale pomyliłem dawki chmielu do gotowania i wrzuciłem 40g na 20 minut i 30g na 7 minut zamiast odwrotnie.",
		"Następnego dnia po nastawieniu piwa nie było żadnyh oznak fermentacji. Gęstwa opadła na dno i nie wykazała żadnej aktywności. Zadałem awaryjnie około 200ml użyczonej mi przez innego piwowara gęstwy US-05 po grodziskim. Po kilku godzinach na szczęście fermentacja ruszyła.",
		"Podczas przelewania na cichą bardzo mocny aromat czegoś podobnego do aldehydu octowego, identycznie jak w warkach #9 i #35. Mam nadzieję, że to jeszcze zdąży zniknąć i dodatkowo zostanie przykryte chmielem.",
		"Przy butelkowaniu niestety nadal był bardzo mocny aromat aldehydu, dodatkowo w ogóle nie było czuć chmielu. Podejrzewam, że lag przy starcie dał okienko innym drobnoustrojom do rozwinięcia się i narozrabiania. Piwo niestety zapowiada się bardzo słabo."
	],
	tastingNotes: [
		"Mętne, jasnozłociste, bez piany. Zamiast niej, na powierzchni piwa pojawiają się brzydko wyglądające \"wysepki\" przypominające jakieś farfocle czy pływający osad. Nieapetyczne.",
		"Aromat i smak zdominowane przez to dziwne coś podobne do aldehydu, ale nim niebędące. Nie mam pojęcia, co to, ale jest dość paskudne, bo zagłusza wszystko inne. Goryczka niska, chmielowości brak. W skrócie: raczej nie nadaje się do picia.",
		"Piwo trzeba uwarzyć od nowa, bo coś poszło bardzo nie tak. Na podstawie tej warki ciężko podjąć jakieś decyzje na temat tego, co warto kontynuować, a co zmienić."
	]
};
