import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch034: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Cowboy Hat",
	tags: [
		Tag.PSZENICA,
		Tag.WEIZENBOCK,
		Tag.BOCK,
		Tag.SINGLE_HOP,
		Tag.BEZALKOHOLOWE
	],
	version: 2,
	original: 6,
	batch: 34,
	style: Style.WEIZENBOCK,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			volume: 18,
			og: 16.1,
			fg: 3.7,
			ibu: 25,
			ebc: 35,
			abv: 6.8,
			efficiency: 65,
			carbonation: 2.4,
			attenuation: 77.1
		},
		actual: {
			volume: 18.7,
			og: 16,
			fg: 4.5,
			abv: 6.3,
			efficiency: 67.1,
			attenuation: 71.9
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(3, Unit.KILOGRAM)).wheat.weyermann.build(),
			new MashIngredientBuilder(new Amount(3, Unit.KILOGRAM)).vienna.weyermann.build(),
			new MashIngredientBuilder(new Amount(0.25, Unit.KILOGRAM)).caraWheat.build(),
			new MashIngredientBuilder(new Amount(75, Unit.GRAM)).carafa3Special.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(60, Unit.MINUTE)).hallertauTradition.hopData(2016, 3.7).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(20, Unit.MINUTE)).hallertauTradition.hopData(2016, 3.7).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(7, Unit.MINUTE)).hallertauTradition.hopData(2016, 3.7).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).fm41.yeastCakeFrom(33).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 20,
				temperature: 72.4,
				note: "Dodajemy słody jasne i karmelowe"
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10,
				note: "Dodajemy słód palony"
			},
			{
				step: BrewStep.SPARGE,
				volume: 10,
				temperature: 78,
				estimated: {
					sg: 14.2,
					volume: 21.7
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2018-03-04",
			step: BrewStep.WORT_FIRST,
			sg: 15.2,
			volume: 20.1
		},
		{
			date: "2018-03-04",
			step: BrewStep.WORT_BOILED,
			sg: 17.5,
			volume: 17,
			note: "Rozcieńczona 1,7l wody"
		},
		{
			date: "2018-03-04",
			step: BrewStep.FERM_PRIMARY,
			sg: 16,
			volume: 18.7,
			pinned: true
		},
		{
			date: "2018-04-04",
			step: BrewStep.BOTTLING,
			sg: 4.5,
			volume: 18,
			note: "Refermentacja: 100g cukru"
		}
	],
	notes: [
		"Powtórka warki #6, ale z niższym ekstraktem początkowym (16,3 zamiast 18,5°Blg) i zacieraniem jednotemperaturowym.",
		"Profil wody: 50% kranówka, 50% z filtra Brita.",
		"Zacieranie wyszło w nieco wyższej temperaturze; zaczęło się w 70°C i nie dogrzewałem. Po godzinie temperatura spadła do 62°C.",
		"Podobnie jak w przypadku #33, podzieliłem warkę przy rozlewie na pół. 9 litrów zabutelkowałem, a drugie 9 przetrzymałem w 80°C przez 30 minut w celu odparowania alkoholu. Ubytek wody i alkoholu uzupełniłem 0,4l przegotowanej kranówki."
	],
	tastingNotes: [
		"Wersja 0%: bardzo niskie nagazowanie. Przyjemny słodowy aromat podbity słodyczą banana. W smaku podobnie, z tym, że w finiszu dochodzi przyjemna opiekaność. Wydaje się być dwunastką.",
		"Wersja podstawowa: wysycenie średnie, być może jeszcze nieco za niskie, ale przyjemne. Aromat to ciekawy balans słodowości, bananów i goździków. W smaku podobnie. Gdzieś tam w tle pałęta się leciutki alkohol. Piwo sprawia wrażenie gęstego, oblepiającego jamę ustną. Gładkie i przyjemne, bardzo satysfakcjonujące.",
		"Nie ma różnic w barwie, obie wersje są też mocno zmętnione.",
		"Zdecydowanie wygrywa Weizenbock z alkoholem: jest wyżej wysycony, sprawia wrażenie pełniejszego i ma sporo goździka, którego brakuje w wersji bezalkoholowej. Wersja 0% jest niezła, podstawowa - bardzo dobra. Muszę częściej powtarzać. Nuty opiekane nie są w stylu, więc zastanowię się nad użyciem ciemnego słodu monachijskiego zamiast dodatku słodu palonego."
	]
};
