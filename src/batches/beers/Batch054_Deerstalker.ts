import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { Water } from "../../ingredients/water/Water";

export const Batch054: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Deerstalker",
	tags: [
		Tag.BURTON_ALE
	],
	version: 1,
	batch: 54,
	style: Style.BURTON_ALE,
	stock: 0,
	params: {
		planned: {
			volume: 12,
			og: 17.9,
			fg: 4.2,
			ibu: 105,
			ebc: 46,
			abv: 7.6,
			efficiency: 70,
			carbonation: 2.0,
			attenuation: 76.4
		},
		actual: {
			volume: 11.3,
			og: 18.5,
			efficiency: 68.1
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(3, Unit.KILOGRAM)).marisOtter.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).amber.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).munich1.bestmalz.build(),
			new MashIngredientBuilder(new Amount(0.1, Unit.KILOGRAM)).darkChocolate.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(130, Unit.GRAM), new Amount(60, Unit.MINUTE)).ekg.hopData(2018, 4.1).build(),
			new WetIngredientBuilder(new Amount(3, Unit.GRAM), new Amount(10, Unit.MINUTE)).nutrient.build(),
			new WetIngredientBuilder(new Amount(2.5, Unit.GRAM), new Amount(10, Unit.MINUTE)).irishMoss.build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).fm10.yeastCakeFrom(53).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 13.3,
				temperature: 72.2
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 9.2,
				temperature: 78,
				estimated: {
					sg: 13.7,
					volume: 17.3
				}
			}
		]
	],
	boilTime: 120,
	water: {
		profile: "Burton On Trent (wg Raya Danielsa)",
		base: new DistilledWater(22.5),
		additions: {
			CaSO4: 28.4,
			NaCl: 1.3,
			MgSO4: 5.5
		},
		ions: Water.PROFILE_BURTON_ON_TRENT_DANIELS
	},
	fermentation: [
		{
			date: "2019-12-20",
			step: BrewStep.WORT_FIRST,
			sg: 13.2,
			volume: 17.6
		},
		{
			date: "2019-12-20",
			step: BrewStep.WORT_BOILED,
			sg: 18.5,
			volume: 11.3
		},
		{
			date: "2019-12-20",
			step: BrewStep.FERM_PRIMARY,
			sg: 18.5,
			volume: 11.3,
			pinned: true
		},
		{
			date: "2020-01-21",
			step: BrewStep.FERM_SECONDARY,
			sg: 10,
			volume: 11.3
		},
	],
	notes: [
		"Podwójne wysładzanie: całość brzeczki zebrana po normalnym wysładzaniu, podgrzana i młóto zalane ponownie w celu ponownego przefiltrowania. Niestety nie zauważyłem zmiany gęstości.",
		"Po miesięcznej burzliwej, zlałem piwo na roczną cichą do szklanego słoja. Odebrałem litr i rozlałem w 3 butelki 0,33, dodając do każdej 1,4 g cukru. Butelki posłużą do porównania leżakowania w butelce i w balonie.",
		"Gęstość po cichej dramatycznie wysoka. Tak jak w poprzednich dwóch warkach, albo zbyt wysoka temperatura zacierania (pirometr?), albo szykują się granaty. Mógłbym spróbować dodać nieco glukoamylazy do słoja, ale nie chcę mieć innego piwa w słoju niż w butelkach."
	],
	tastingNotes: []
};
