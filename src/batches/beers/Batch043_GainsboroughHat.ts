import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch043: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Gainsborough Hat",
	tags: [
		Tag.OLD_ALE,
		Tag.SINGLE_HOP,
		Tag.PLATKI_DEBOWE
	],
	version: 1,
	batch: 43,
	style: Style.OLD_ALE,
	rating: 4,
	stock: 25,
	params: {
		planned: {
			volume: 17.5,
			og: 20,
			fg: 6.5,
			ibu: 54,
			ebc: 37,
			abv: 7.7,
			efficiency: 65,
			carbonation: 2,
			attenuation: 67.6
		},
		actual: {
			og: 20,
			fg: 7,
			efficiency: 69.6,
			volume: 18.7,
			abv: 7.4,
			attenuation: 65
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(6, Unit.KILOGRAM)).marisOtter.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caraClair.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramelAromatic.build(),
			new MashIngredientBuilder(new Amount(0.25, Unit.KILOGRAM)).brown.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(75, Unit.GRAM), new Amount(60, Unit.MINUTE)).challenger.hopData(2017, 5.3).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(20, Unit.MINUTE)).challenger.hopData(2017, 5.3).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(15, Unit.MINUTE)).irishMoss.build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(10, Unit.MINUTE)).nutrient.build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(7, Unit.MINUTE)).challenger.hopData(2017, 5.3).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).fm11.yeastCakeFrom(42).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(60, Unit.DAY)).name("płatki dębowe Pure Vanilla").build()
		]
	},
	water: {
		profile: "Edynburg + chmiel",
		base: new TapWater2018(16.3),
		additions: {
			CaSO4: 8,
			MgSO4: 2,
			NaHCO3: 2
		},
		dilution: new DistilledWater(15),
		ions: {
			Ca: 99.6,
			Mg: 10,
			Na: 39.3,
			SO4: 193,
			Cl: 18.7,
			HCO3: 197
		}
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 22,
				temperature: 76
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 70,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 9.3,
				temperature: 78,
				estimated: {
					sg: 16.3,
					volume: 23
				}
			}
		]
	],
	boilTime: 120,
	fermentation: [
		{
			date: "2018-05-26",
			step: BrewStep.WORT_FIRST,
			sg: 17,
			volume: 23.5
		},
		{
			date: "2018-05-26",
			step: BrewStep.WORT_BOILED,
			sg: 21.5,
			volume: 17.3,
			note: "Rozcieńczona 1,4l wody"
		},
		{
			date: "2018-05-26",
			step: BrewStep.FERM_PRIMARY,
			sg: 20,
			volume: 18.7,
			pinned: true
		},
		{
			date: "2018-06-10",
			step: BrewStep.FERM_SECONDARY,
			sg: 7.5,
			volume: 18.2,
			pinned: true
		},
		{
			date: "2018-08-09",
			step: BrewStep.BOTTLING,
			sg: 7,
			volume: 18,
			note: "Refermentacja: 82g cukru"
		}
	],
	notes: [
		"Piwo przeznaczone do dłuższego leżakowania, wybór padł na old ale ze względu na złożony charakter. Zasyp jest intencjonalnie dość urozmaicony, a złożoności dodać mają jeszcze płatki dębowe podczas miesięcznej fermentacji cichej.",
		"Profil zbliżony do wody edynburskiej, stosownej do brązowych, słodowych piw, ale z nieco zmniejszoną twardością węglanową, a zwiększoną siarczanową (mniej więcej zgodnie z wytycznymi Raya Danielsa z \"Designing Great Beers\").",
		"Pomimo długiej cichej, piwo dość mocno przegazowało się po dwóch miesiącach od zabutelkowania. Musiałem odgazować wszystkie pozostałe butelki."
	],
	tastingNotes: [
		"Barwa miedziana. Klarowne, z niską pianą barwy kremowej, stosunkowow drobną i niezbyt trwałą.",
		"W aromacie jest sporo słodu pomieszanego z wanilią. Od razu czuć płatki dębowe.",
		"Smak umiarkowanie zbalansowany: lekka słodowość, troszkę karmelu, wyraźna chmielowa goryczka i ponownie wyraźne płatki dębowe. Niestety, płatki za bardzo dominują.",
		"Umiarkowanie treściwe, średnio wysycone.",
		"Nawet udane piwko. Płatki co prawda za bardzo dominują i następnym razem spróbuję leżakowania przez 45 dni zamiast 60, ale poza tym mankamentem jest całkiem dobrze. Piwo nie jest jakieś mega złożone, ale piję je zaledwie dwa tygodnie po zabutelkowaniu. Złożoność powinna przyjść z czasem, kiedy dojdą nuty utlenienia."
	]
};
