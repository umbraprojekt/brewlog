import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch002: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "War Bonnet",
	tags: [
		Tag.BROWN_ALE,
		Tag.CHMIEL_NOWOFALOWY,
		Tag.EKSTRAKT
	],
	version: 1,
	batch: 2,
	style: Style.AMERICAN_BROWN_ALE,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			ibu: 50
		},
		actual: {
			og: 15
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(1.7, Unit.KILOGRAM)).lightLE.bruntal.build(),
			new MashIngredientBuilder(new Amount(1.7, Unit.KILOGRAM)).darkLE.bruntal.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM)).tomahawk.build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM)).willamette.build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).dryYeast.us05.build()
		]
	},
	mash: [],
	fermentation: [],
	notes: [
		"Pierwsza warka z samodzielnym chmieleniem. Niestety szczegóły receptury ani notatki dotyczące fermentacji się nie zachowały."
	],
	tastingNotes: [
		"Piwo wspominam jako bardzo udane, z ciekawymi nutami korzennymi fajnie współgrającymi ze słodowością."
	]
};
