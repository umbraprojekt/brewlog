import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch023: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Canterbury Cap",
	tags: [
		Tag.PORTER,
		Tag.MALE_PIWO,
		Tag.EKSTRAKT
	],
	version: 1,
	batch: 23,
	style: Style.ENGLISH_PORTER,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			volume: 10,
			og: 11.5,
			ibu: 31,
			carbonation: 2
		},
		actual: {
			volume: 9.2,
			og: 12,
			fg: 5.5,
			abv: 3.5
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(13, Unit.LITRE)).wort.name("wody wysłodkowe po warce #22, 7,7°Blg").build(),
			new MashIngredientBuilder(new Amount(190, Unit.GRAM)).dme.build(),
			new MashIngredientBuilder(new Amount(25, Unit.GRAM)).carafa3Special.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(60, Unit.MINUTE)).marynka.hopData(2015, 9.5).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(20, Unit.MINUTE)).marynka.hopData(2015, 9.5).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(7, Unit.MINUTE)).marynka.hopData(2015, 9.5).build(),
			new WetIngredientBuilder(new Amount(6, Unit.GRAM), new Amount(7, Unit.MINUTE)).lubelski.hopData(2015, 3.6).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).s04.dryYeast.build()
		]
	},
	mash: [],
	boilTime: 120,
	fermentation: [
		{
			date: "2017-01-10",
			step: BrewStep.WORT_BOILED,
			sg: 12,
			volume: 9.2
		},
		{
			date: "2017-01-10",
			step: BrewStep.FERM_PRIMARY,
			sg: 12,
			volume: 9.2,
			pinned: true
		},
		{
			date: "2017-01-25",
			step: BrewStep.FERM_SECONDARY,
			sg: 6,
			volume: 8.9,
			pinned: true
		},
		{
			date: "2017-02-12",
			step: BrewStep.BOTTLING,
			sg: 5.5,
			volume: 8.7,
			note: "Refermentacja: 40g cukru"
		}
	],
	notes: [
		"Warka nie była planowana, ale szkoda było zmarnować wiaderka wód wysłodkowych.",
		"Proporcje składników dobrane tylko z pomocą kalkulatora, więc nie ma kompletnych danych dotyczących prognozowanych parametrów.",
		"Ekstrakt słodowy służył tylko podbiciu Blg.",
		"Słód palony użyty tylko w celu przyciemnienia brzeczki. Odebrałem część brzeczki z kotła, podgrzałem w rondelku i dodałem rozdrobniony słód. Przetrzymałem 10 minut, odfiltrowałem za pomocą filtra do przelewowego ekspresu do kawy i ciemną brzeczkę wlałem z powrotem do gara warzelnego.",
		"Piwo nie dofermentowało i po jakimś czasie zrobiły się granaty."
	],
	tastingNotes: [
		"Całkiem smaczny porter, czekoladowy, lekki. Niestety, przegazowany."
	]
};
