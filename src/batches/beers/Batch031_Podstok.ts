import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch031: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Podstok",
	tags: [
		Tag.SINGLE_MALT,
		Tag.SINGLE_HOP,
		Tag.PALE_ALE
	],
	version: 1,
	batch: 31,
	style: Style.PALE_ALE,
	rating: 5,
	stock: 0,
	params: {
		planned: {
			volume: 23,
			og: 11.7,
			fg: 2.2,
			ibu: 40,
			ebc: 6,
			abv: 5.1,
			efficiency: 75,
			carbonation: 2.5,
			attenuation: 81.4
		},
		actual: {
			volume: 25.7,
			og: 12,
			fg: 1.5,
			abv: 5.6,
			efficiency: 86.4,
			attenuation: 87.5
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).pils.viking.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(60, Unit.GRAM), new Amount(60, Unit.MINUTE)).saaz.hopData(2016, 3).build(),
			new WetIngredientBuilder(new Amount(60, Unit.GRAM), new Amount(20, Unit.MINUTE)).saaz.hopData(2016, 3).build(),
			new WetIngredientBuilder(new Amount(5, Unit.GRAM), new Amount(10, Unit.MINUTE)).irishMoss.build(),
			new WetIngredientBuilder(new Amount(80, Unit.GRAM), new Amount(7, Unit.MINUTE)).saaz.hopData(2016, 3).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).us05.yeastCakeFrom(30).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(100, Unit.GRAM), new Amount(21, Unit.DAY)).saaz.hopDry.hopData(2016, 3).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 15,
				temperature: 70.4
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 65,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 17.9,
				temperature: 78,
				estimated: {
					sg: 10.5,
					volume: 26.9
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2017-10-15",
			step: BrewStep.WORT_FIRST,
			sg: 11.7,
			volume: 29,
			note: "Podejrzewam błąd pomiaru gęstości"
		},
		{
			date: "2017-10-15",
			step: BrewStep.WORT_BOILED,
			sg: 12.9,
			volume: 23.8,
			note: "Rozcieńczona 1,9l wody"
		},
		{
			date: "2017-10-15",
			step: BrewStep.FERM_PRIMARY,
			sg: 12,
			volume: 25.7,
			pinned: true
		},
		{
			date: "2017-10-25",
			step: BrewStep.FERM_SECONDARY,
			sg: 2,
			volume: 25,
			pinned: true
		},
		{
			date: "2017-11-16",
			step: BrewStep.BOTTLING,
			sg: 1.5,
			volume: 23.7,
			note: "Refermentacja: 134g cukru"
		}
	],
	notes: [
		"W założeniu piwo miało być górnofermentacyjną \"podróbką\" czeskiego pilsa - stąd słód pilzneński w zasypie i obfite chmielenie Żateckim.",
		"Żeby zbliżyć się nieco do profilu wody w Pilźnie, użyłem do warzenia 10l wody źródlanej o mineralizacji 350mg/l, 20l wody demineralizowanej i 3l wody przefiltrowanej w filtrze Brita.",
		"Zbyt wysoka wydajność wysładzania. W związku z tym zmieniłem chmielenie: 80g na 60 min., 80g na 20 min., 80g na 7 min., 60g na cichą, tak by zachować szacowane 40 IBU."
	],
	tastingNotes: [
		"Świetne, mega pijalne piwo. Chmiel jest dość agresywny jak na \"podróbkę\" pilsa, ale ma bardzo przyjemny aromat, jednoznacznie kojarzący się z najlepszymi czeskimi pilsami. Barwa bardzo ciemna jak na 100% słodu pilzneńskiego, trochę to dziwne. Mimo to, piwo zdecydowanie do powtórki!"
	]
};
