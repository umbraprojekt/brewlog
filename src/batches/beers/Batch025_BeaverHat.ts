import { BrewStep } from "../../enums/BrewStep";
import { IngredientType } from "../../enums/IngredientType";
import { Producer } from "../../enums/Producer";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { Ingredient } from "../../ingredients/Ingredient";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch025: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Beaver Hat",
	tags: [
		Tag.BROWN_ALE,
		Tag.WANILIA,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 25,
	style: Style.BROWN_ALE,
	rating: 2,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 17,
			fg: 4.6,
			ibu: 30,
			ebc: 47,
			abv: 6.9,
			efficiency: 75,
			carbonation: 1.7,
			attenuation: 72.8
		},
		actual: {
			volume: 19,
			og: 17.6,
			fg: 6.5,
			abv: 6.2,
			efficiency: 73.6,
			attenuation: 63.1
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4.25, Unit.KILOGRAM)).mild.build(),
			new MashIngredientBuilder(new Amount(2, Unit.KILOGRAM)).amber.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(90, Unit.MINUTE)).northdown.hopData(2015, 8.2).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(20, Unit.MINUTE)).northdown.hopData(2015, 8.2).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(7, Unit.MINUTE)).northdown.hopData(2015, 8.2).build(),
			new WetIngredientBuilder(new Amount(1, Unit.UNIT), new Amount(7, Unit.MINUTE)).vanillaBean.build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).s04.yeastCakeFrom(24).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(1, Unit.UNIT), new Amount(30, Unit.DAY)).vanillaBean.build()
		],
		bottling: [
			new Ingredient(IngredientType.FLAVOUR, Producer.NONE, "ekstrakt z wanilii", new Amount(200, Unit.MILLILITRE))
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 19,
				temperature: 72.6
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 12
			},
			{
				step: BrewStep.SPARGE,
				volume: 12.97,
				temperature: 78,
				estimated: {
					sg: 14.6,
					volume: 24.7
				}
			}
		]
	],
	boilTime: 90,
	fermentation: [
		{
			date: "2017-02-26",
			step: BrewStep.WORT_FIRST,
			sg: 14.6,
			volume: 23.7
		},
		{
			date: "2017-02-26",
			step: BrewStep.WORT_BOILED,
			sg: 17.6,
			volume: 19
		},
		{
			date: "2017-02-26",
			step: BrewStep.FERM_PRIMARY,
			sg: 17.6,
			volume: 19,
			pinned: true
		},
		{
			date: "2017-03-12",
			step: BrewStep.FERM_SECONDARY,
			sg: 7.5,
			volume: 18.3,
			pinned: true
		},
		{
			date: "2017-04-20",
			step: BrewStep.BOTTLING,
			sg: 6.5,
			volume: 18,
			note: "Refermentacja: 60g cukru"
		}
	],
	notes: [
		"Laskę wanilii do gotowania pociąłem na paski i wrzuciłem do gara warzelnego wraz z ostatnią porcją chmielu.",
		"Przy zlewaniu na cichą odebrałem 0,5l piwa i zagotowałem w nim przez kilka minut drugą laskę wanilii, schłodziłem i dodałem z powrotem do fermentora.",
		"Ekstrakt waniliowy (200ml wódki + 2 pocięte na paski laski wanilii, leżakowane rok) dodałem bezpośrednio do piwa przed rozlewem. Podniosło to poziom alkoholu o ok. 1% w stosunku do wyliczonego."
	],
	tastingNotes: [
		"Umiarkowanie smaczne, ale zbyt intensywnie waniliowe. Ciężkie, dość męczące, trochę za bardzo alkoholowe.",
		"Aromat wanilii okazał się nietrwały: po kilku miesiącach w ogóle nie było go czuć."
	]
}
