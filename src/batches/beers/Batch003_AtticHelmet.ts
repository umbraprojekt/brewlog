import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch003: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Attic Helmet",
	tags: [
		Tag.CHMIEL_NOWOFALOWY,
		Tag.APA,
		Tag.SINGLE_HOP,
		Tag.SINGLE_MALT
	],
	version: 1,
	batch: 3,
	style: Style.APA,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			volume: 17.5,
			og: 13.7,
			ibu: 36,
			ebc: 14,
			efficiency: 70,
			carbonation: 2.2
		},
		actual: {
			volume: 19,
			og: 13.5,
			fg: 2.5,
			abv: 6,
			efficiency: 68.5,
			attenuation: 82.1
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).pale.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(60, Unit.MINUTE)).warrior.hopData(2014, 14.1).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(30, Unit.MINUTE)).warrior.hopData(2014, 14.1).build(),
			new WetIngredientBuilder(new Amount(10, Unit.GRAM), new Amount(15, Unit.MINUTE)).warrior.hopData(2014, 14.1).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).dryYeast.us05.build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(60, Unit.GRAM), new Amount(7, Unit.DAY)).hopDry.warrior.hopData(2014, 14.1).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 17.5,
				temperature: 73
			},
			{
				step: BrewStep.REST_MALTOSE,
				temperature: 66,
				time: 60
			},
			{
				step: BrewStep.REST_DEXTRINE,
				temperature: 72,
				time: 15
			},
			{
				step: BrewStep.SPARGE,
				volume: 10.6,
				temperature: 78
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-03-05",
			step: BrewStep.FERM_PRIMARY,
			sg: 13.5,
			volume: 19,
			pinned: true
		},
		{
			date: "2016-03-13",
			step: BrewStep.FERM_SECONDARY,
			sg: 3.5,
			pinned: true
		},
		{
			date: "2016-03-20",
			step: BrewStep.BOTTLING,
			sg: 2.5,
			volume: 17.5,
			note: "Refermentacja: 70g cukru"
		}
	],
	notes: [
		"Pierwsza warka z zacieraniem, oczywiście obarczona paroma głupimi błędami.",
		"Gęstość przy rozlewie zmierzona po dodaniu cukru na refermenację, więc może być troszkę zawyżona."
	],
	tastingNotes: [
		"Słód wniósł posmak orzechów, zaś chmiel okazał się wybitnie ananasowy, pomimo, że liczyłem na bardziej leśne nuty. Piwo nie oczarowywało, ale wyszło niezłe. Do powtórki (z nadzieją na uniknięcie błędów żółtodzioba)."
	]
};
