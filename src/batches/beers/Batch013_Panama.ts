import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch013: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Panama",
	tags: [
		Tag.IPA,
		Tag.CHMIEL_NOWOFALOWY,
		Tag.PSZENICA,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 13,
	style: Style.WHEAT_IPA,
	rating: 3,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 15,
			fg: 2.1,
			ibu: 74,
			ebc: 9,
			abv: 7,
			efficiency: 70,
			carbonation: 2.3,
			attenuation: 85.9
		},
		actual: {
			volume: 22,
			og: 15,
			fg: 2,
			abv: 7.1,
			efficiency: 77.4,
			attenuation: 86.7
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(3, Unit.KILOGRAM)).pale.strzegom.build(),
			new MashIngredientBuilder(new Amount(2.5, Unit.KILOGRAM)).wheat.strzegom.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).pils.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(60, Unit.MINUTE)).rakau.hopData(2015, 9.5).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(20, Unit.MINUTE)).rakau.hopData(2015, 9.5).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).rakau.hopData(2015, 9.5).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(300, Unit.MILLILITRE)).us05.yeastCakeFrom(12).build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(100, Unit.GRAM), new Amount(21, Unit.DAY)).rakau.hopDry.hopData(2015, 9.5).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 20,
				temperature: 68
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 63,
				time: 75
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 16,
				temperature: 78,
				estimated: {
					sg: 17.8,
					volume: 24.7
				}
			}
		]
	],
	boilTime: 90,
	fermentation: [
		{
			date: "2016-08-11",
			step: BrewStep.WORT_FIRST,
			sg: 13.6,
			volume: 30
		},
		{
			date: "2016-08-11",
			step: BrewStep.WORT_BOILED,
			sg: 15,
			volume: 22
		},
		{
			date: "2016-08-11",
			step: BrewStep.FERM_PRIMARY,
			sg: 15,
			volume: 22,
			pinned: true
		},
		{
			date: "2016-08-21",
			step: BrewStep.FERM_SECONDARY,
			sg: 3,
			volume: 21.4,
			pinned: true
		},
		{
			date: "2016-09-11",
			step: BrewStep.BOTTLING,
			sg: 2,
			volume: 20.3,
			note: "Refermentacja: 117g cukru"
		}
	],
	notes: [
		"W założeniu miałem warzyć American Wheat 12°Blg, ale chyba znów dałem o kilogram słodu za dużo i mocno przestrzeliłem ekstrakt. W efekcie zmodyfikowałem recepturę już po wysładzaniu, a dodatkowe 100g chmielu musiałem dokupić przed zlaniem na cichą."
	],
	tastingNotes: [
		"Przez niską temperaturę zacierania, piwo wyszło bardzo wytrawne i dość mocno alkoholowe. Przyjemna chmielowość nie dała rady przykryć faktu, że to był mimo wszystko zawodnik wagi ciężkiej. Moim zdaniem piwo niespecjalnie udane."
	]
};
