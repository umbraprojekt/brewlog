import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch029: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Hard Hat",
	tags: [
		Tag.STOUT,
		Tag.ZYTO
	],
	version: 2,
	original: 8,
	batch: 29,
	style: Style.FES,
	rating: 3,
	stock: 9,
	params: {
		planned: {
			volume: 19,
			og: 18,
			fg: 4.9,
			ibu: 61,
			ebc: 103,
			abv: 7.3,
			efficiency: 70,
			carbonation: 1.8,
			attenuation: 72.7
		},
		actual: {
			volume: 17.2,
			og: 17.8,
			fg: 6.5,
			abv: 6.4,
			efficiency: 62.8,
			attenuation: 63.5
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4, Unit.KILOGRAM)).pale.viking.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).rye.fawcett.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).cara30.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).caramelRye.fawcett.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).chocolate400.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).roastedBarley.viking.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(100, Unit.GRAM), new Amount(60, Unit.MINUTE)).fuggle.hopData(2015, 3).build(),
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(60, Unit.MINUTE)).target.hopData(2015, 5).build(),
		],
		boil2: [],
		yeast: [
			new YeastIngredientBuilder(new Amount(500, Unit.MILLILITRE)).s04.yeastCakeFrom(28).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				note: "Dodać słody jasne i karmelowe",
				volume: 22,
				temperature: 72.4
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				note: "Dodać jęczmień prażony",
				temperature: 78,
				time: 12
			},
			{
				step: BrewStep.SPARGE,
				volume: 8.74,
				temperature: 78,
				estimated: {
					sg: 16,
					volume: 22.7
				}
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2017-09-22",
			step: BrewStep.WORT_FIRST,
			sg: 15.9,
			volume: 22
		},
		{
			date: "2017-09-22",
			step: BrewStep.WORT_BOILED,
			sg: 17.8,
			volume: 17.2
		},
		{
			date: "2017-09-22",
			step: BrewStep.FERM_PRIMARY,
			sg: 17.8,
			volume: 17.2,
			pinned: true
		},
		{
			date: "2017-09-29",
			step: BrewStep.FERM_SECONDARY,
			sg: 6.5,
			volume: 16.9,
			pinned: true
		},
		{
			date: "2017-10-23",
			step: BrewStep.BOTTLING,
			sg: 6.5,
			volume: 16.8,
			note: "Refermentacja: 55g cukru"
		}
	],
	notes: [
		"Powtórka znakomitej warki #8.",
		"Użyta woda: kranówka."
	],
	tastingNotes: [
		"Próbka przy rozlewie miała srogą goryczkę. Smakowała dobrze, ale chyba przesadziłem z chmieleniem.",
		"Ostatecznie nagazowanie okazało się bardzo niskie, ale buduje się śliczna, drobna jak na azocie pianka. Piwo bardzo gorzkie, trochę za agresywne, ale smaczne. Przy powtórce będę musiał uważać z chmieleniem, bo jednak pierwowzór był lepszy.",
		"Po półtora roku piwo ułożyło się: goryczka jest znacznie miększa, a całość stała się niezwykle gładka. Leżakowanie zdecydowanie dobrze wpłynęło na to piwo. Nagazowanie nieco wzrosło, ale poziom jest nadal akceptowalny."
	]
};
