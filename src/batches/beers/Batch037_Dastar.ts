import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch037: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Dastar",
	tags: [
		Tag.STOUT,
		Tag.OWIES,
		Tag.LAKTOZA,
		Tag.SINGLE_HOP,
		Tag.BEZALKOHOLOWE,
		Tag.PLATKI_DEBOWE
	],
	version: 1,
	batch: 37,
	style: Style.TROPICAL_STOUT,
	rating: 2,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 16,
			fg: 5.6,
			ibu: 41,
			ebc: 82,
			abv: 0,
			efficiency: 70,
			carbonation: 2.2,
			attenuation: 65.3
		},
		actual: {
			og: 16,
			fg: 7.5,
			efficiency: 70.4,
			attenuation: 49.7,
			volume: 23.3,
			abv: 0
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4, Unit.KILOGRAM)).pale.malteurop.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).caramel150.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).oatFlakes.build(),
			new MashIngredientBuilder(new Amount(0.5, Unit.KILOGRAM)).chocolate400.build(),
			new MashIngredientBuilder(new Amount(0.3, Unit.KILOGRAM)).roastedBarley.viking.build(),
			new MashIngredientBuilder(new Amount(150, Unit.GRAM)).lactose.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(20, Unit.MINUTE)).marynka.hopData(2016, 6).build()
		],
		boil2: [
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(20, Unit.MINUTE)).marynka.hopData(2016, 6).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(500, Unit.MILLILITRE)).pa7.yeastCakeFrom(35).build()
		]
	},
	water: {
		profile: "kranówka",
		base: new TapWater2018(33.1),
		ions: TapWater2018.ions
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 22,
				temperature: 77.8,
				note: "Słody jasne, karmelowe i czekoladowe"
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 72,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10,
				note: "Dodać jęczmień prażony"
			},
			{
				step: BrewStep.SPARGE,
				volume: 11.1,
				temperature: 78,
				estimated: {
					sg: 14.3,
					volume: 25.3
				}
			}
		]
	],
	boilTime: [
		20,
		150
	],
	fermentation: [
		{
			date: "2018-04-14",
			step: BrewStep.WORT_FIRST,
			sg: 14.6,
			volume: 24,
			note: "Rozcieńczyłem 1,3l wody do 13,9°Blg"
		},
		{
			date: "2018-04-14",
			step: BrewStep.WORT_BOILED,
			sg: 14.9,
			volume: 23.3
		},
		{
			date: "2018-04-14",
			step: BrewStep.FERM_PRIMARY,
			sg: 14.9,
			volume: 23.3,
			pinned: true
		},
		{
			date: "2018-04-21",
			step: BrewStep.FERM_SECONDARY,
			sg: 8,
			volume: 22.5,
			pinned: true
		},
		{
			date: "2018-05-20",
			step: BrewStep.BOTTLING,
			sg: 7.5,
			volume: 12,
			note: "Wersja podstawowa (85g cukru). Refermentacja: 140g cukru"
		},
		{
			date: "2018-05-20",
			step: BrewStep.BOTTLING,
			sg: 7.5,
			volume: 10.5,
			note: "Wersja z płatkami dębowymi (4,1% alko; 55g cukru)"
		}
	],
	notes: [
		"Drugi eksperyment z piwem bezalkoholowym, tym razem o dwukrotnie większym ekstrakcie, a więc w założeniu bardziej złożonym i pełniejszym. Piwo jest też trochę sposobem na pozbycie się zalegających surowców: resztka Marynki, resztka laktozy i prażony jęczmień, którego jakiś czas temu przez pomyłkę kupiłem o kilogram za dużo. Poza tym chcę spróbować bezalkoholowego stoutu.",
		"Profil wody: 100% kranówka.",
		"Dwa gotowania: 20 minut przed zadaniem drożdży i 30 minut po zakończonej fermentacji.",
		"Młóto mocno wiązało wodę, być może ze względu na kleistość wniesioną przez owies. Niestety, zauważyłem to dopiero po zebraniu sprzętu po wysładzaniu (dodajmy też, że wody i tak było około litra więcej niż w przepisie, bo zachciało mi się kleikowania owsa). Powinienem podgrzać wodę i wysładzać nieco więcej niż miałem w planie, no ale wyszło jak wyszło. Na następny raz będę pamiętał.",
		"W związku z faktem, że w trakcie gotowania po fermentacji odparuje część wody, nie ma możliwości stwierdzenia, jaka jest realna gęstość początkowa piwa; 16°Blg jest zaledwie przybliżeniem. Podobnie rzecz się ma z gęstością końcową.",
		"Przy zlewaniu na cichą podzieliłem piwo na dwie części: 12l wersji podstawowej i 10,5l drugiej, do której dodałem 20g mocno opiekanych płatków dębowych. Pozwolę piwu poleżeć przez miesiąc i zobaczymy, czy płatki coś ciekawego wniosą. I czy jest sens dawać płatki dębowe do piwa 0%.",
		"Zmiana planów: drugie gotowanie wydłużone do 150 minut (2,5h), żeby odparować 95% alkoholu.",
		"Postanowiłem nie gotować wersji z płatkami; zabutelkuję ją bez odparowywania alkoholu. Pozwoli to na porównanie obu wersji (za wyjątkiem nachmielenia, które w obu będzie różne), pozza tym szkoda robić wersję 0% z piwa leżakowanego z płatkami dębowymi. Chmielenie części bez płatków zmieniłem z 25 na 15g Marynki."
	],
	tastingNotes: [
		"Wersja bezalkoholowa:",
		"Piana niemal zerowa, tylko krótko utrzymująca się warstewka na ćwierć palca. Drobna, jasnobeżowa. Piwo czarne, przy dnie szklanki brązowy prześwit.",
		"Aromat słodkawy, mimo znikomej ilości, wydaje mi się, że odróżniam tam mleczny zapaszek laktozy. Trochę estrowe. Kojarzy się z watą cukrową.",
		"W smaku przyjemnie palone, nie za mocno, ale w sam raz, by zidentyfikować piwo jako stout. Bardzo delikatna goryczka chmielowa, w zasadzie marginalna. Troszkę spalonego tostu, odrobinka zboża. Nawet umiarkowanie złożone, choć liczyłem na więcej.",
		"Niezbyt rzadkie. Nie jest też gęste, ale czuć nieco ciała dzięki zatarciu na słodko, laktozie i płatkom owsianym. Na pewno nie jest wodniste.",
		"Piwo przypomina zwykłego FESa, różni się tylko dość wyraźną estrowością od stosunkowo wysokiej temperatury fermentacji. Generalnie całkiem niezłe, z dobrym ciałem i całkiem znośnym smakiem. Aromat trochę kulawy, ale da się z tym żyć.",
		"Do poprawy: w zasadzie nic. Gdybym warzył jeszcze raz bezalkoholowy stout, to można spróbować tego samego przepisu, ale z mniejszą ilością słodu karmelowego, bo przypuszczam, że zapach waty cukrowej może pochodzić właśnie od niego. Ewentualnie sypnąć więcej laktozy, żeby dała coś w smaku.",
		"Do powtórzenia: zacieranie na słodko i płatki owsiane."
	]
};
