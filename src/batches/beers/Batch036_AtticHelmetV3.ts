import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch036: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Attic Helmet",
	tags: [
		Tag.APA,
		Tag.SINGLE_HOP,
		Tag.CHMIEL_NOWOFALOWY
	],
	version: 3,
	original: 12,
	batch: 36,
	style: Style.APA,
	rating: 4,
	stock: 0,
	params: {
		planned: {
			volume: 22,
			og: 12.1,
			fg: 2.9,
			ibu: 41,
			ebc: 12,
			abv: 5,
			efficiency: 75,
			carbonation: 2.3,
			attenuation: 76.6
		},
		actual: {
			volume: 26.2,
			og: 12,
			fg: 1.75,
			abv: 5.5,
			efficiency: 88.4,
			attenuation: 85.4
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).pale.bestmalz.build(),
			new MashIngredientBuilder(new Amount(10, Unit.GRAM)).carafa3Special.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(20, Unit.MINUTE)).warrior.hopData(2017, 13.7).build(),
			new WetIngredientBuilder(new Amount(30, Unit.GRAM), new Amount(7, Unit.MINUTE)).warrior.hopData(2017, 13.7).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).us05.dryYeast.build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(40, Unit.GRAM), new Amount(21, Unit.DAY)).warrior.hopDry.hopData(2017, 13.7).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 15,
				temperature: 72.7,
				note: "Tylko słód pale ale"
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10,
				note: "Dodać słód palony"
			},
			{
				step: BrewStep.SPARGE,
				volume: 10,
				temperature: 78,
				estimated: {
					sg: 11.4,
					volume: 24.6
				}
			}
		]
	],
	boilTime: 20,
	fermentation: [
		{
			date: "2018-04-07",
			step: BrewStep.WORT_FIRST,
			sg: 12.6,
			volume: 25.9
		},
		{
			date: "2018-04-07",
			step: BrewStep.WORT_BOILED,
			sg: 13.6,
			volume: 23,
			note: "Rozcieńczona 3,2l wody"
		},
		{
			date: "2018-04-07",
			step: BrewStep.FERM_PRIMARY,
			sg: 12,
			volume: 26.2,
			pinned: true
		},
		{
			date: "2018-04-15",
			step: BrewStep.FERM_SECONDARY,
			sg: 3,
			volume: 26,
			pinned: true
		},
		{
			date: "2018-05-05",
			step: BrewStep.BOTTLING,
			sg: 1.75,
			volume: 25.2,
			note: "Refermentacja: 135g cukru"
		}
	],
	notes: [
		"Delikatnie zmniejszony ekstrakt w stosunku do poprzednich wersji: 12 zamiast 13,5°Blg. Powrót do prostego zasypu z warki #3.",
		"Eksperyment z krótkim gotowaniem: ponieważ chmiel postanowiłem dodać tylko na smak i aromat, gotowanie skróciłem do 20 minut. To był chyba głupi pomysł, bo nie strąciły się białka i brzeczka okazała się błotniście mętna.",
		"Również eksperymentem jest użycie słodu Bestmalz. Poprzednie warki były na słodzie ze Strzegomia i wyszły wyraźnie orzechowe. Tym razem chciałbym uzyskać bardziej neutralny profil słodowy.",
		"Profil wody: 30l butelkowanej wody Amita (~350mg/l) i 0,6l wody z filtra Brita.",
		"Zbyt wysoka wydajność zacierania. W związku z tym zmieniłem odrobinę chmielenie, tak, żeby nadal móc uzyskać 40 IBU: na 20 minut poszło 32g chmielu, a na 7 minut 33g.",
		"Pomimo zaledwie tygodniowej fermentacji burzliwej, gęstość zeszła aż do 3°Blg. Zebrałem też prawie litr gęstwy, co okazało się błogosławieństwem, bo mogłem zadać drożdże do warek #38 i #39."
	],
	tastingNotes: [
		"Mocno zamglone, ciemnozłociste. Mogłoby być nieco klarowniejsze. W zasadzie brak piany. Następnym razem poprzestanę na samym słodzie pale ale albo dodam płatków owsianych.",
		"Aromat mało wyraźny. Ananas, mango, nieco słodowości. W smaku dominuje baza słodowa - ciasto z minimalną domieszką orzechów. Chmiel wniósł owoce tropikalne i grejpfruta. Ten ostatni zostaje na długo w finiszu. Pomimo niskiego ekstraktu końcowego, piwo nie jest wytrawne, a co najwyżej półwytrawne. Brak mi bezpośredniego porównania, ale wydaje mi się, że obniżenie ekstraktu o 1,5% nie spowodowało spadku treściwości piwa, ale może to być spowodowane niezbiciem białek podczas gotowania.",
		"Nagazowanie po nieco ponad tygodniu od zabutelkowania jest stosunkowo niskie, ale akceptowalne. Prawdopodobnie jeszcze się dogazuje, ale nie sądzę, żeby było to bardzo dużo.",
		"Do poprawy w kolejnej warce: wyeliminować niepotrzebny dodatek Carafy, gotować przez pełną godzinę celem zbicia białek, zwiększyć dawkę chmielu na aromat i na zimno.",
		"Do powtórzenia: chmielenie tylko na smak i aromat, ekstrakt 12%."
	]
};
