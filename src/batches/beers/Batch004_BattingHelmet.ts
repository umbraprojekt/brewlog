import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch004: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Batting Helmet",
	tags: [
		Tag.CHMIEL_NOWOFALOWY,
		Tag.IPA,
		Tag.SINGLE_HOP
	],
	version: 1,
	batch: 4,
	style: Style.RED_IPA,
	rating: 5,
	stock: 0,
	params: {
		planned: {
			volume: 20,
			og: 16.6,
			ibu: 50,
			ebc: 27,
			efficiency: 70,
			carbonation: 1.8
		},
		actual: {
			volume: 21,
			og: 15,
			fg: 5.5,
			abv: 5.4,
			efficiency: 60.9,
			attenuation: 64.5
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(5, Unit.KILOGRAM)).pale.strzegom.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).caramelRed.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).munich2.strzegom.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(60, Unit.MINUTE)).mosaic.hopData(2015, 11.7).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(30, Unit.MINUTE)).mosaic.hopData(2015, 11.7).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(15, Unit.MINUTE)).mosaic.hopData(2015, 11.7).build(),
			new WetIngredientBuilder(new Amount(15, Unit.GRAM), new Amount(5, Unit.MINUTE)).mosaic.hopData(2015, 11.7).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).yeastCakeFrom(3).us05.build()
		],
		secondary: [
			new WetIngredientBuilder(new Amount(25, Unit.GRAM), new Amount(7, Unit.DAY)).mosaic.hopDry.hopData(2015, 11.7).build()
		]
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 21,
				temperature: 76
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 68,
				time: 70
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 12.4,
				temperature: 78,
			}
		]
	],
	boilTime: 60,
	fermentation: [
		{
			date: "2016-03-15",
			step: BrewStep.FERM_PRIMARY,
			sg: 15,
			volume: 21,
			pinned: true
		},
		{
			date: "2016-03-29",
			step: BrewStep.FERM_SECONDARY,
			sg: 6,
			pinned: true
		},
		{
			date: "2016-04-05",
			step: BrewStep.BOTTLING,
			sg: 5.5,
			volume: 19,
			note: "Refermentacja: 70g cukru"
		}
	],
	notes: [],
	tastingNotes: [
		"Bardzo smaczne. Zrównoważona słodowość i chmielowość, dość dużo ciała, nie za wysoka, choć zaznaczona goryczka. Zdecydowanie do powtórki."
	]
};
