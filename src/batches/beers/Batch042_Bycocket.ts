import { BrewStep } from "../../enums/BrewStep";
import { Status } from "../../enums/Status";
import { Style } from "../../enums/Style";
import { Tag } from "../../enums/Tag";
import { Amount } from "../../ingredients/Amount";
import { MashIngredientBuilder } from "../../ingredients/MashIngredientBuilder";
import { Unit } from "../../ingredients/Unit";
import { DistilledWater } from "../../ingredients/water/DistilledWater";
import { TapWater2018 } from "../../ingredients/water/TapWater2018";
import { WetIngredientBuilder } from "../../ingredients/WetIngredientBuilder";
import { YeastIngredientBuilder } from "../../ingredients/YeastIngredientBuilder";
import { IBatchConfig } from "../../types/IBatch";

export const Batch042: IBatchConfig = {
	status: Status.PUBLISHED,
	name: "Bycocket",
	tags: [
		Tag.BITTER,
		Tag.BEZALKOHOLOWE
	],
	version: 1,
	batch: 42,
	style: Style.ESB,
	rating: 4,
	stock: 4,
	params: {
		planned: {
			volume: 20.5,
			og: 13,
			fg: 3.2,
			ibu: 40,
			ebc: 25,
			abv: 0,
			efficiency: 70,
			carbonation: 2,
			attenuation: 75.2
		},
		actual: {
			og: 12.9,
			efficiency: 77.2,
			volume: 22.8,
			fg: 2.75,
			abv: 0,
			attenuation: 78.7
		}
	},
	ingredients: {
		mash: [
			new MashIngredientBuilder(new Amount(4, Unit.KILOGRAM)).marisOtter.build(),
			new MashIngredientBuilder(new Amount(1, Unit.KILOGRAM)).paleCrystal.build()
		],
		boil: [
			new WetIngredientBuilder(new Amount(50, Unit.GRAM), new Amount(60, Unit.MINUTE)).challenger.hopData(2017, 5.3).build()
		],
		boil2: [
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(20, Unit.MINUTE)).fuggle.hopData(2017, 3.9).build(),
			new WetIngredientBuilder(new Amount(20, Unit.GRAM), new Amount(7, Unit.MINUTE)).fuggle.hopData(2017, 3.9).build()
		],
		yeast: [
			new YeastIngredientBuilder(new Amount(1, Unit.PACKET)).fm11.yeastStarter.build()
		]

	},
	water: {
		profile: "Burton On Trent",
		base: new TapWater2018(20.3),
		dilution: new DistilledWater(10),
		additions: {
			CaSO4: 27,
			MgSO4: 15,
			NaHCO3: 4,
			CaCO3: 2
		},
		ions: {
			Ca: 285.5,
			Mg: 53.6,
			Na: 64.1,
			SO4: 723.1,
			Cl: 24.1,
			HCO3: 308.7
		}
	},
	mash: [
		[
			{
				step: BrewStep.DOUGH_IN,
				volume: 16,
				temperature: 72.3
			},
			{
				step: BrewStep.REST_MAIN,
				temperature: 67,
				time: 60
			},
			{
				step: BrewStep.MASHOUT,
				temperature: 78,
				time: 10
			},
			{
				step: BrewStep.SPARGE,
				volume: 14.3,
				temperature: 78,
				estimated: {
					sg: 11.6,
					volume: 24.3
				}
			}
		]
	],
	boilTime: [
		60,
		150
	],
	fermentation: [
		{
			date: "2018-05-12",
			step: BrewStep.WORT_FIRST,
			sg: 13.4,
			volume: 23.8,
			note: "Rozcieńczona 3l wody"
		},
		{
			date: "2018-05-13",
			step: BrewStep.WORT_BOILED,
			sg: 12.9,
			volume: 22.8
		},
		{
			date: "2018-05-13",
			step: BrewStep.FERM_PRIMARY,
			sg: 12.9,
			volume: 22.8,
			pinned: true
		},
		{
			date: "2018-05-26",
			step: BrewStep.FERM_SECONDARY,
			sg: 3.5,
			volume: 22.7,
			pinned: true
		},
		{
			date: "2018-06-21",
			step: BrewStep.BOTTLING,
			sg: 2.75,
			volume: 20.8,
			note: "Refermentacja: 140g cukru"
		}
	],
	notes: [
		"Bezalkoholowy ESB. W zasadzie ma posłużyć tylko za starter drożdżowy na potrzeby old ale. Nie chciałem też robić kolejnej warki best bittera, a tym bardziej ordinary bittera, więc tym razem padło na ESB.",
		"Pierwsza warka, w której świadomie modyfikowałem profil wody.",
		"Gotowanie musiałem przesunąć na kolejny dzień, bo zapomniałem przygotować startera drożdżowego. Mógłbym niby zadać drożdże bezpośrednio, ale przy takiej gęstości brzeczki to już loteria; wolę mieć pewność.",
		"Próbka przed gotowaniem po zakończonej fermentacji była bardzo smaczna. Głównie karmelowa, z fajną chmielową kontrą, bardzo w stylu. Aż szkoda wygotowywać. Będę chyba musiał uwarzyć to samo piwo w wersji z alkoholem."
	],
	tastingNotes: [
		"Próbka 100ml degustowana 4 dni po butelkowaniu. Jasnobursztynowe, opalizujące, niska, nietrwała pianka. W aromacie karmel, melasa, rodzynki i delikatna kwaskowatość kojarząca się z wiśniówką. Bardzo ciekawy zapach. Smak mniej emocjonujący: typowe ESB, lekko słodowe z wyraźną ziołową goryczką. Rzadkie, ale nie tak wodniste jak się spodziewałem. Dobre, wbrew obawom, naprawdę dobre. Do powtórzenia bez zmian (najlepiej w wersji z alkoholem.",
		"Z czasem piwo stało się mniej interesujące, uboższe, ale nadal przywodzi na myśl nieco wykastrowanego bittera."
	]
}
