import { IBatch } from "../types/IBatch";
import { Beers } from "./beers/Beers";
import { Meads } from "./mead/Meads";

export const Batches: Array<IBatch> = [...Beers, ...Meads];

Batches.forEach(batch => {
	batch.tags.forEach(tag => tag.increment());
});
