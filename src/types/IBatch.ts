import { IBatchParams } from "./IBatch/IBatchParams";
import { IBatchIngredients } from "./IBatch/IBatchIngredients";
import { IBatchFermentationStep } from "./IBatch/IBatchFermentationStep";
import { IBatchMashStep } from "./IBatch/IBatchMashStep";
import { IBatchWaterProfile } from "./IBatch/IBatchWaterProfile";
import { ITag } from "./ITag";
import { Status } from "../enums/Status";
import { Type } from "../enums/Type";
import { Style } from "../enums/Style";
import { Tag } from "../enums/Tag";

export interface IBatchConfig {
	name: string;
	status: Status;
	version: number;
	versions?: Array<number>;
	original?: number;
	batch: number;
	tags: Array<Tag>;
	style: Style;
	rating?: number;
	stock: number;
	params: {
		planned?: IBatchParams;
		actual: IBatchParams;
	};
	ingredients: IBatchIngredients;
	water?: IBatchWaterProfile;
	mash: Array<Array<IBatchMashStep>>;
	mashNote?: string;
	boilTime?: number|Array<number>;
	fermentation: Array<IBatchFermentationStep>;
	notes: Array<string>;
	tastingNotes: Array<string>;
}

export interface IBatch extends IBatchConfig {
	url: string;
	type: Type;
	next: IBatch;
	prev: IBatch;
}
