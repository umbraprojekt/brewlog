import { IBatchWaterIons } from "./IBatchWaterIons";
import { IBatchWater } from "./IBatchWater";

export interface IBatchWaterProfile {
	profile: string;
	base: IBatchWater;
	dilution?: IBatchWater;
	additions?: {
		CaSO4?: number;
		CaCO3?: number;
		CaCl2?: number;
		MgSO4?: number;
		NaHCO3?: number;
		NaCl?: number;
	};
	ions: IBatchWaterIons;
}
