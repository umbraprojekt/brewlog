export interface IBatchMashStep {
	step: string;
	volume?: number;
	temperature: number;
	time?: number;
	note?: string;
	estimated?: {
		sg: number;
		volume: number;
	};
}
