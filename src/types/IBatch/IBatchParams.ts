export interface IBatchParams {
	volume?: number;
	og?: number;
	fg?: number;
	ibu?: number;
	ebc?: number;
	abv?: number;
	efficiency?: number;
	carbonation?: number;
	attenuation?: number;
}
