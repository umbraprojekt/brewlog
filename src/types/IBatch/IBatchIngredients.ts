import { IBatchMashIngredient } from "./IBatchMashIngredient";
import { IBatchBoilIngredient } from "./IBatchBoilIngredient";
import { IBatchYeastIngredient } from "./IBatchYeastIngredient";
import { IBatchSecondaryIngredient } from "./IBatchSecondaryIngredient";
import { IBatchBottlingIngredient } from "./IBatchBottlingIngredient";
import { IIngredient } from "../../ingredients/IIngredient";

export interface IBatchIngredients {
	mash: Array<IIngredient>;
	boil?: Array<IIngredient>;
	boil2?: Array<IIngredient>;
	yeast: Array<IIngredient>;
	secondary?: Array<IIngredient>;
	bottling?: Array<IIngredient>;
}
