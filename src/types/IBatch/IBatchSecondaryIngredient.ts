export interface IBatchSecondaryIngredient {
	type: string;
	name: string;
	amount: string;
	time: string;
}
