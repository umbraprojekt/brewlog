export interface IBatchMashIngredient {
	type: string;
	name: string;
	amount: string;
}
