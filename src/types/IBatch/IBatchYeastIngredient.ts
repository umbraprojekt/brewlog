export interface IBatchYeastIngredient {
	name: string;
	amount: string;
}
