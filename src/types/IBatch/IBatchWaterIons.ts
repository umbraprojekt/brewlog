export interface IBatchWaterIons {
	Ca: number;
	Mg: number;
	Na: number;
	SO4: number;
	Cl: number;
	HCO3: number;
}
