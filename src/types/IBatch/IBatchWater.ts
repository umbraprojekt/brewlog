import { IBatchWaterIons } from "./IBatchWaterIons";

export interface IBatchWater {

	name: string;
	amount: number;
	ions: IBatchWaterIons;
}
