export interface IBatchBottlingIngredient {
	name: string;
	amount: string;
}
