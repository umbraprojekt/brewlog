export interface IBatchBoilIngredient {
	type: string;
	name: string;
	amount: string;
	time: string;
}
