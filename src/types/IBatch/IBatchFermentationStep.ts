export interface IBatchFermentationStep {
	date?: string;
	step: string;
	sg?: number;
	volume?: number;
	note?: string;
	pinned?: boolean;
}
